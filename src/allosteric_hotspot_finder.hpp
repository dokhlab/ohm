#pragma once

#include <algorithm>
#include <array>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <functional>
#include "grid.hpp"
#include "jnc/math"
#include "jnc/bio"

class AllostericHotspotFinder {
public:
    using Position = std::array<int, 3>;

    struct Site {
        Position position;
        double volume;
        double bfactor;
        int spot;
        std::deque<int> neighbors;
    };
    using Sites = std::map<int, Site>;

    Sites sites;

    struct Spot {
        std::set<int> sites;
        double bfactor;
        double volume;
        int centroid;
        int area;
    };
    using Spots = std::map<int, Spot>;

    Spots spots;
    std::vector<int> spot_ids;

    struct Area {
        std::vector<int> spot_inds;
    };
    using Areas = std::vector<Area>;

    Areas areas;

    std::string infile;
    std::string outfile;

    Grid sgrid;

    double vdw_radius = 3.3;
    double shell = 2;
    double interval = 1;
    double threshold = 0.1;
    bool show_all = false;
    int delta = 3;
    double volume_ratio = 0.5;
    double cutoff = 0.5;

    AllostericHotspotFinder(const jnc::Opt &opt) {
        infile = opt.get("i");
        outfile = opt.get("o");
        opt.set(vdw_radius, "r", "vdw_radius");
        opt.set(shell, "s", "shell");
        opt.set(interval, "int", "interval");
        opt.set(threshold, "t", "threshold");
        opt.set(delta, "d", "delta");
        opt.set(volume_ratio, "v", "volume");
        opt.set(cutoff, "c", "cutoff");
        show_all = opt.has("all");
    }

    template<typename Visited_>
    void set_spot(int id1, int spot_id, Spot &spot, Visited_ &visited) {
        visited[id1] = true;
        auto &site1 = sites[id1];

        spot.sites.insert(id1);
        spot.bfactor = site1.bfactor;
        site1.spot = spot_id;

        for (auto && id2 : site1.neighbors) {
            if (!visited[id2]) {
                auto &site2 = sites[id2];
                if (site1.bfactor == site2.bfactor || std::fabs(site1.bfactor - site2.bfactor) < threshold) {
                    set_spot(id2, spot_id, spot, visited);
                }
            }
        }
    }

    template<typename Visited_, typename Distances_>
    void set_area(int spot_ind1, int area_id, Area &area, Visited_ &visited, const Distances_ &distances) {
        visited[spot_ind1] = true;

        int spot_id1 = spot_ids[spot_ind1];
        auto &spot = spots[spot_id1];
        spot.area = area_id;

        area.spot_inds.push_back(spot_ind1);

        for (int spot_ind2 = 0; spot_ind2 < spot_ids.size(); spot_ind2++) {
            if (spot_ind1 != spot_ind2) {
                if (!visited[spot_ind2]) {
                    double d = distances(spot_ind1, spot_ind2);
                    if (d < 7) {
                        set_area(spot_ind2, area_id, area, visited, distances);
                    }
                }
            }
        }
    }

    double site_distance(int id1, int id2) {
        auto &p1 = sites[id1].position;
        auto &p2 = sites[id2].position;
        return jnc::distance(p1, p2);
    }

    void set_spot_volume(Spot &spot) {
        double volume = 0;
        int n = 0;
        for (auto && site_id : spot.sites) {
            auto &site = sites[site_id];
            volume += site.volume;
            n++;
        }
        spot.volume = volume / n;
    }

    void set_areas() {
        // Set spot centroid distances matrix
        int n_spots = spot_ids.size();
        jnc::Matd spot_distances(n_spots, n_spots);
        for (int i = 0; i < n_spots; i++) {
            int site_id1 = spots[spot_ids[i]].centroid;
            spot_distances(i, i) = 0;
            for (int j = i + 1; j < n_spots; j++) {
                int site_id2 = spots[spot_ids[j]].centroid;
                spot_distances(i, j) = spot_distances(j, i) = site_distance(site_id1, site_id2);
            }
        }

        // Set spot_visited
        std::vector<bool> spot_visited(n_spots);
        for (int i = 0; i < n_spots; i++) {
            spot_visited[i] = false;
        }

        int area_id = 0;
        areas.push_back(Area{});
        for (int i = 0; i < n_spots; i++) {
            if (!spot_visited[i]) {
                set_area(i, area_id, areas.back(), spot_visited, spot_distances);
                areas.push_back(Area{});
                area_id++;
            }
        }
        if (areas.back().spot_inds.empty()) {
            areas.pop_back();
        }

        for (auto && area : areas) {
            set_area_centroid(area, spot_distances);
        }
    }

    template<typename Distances_>
    void set_area_centroid(Area &area, const Distances_ &distances) {
        int n = area.spot_inds.size();;
        std::vector<double> v(n);
        for (int i = 0; i < n; i++) {
            int ind1 = area.spot_inds[i];
            v[i] = spots[spot_ids[area.spot_inds[i]]].bfactor;
//            double d = 0;
//            for (int j = 0; j < n; j++) {
//                if (i != j) {
//                    int ind2 = area.spot_inds[j];
//                    double dist = distances(ind1, ind2);
//                    if (d < dist) {
//                        d = dist;
//                    }
//                }
//            }
//            v[i] = d;
        }

        auto it = std::max_element(v.begin(), v.end());
        if (it != v.end()) {
            int diff = std::distance(v.begin(), it);
            std::swap(area.spot_inds[0], area.spot_inds[diff]);
        }
    }

    void set_spot_centroid(Spot &spot) {
//        spot.centroid = *spot.sites.begin();
//        return;
        int n = spot.sites.size();
//        std::cout << "Spot: " << n << std::endl;
        std::vector<int> sites(n);
        int i = 0;
        for (auto && site : spot.sites) {
            sites[i] = site;
            i++;
        }

        std::vector<double> distances(n);
        for (int i = 0; i < n; i++) {
            int site_id1 = sites[i];
            double d_max = 0;
            for (int j = 0; j < n; j++) {
                if (i != j) {
                    int site_id2 = sites[j];
                    double d = site_distance(site_id1, site_id2);
                    if (d_max < d) {
                        d_max = d;
                    }
                }
            }
            distances[i] = d_max;
        }
        auto it = std::min_element(distances.begin(), distances.end());
        int d = std::distance(distances.begin(), it);
        spot.centroid = sites[d];
    }

    void set_spots() {
        // Set spot_visited
        std::map<int, bool> spot_visited;
        for (auto && p : sites) {
            spot_visited[p.first] = false;
        }

        // Set site neighbors

        // Initialize spots
        int spot_id = 0;
        for (auto && p : sites) {
            int id = p.first;
            if (!spot_visited[id]) {
                spots[spot_id] = Spot{};
                set_spot(id, spot_id, spots[spot_id], spot_visited);
                spot_id++;
            }
        }

        // Set neighbors of spots
//        for (int i = 0; i < spots.size(); i++) {
//            int spot_id1 = i;
//            auto &spot1 = spots[i];
//            for (auto && id1 : spot1.sites) {
//                auto &site1 = sites[id1];
//                for (auto && id2 : site1.neighbors) {
//                    auto &site2 = sites[id2];
//                    int spot_id2 = site2.spot;
//                    if (spot_id1 != spot_id2) {
//                        spot1.neighbors.insert(spot_id2);
//                    }
//                }
//            }
//        }

        // Erase spots
        std::deque<int> dq;
        for (auto && p : spots) {
            int spot_id = p.first;
            auto &spot = p.second;
            if (spot.sites.size() <= 5 || spot.volume > 0.5) {
                dq.push_back(spot_id);
            }
        }
        for (auto && i : dq) {
            spots.erase(i);
        }

        spot_ids.resize(spots.size());
        int i = 0;
        for (auto && p : spots) {
            spot_ids[i] = p.first;
            i++;
        }

        // Set centroids and volume
        for (auto && p : spots) {
            set_spot_centroid(p.second);
            set_spot_volume(p.second);
        }

    }

    void set_grid() {
        jnc::bio::PdbChain in_pdb(infile);
        sgrid.vdw_radius = vdw_radius;
        sgrid.shell = shell;
        sgrid.interval = {interval, interval, interval};
        sgrid.init(in_pdb);
    }

    void set_sites() {
        double *p1 = sgrid.data;
        int d = delta;
        for (int x1 = 0; x1 < sgrid.shape[0]; x1++) {
            for (int y1 = 0; y1 < sgrid.shape[1]; y1++) {
                for (int z1 = 0; z1 < sgrid.shape[2]; z1++) {
                    double n = 0;
                    double m = 0;
                    // *p1 > 0 means this point is on the surface of an atom
                    if (*p1 > 0 &&
                        x1 >= d && x1 < sgrid.shape[0] - d &&
                        y1 >= d && y1 < sgrid.shape[1] - d &&
                        z1 >= d && z1 < sgrid.shape[2] - d)
                    {
                        for (int x2 = -d; x2 < d; x2++) {
                            for (int y2 = -d; y2 < d; y2++) {
                                for (int z2 = -d; z2 < d; z2++) {
                                    if (std::sqrt(x2 * x2 + y2 * y2 + z2 * z2) < d) {
                                        if (sgrid.at(x1 + x2, y1 + y2, z1 + z2) >= 0) {
                                            n++;
                                        }
                                        m++;
                                    }
                                }
                            }
                        }
                        double s = n / m;
//                        if (s < volume_ratio) {
                            int site_id = sgrid.index(x1, y1, z1);
                            sites[site_id] = Site {Position {x1, y1, z1}, s, sgrid.bfactor(x1, y1, z1), -1, std::deque<int>{}};
//                        }
                    }
                    p1++;
                }
            }
        }

        int n_sites = sites.size();

        std::vector<int> site_ids(n_sites);
        // Set site_ids
        int i = 0;
        for (auto && p : sites) {
            site_ids[i] = p.first;
            i++;
        }

        // Sort sites
        std::sort(site_ids.begin(), site_ids.end(), [this](int i, int j){
            return sites[i].bfactor < sites[j].bfactor;
        });

        // Erase sites
        for (int i = 0; i < n_sites * cutoff; i++) {
            int id = site_ids[i];
            sites.erase(id);
        }

        // Set site neighbors
        set_site_neighbors();
    }

    void set_site_neighbors() {
        for (auto && p : sites) {
            int site_id1 = p.first;
            auto & site1 = p.second;
            auto & position1 = site1.position;
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    for (int k = -1; k <= 1; k++) {
                        int x = position1[0] + i;
                        int y = position1[1] + j;
                        int z = position1[2] + k;
                        int site_id2 = sgrid.index(x, y, z);
                        if (sites.count(site_id2)) {
                            if (site_id1 != site_id2) {
                                site1.neighbors.push_back(site_id2);
                            }
                        }
                    }
                }
            }
        }
    }

    void write_pdb() {
        // Write PDB
        std::ofstream ofile(outfile.c_str());
        jnc::bio::PdbWriter writer(ofile);
        jnc::bio::PdbChain chain;
        chain.name = "A";
        //        int chain_name = 1;
        for (int i = 0; i < areas.size(); i++) {
            auto &area = areas[i];
            int spot_ind = area.spot_inds[0];
            int spot_id = spot_ids[spot_ind];
            auto &spot = spots[spot_id];
            int site_id = spot.centroid;
            auto &site = sites[site_id];

            std::cout << "Hotspot " << i + 1 << " (" << spot.bfactor << "):";
            //            chain.name = jnc::string_format("%d", chain_name);
            //            chain_name++;

            jnc::bio::PdbResidue residue;
            residue.name = "X";
            auto && c = sgrid.get_coord(site.position[0], site.position[1], site.position[2]);
            std::cout << " " << site.bfactor;

            jnc::bio::PdbAtom atom;
            atom.name = "P";
            atom.bfactor = site.bfactor;
            for (int j = 0; j < 3; j++) atom[j] = c[j];

            residue.push_back(atom);
            chain.push_back(residue);
            std::cout << std::endl;
        }
        writer.write_chain(chain);
        ofile.close();
    }

    void run() {
        set_grid();
        set_sites();

        // Set spots
        set_spots();

        set_areas();

        write_pdb();

    }
};

