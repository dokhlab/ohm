#pragma once

#include "pdb_chain.hpp"
#include "../core/coro.hpp"

namespace jnc {

namespace bio {

class PdbModel : public std::vector<PdbChain> {
public:
  std::string name;
  int num;

  std::vector<const PdbAtom *> patoms() const;

  std::vector<PdbAtom *> patoms();

  std::vector<const PdbResidue *> presidues() const;

  std::vector<PdbResidue *> presidues();

};

template<typename Model_>
generator<PdbAtom> model_atoms(Model_ &&m) {
    for (auto &&chain : m) {
      for (auto &&res : chain) {
        for (auto &&atom : res) {
          co_yield atom;
        }
      }
    }
}

template<typename Model_>
generator<PdbResidue> model_residues(Model_ &&m) {
  for (auto &&chain : m) {
    for (auto &&res : chain) {
      co_yield res;
    }
  }
}

} // namespace bio

} // namespace jnc
