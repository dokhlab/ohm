#pragma once

#include <cfloat>
#include <climits>
#include "mol2.hpp"

namespace jnc {
namespace bio {

class Mol2Writer {
public:

    const Mol2 &m;

    Mol2Writer(const Mol2 &m_) : m(m_) {}

    void write(std::string fn) {
        std::ofstream ofile(fn.c_str());
        write_molecule(ofile);
        write_atoms(ofile);
        write_bonds(ofile);
        write_substructures(ofile);
        ofile.close();
    }

    void write_molecule(std::ostream &out) {
        out << "@<TRIPOS>MOLECULE" << std::endl;
        out << mol_name << std::endl;
        out << num_atoms;
        if (num_bonds != INT_MAX) {
            out << ' ' << num_bonds;
            if (num_subst != INT_MAX) {
                out << ' ' << num_subst;
                if (num_feat != INT_MAX) {
                    out << ' ' << num_feat;
                    if (num_sets != INT_MAX) {
                        out << ' ' << num_sets;
                    }
                }
            }
        }
        out << std::endl;

        out << mol_type << std::endl;
        out << charge_type << std::endl;

        if (!status_bits.empty()) out << status_bits << std::endl;
        if (!mol_comment.empty()) out << mol_comment << std::endl;
    }

    void write_atoms(std::ostream &out) {
        out << "@<TRIPOS>ATOM" << std::endl;
        int i = 0;
        for (auto &&atom : atoms) {
            out << i+1 << ' ' << atom.name << ' ' << atom.x << ' ' << atom.y << ' ' << atom.z << ' ' << atom.type;
            if (atom.subst_id != INT_MAX) {
                out << ' ' << atom.subst_id+1;
                if (!atom.subst_name.empty()) {
                    out << ' ' << atom.subst_name;
                    if (atom.charge != DBL_MAX) {
                        out << ' ' << atom.charge;
                        if (!atom.status_bit.empty()) {
                            out << ' ' << atom.status_bit;
                        }
                    }
                }
            }
            out << std::endl;
            i++;
        }
    }

    void write_bonds(std::ostream &out) {
        if (bonds.empty()) return;

        out << "@<TRIPOS>BOND" << std::endl;
        int i = 0;
        for (auto &&bond : bonds) {
            out << i+1 << ' ' << bond.origin_atom_id << ' ' << bond.target_atom_id << ' ' << bond.type;
            if (!bond.status_bits.empty()) out << ' ' << bond.status_bits;
            out << std::endl;
            i++;
        }
    }

    void write_substructures(std::ostream &out) {
        if (substs.empty()) return;

        out << "@<TRIPOS>SUBSTRUCTURE" << std::endl;
        int i = 0;
        for (auto &&subst : substs) {
            out << i+1 << ' ' << subst.name << ' ' << subst.root_atom;
            if (!subst.type.empty()) {
                out << ' ' << subst.type;
                if (subst.dict_type != INT_MAX) {
                    out << ' ' << subst.dict_type;
                    if (!subst.chain.empty()) {
                        out << ' ' << subst.chain;
                        if (!subst.sub_type.empty()) {
                            out << ' ' << subst.sub_type;
                            if (subst.inter_bonds != INT_MAX) {
                                out << ' ' << subst.inter_bonds;
                                if (!subst.status.empty()) {
                                    out << ' ' << subst.status;
                                    if (!subst.comment.empty()) {
                                        out << ' ' << subst.comment;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            out << std::endl;
            i++;
        }
    }

}; // class Mol2Writer

} // namespace bio
} // namespace jnc

