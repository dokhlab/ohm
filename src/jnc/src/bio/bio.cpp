#include "bio.hpp"
#include <atomic>
#include <string>
#include <unordered_set>

namespace jnc {
namespace bio {

bool is_rna_residue(const std::string &res_name) {
  static std::unordered_set<std::string> rna_residue_names{"A",  "U",  "G",  "C",  "A5", "U5",
                                                           "G5", "C5", "A3", "U3", "G3", "C3"};
  return rna_residue_names.contains(res_name);
}

bool is_protein_residue(const std::string &res_name) {
  static std::unordered_set<std::string> protein_residue_names{"ALA", "ARG", "ASN", "ASP", "CYS", "GLU", "GLN",
                                                               "GLY", "HIS", "ILE", "LEU", "LYS", "MET", "PHE",
                                                               "PRO", "SER", "THR", "TRP", "TYR", "VAL"};
  return protein_residue_names.contains(res_name);
}

bool is_rna(const PdbChain &chain) {
  int nrnas = std::ranges::count_if(chain, [](const auto &r) { return is_rna_residue(r.name); });
  return nrnas >= 1;
}

bool is_protein(const PdbChain &chain) {
  int nrnas = std::ranges::count_if(chain, [](const auto &r) { return is_rna_residue(r.name); });
  return nrnas >= 1;
}

std::string pdb_line(const std::string &record_name, int atom_num, const std::string &atom_name,
                     const std::string &residue_name, const std::string &chain_name, int residue_num, double x,
                     double y, double z, double occupancy, double temperature_factor, const std::string &element,
                     const std::string &charge) {
  return string_format("%-6.6s%5d %4.4s %3.3s%2.2s%4d%12.3f%8.3f%8.3f%6.2f%6.2f%12.12s%2s", record_name, atom_num,
                       atom_name, residue_name, chain_name, residue_num, x, y, z, occupancy, temperature_factor,
                       element, charge);
}

} // namespace bio
} // namespace jnc
