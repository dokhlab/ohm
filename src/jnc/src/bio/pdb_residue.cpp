#include "pdb_writer.hpp"
#include "pdb_residue.hpp"

namespace jnc {

namespace bio {

PdbAtom &PdbResidue::get_atom(std::string name) {
    for (auto &&atom : *this) {
        if (atom.name == name) return atom;
    }
    std::cerr << *this << std::endl;
    JN_DIE("Atom '" + name + "' not found");
}

const PdbAtom &PdbResidue::get_atom(std::string name) const {
    for (auto &&atom : *this) {
        if (atom.name == name) return atom;
    }
    std::cerr << *this << std::endl;
    JN_DIE("Atom '" + name + "' not found");
}

bool PdbResidue::has_atom(std::string name) const {
    for (auto &&atom : *this) {
        if (atom.name == name) return true;
    }
    return false;
}

std::ostream &operator <<(std::ostream &output, const PdbResidue &residue) {
    PdbWriter(output).write_residue(residue);
    return output;
}

}

} // namespace jnc

