#pragma once

#include <iomanip>
#include <iostream>
#include "pdb.hpp"
#include "../core/string.hpp"

namespace jnc {

namespace bio {

class PdbWriter {
public:
    const Vs chain_names = {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
        "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    };

    int atom_num, residue_num, model_num;
    std::string atom_name, residue_name, chain_name, atom_name_label;
    double x, y, z, a, b;

    std::ostream stream;

    PdbWriter() : stream(std::cout.rdbuf()) {
        init();
    }

    PdbWriter(std::ostream &out) : stream(out.rdbuf()) {
        init();
    }

    void init() {
        atom_num = 1;
        residue_num = 1;
        model_num = 1;
        atom_name = "X";
        residue_name = "X";
        chain_name = chain_names[0];
        atom_name_label = "X";
        x = 0;
        y = 0;
        z = 0;
        a = 1;
        b = 0;
    }

    void bind_stream(std::ostream &s) {
        stream.rdbuf(s.rdbuf());
    }

    void write() {
        std::replace(atom_name.begin(), atom_name.end(), '*', '\'');
        if (atom_name == "O1P") atom_name = "OP1";
        if (atom_name == "O2P") atom_name = "OP2";
        if (std::count_if(chain_name.begin(), chain_name.end(), [](char c) {return c != ' '; }) == 0) {
            chain_name = "A";
        }
        stream
            << string_format("ATOM%7d  %-4.4s%3.3s%2.2s%4d%12.3f%8.3f%8.3f%6.2f%6.2f%12.12s  ",
                             atom_num, atom_name, residue_name, chain_name, residue_num, x, y, z, a, b, atom_name_label)
            << std::endl;
    }

    template<typename Atom_>
    void read_atom(const Atom_ &atom) {
        atom_name = atom.name;
        x = atom[0];
        y = atom[1];
        z = atom[2];
        atom_name_label = atom.name.substr(0, 1);
        b = atom.bfactor;
    }

    template<typename F>
    void write_model_callback(F &&f) {
        write_model_begin();
        f();
        write_model_end();
        model_num++;
        atom_num = 1;
        residue_name = "X";
    }

    template<typename Atom_>
    void write_atom(const Atom_ &atom) {
        read_atom(atom);
        write();
        atom_num++;
    }

    template<typename Residue_>
    void write_residue(const Residue_ &residue) {
        residue_name = residue.name;
        if (residue.num != -1) {
            residue_num = residue.num;
        }
        for (auto && atom : residue) {
            write_atom(atom);
        }
        residue_num++;
    }

    template<typename Chain_>
    void write_chain(const Chain_ &chain) {
        chain_name = chain.name;
        for (auto &&residue : chain) {
            write_residue(residue);
        }
        write_chain_end();
        residue_num = 1;
        auto it = std::find(chain_names.begin(), chain_names.end(), chain_name);
        chain_name = ((it == chain_names.end() || std::next(it) == chain_names.end()) ? chain_names[0] : (*std::next(it)));
    }

    template<typename Model_>
    void write_model(const Model_ &model) {
        write_model_callback([&]() {
            for (auto && chain : model) {
                this->write_chain(chain);
            }
        });
    }

    template<typename Pdb_>
    void write_pdb(const Pdb_ &mol) {
        for (auto && model : mol) {
            write_model(model);
        }
        write_file_end();
    }

    void write_model_begin() {
        stream
            << std::left
            << std::setw(13) << "MODEL"
            << model_num
            << std::right
            << std::endl;
    }

    void write_model_end() {
        stream << "ENDMDL" << std::endl;
    }

    void write_file_end() {
        stream << "END" << std::endl;
    }

    void write_chain_end() {
        stream
            << "TER "
            << std::setw(7) << atom_num
            << "  "
            << std::left
            << std::setw(4) << " "
            << std::right
            << std::setw(3) << residue_name
            << std::setw(2) << chain_name
            << std::setw(4) << residue_num - 1
            << std::endl;
        atom_num++;
    }

};

} // namespace bio

} // namespace jnc

