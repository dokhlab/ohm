#include "pdb_writer.hpp"
#include "pdb_atom.hpp"

namespace jnc {

namespace bio {

int PdbAtom::get_element_type() const {
    if (element == "X") {
        return ::jnc::bio::get_atom_element(name);
    }
    else {
        return ::jnc::get_atom_element(element);
    }
}

std::ostream &operator <<(std::ostream &output, const PdbAtom &atom) {
    PdbWriter(output).write_atom(atom);
    return output;
}

}

} // namespace jnc

