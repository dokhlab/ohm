#pragma once

#include "mol2_types.hpp"
#include "bio_types.hpp"

namespace jnc {
namespace bio {

struct Mol2 {
    std::string mol_name;

    int num_atoms;
    int num_bonds = INT_MAX;
    int num_subst = INT_MAX;
    int num_feat = INT_MAX;
    int num_sets = INT_MAX;

    std::string mol_type;
    std::string charge_type;
    std::string status_bits = "";
    std::string mol_comment = "";

    Mol2Atoms atoms;
    Mol2Bonds bonds;
    Mol2Substructures substructures;

    Mol2() {}

    Mol2(const std::string &filename);

    void read(const std::string &filename);

    static std::vector<Mol2> read_multiple(const std::string &filename);

    void remove_hydrogens();

    void write(std::string fn);

    void write(std::ostream &out);

    void write_molecule(std::ostream &out);

    void write_atoms(std::ostream &out);

    void write_bonds(std::ostream &out);

    void write_substructures(std::ostream &out);

};

} // namespace bio
} // namespace jnc

