#include "mol2_types.hpp"
#include "../core/string.hpp"

namespace jnc {
namespace bio {

using namespace std;

int get_atom_element(string atom_type) {
#define ELT(atom_type_name, atom_type, atom_element) {atom_type_name, atom_element}
#define SEP ,
    map<string, int> type_map { JN_MOL2_ATOM_TABLE };
#undef SEP
#undef ELT
    // try {
        return type_map.at(string_upper_copy(atom_type));
    // }
    // catch (...) {
    //     cerr << "Warning: Unknow mol2 atom type: " << atom_type << endl;
    // }
}

} // namespace bio
} // namespace jnc

