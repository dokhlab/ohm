#pragma once

#include "pdb_residue.hpp"
#include "../core/coro.hpp"

namespace jnc {

namespace bio {

class PdbChain : public std::vector<PdbResidue> {
public:
    std::string name;

    PdbChain() {}

    PdbChain(const std::string &filename) {
        read(filename);
    }

    void read(const std::string &filename);

    std::vector<const PdbAtom *> patoms() const;

    std::vector<PdbAtom *> patoms();

};

std::ostream &operator <<(std::ostream &output, const PdbChain &chain);

template<typename Chain_>
generator<PdbAtom> chain_atoms(Chain_ &&c) {
    for (auto &&res : c) {
        for (auto &&atom : res) {
            co_yield atom;
        }
    }
}

} // namespace bio

} // namespace jnc

