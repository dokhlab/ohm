#pragma once

#include <cfloat>
#include <climits>
#include <string>
#include <map>
#include <vector>
#include <array>
#include "mol2_macros.hpp"
#include "bio_types.hpp"

namespace jnc {
namespace bio {

#define JN_MOL2_ATOM_TABLE \
    ELT("H"       , ATOM_TYPE_H    , ATOM_ELEMENT_H) SEP \
    ELT("H.SPC"   , ATOM_TYPE_H_SPC, ATOM_ELEMENT_H) SEP \
    ELT("H.T3P"   , ATOM_TYPE_H_T3P, ATOM_ELEMENT_H) SEP \
\
    ELT("C.1"     , ATOM_TYPE_C_1  , ATOM_ELEMENT_C) SEP \
    ELT("C.2"     , ATOM_TYPE_C_2  , ATOM_ELEMENT_C) SEP \
    ELT("C.3"     , ATOM_TYPE_C_3  , ATOM_ELEMENT_C) SEP \
    ELT("C.AR"    , ATOM_TYPE_C_AR , ATOM_ELEMENT_C) SEP \
    ELT("C.CAT"   , ATOM_TYPE_C_CAT, ATOM_ELEMENT_C) SEP \
\
    ELT("N.1"     , ATOM_TYPE_N_1  , ATOM_ELEMENT_N) SEP \
    ELT("N.2"     , ATOM_TYPE_N_2  , ATOM_ELEMENT_N) SEP \
    ELT("N.3"     , ATOM_TYPE_N_3  , ATOM_ELEMENT_N) SEP \
    ELT("N.4"     , ATOM_TYPE_N_4  , ATOM_ELEMENT_N) SEP \
    ELT("N.AR"    , ATOM_TYPE_N_AR , ATOM_ELEMENT_N) SEP \
    ELT("N.AM"    , ATOM_TYPE_N_AM , ATOM_ELEMENT_N) SEP \
    ELT("N.PL3"   , ATOM_TYPE_N_PL3, ATOM_ELEMENT_N) SEP \
\
    ELT("O.2"     , ATOM_TYPE_O_2  , ATOM_ELEMENT_O) SEP \
    ELT("O.3"     , ATOM_TYPE_O_3  , ATOM_ELEMENT_O) SEP \
    ELT("O.CO2"   , ATOM_TYPE_O_CO2, ATOM_ELEMENT_O) SEP \
    ELT("O.T3P"   , ATOM_TYPE_O_T3P, ATOM_ELEMENT_O) SEP \
    ELT("O.SPC"   , ATOM_TYPE_O_SPC, ATOM_ELEMENT_O) SEP \
\
    ELT("P.3"     , ATOM_TYPE_P_3  , ATOM_ELEMENT_P) SEP \
\
    ELT("S.2"     , ATOM_TYPE_S_2  , ATOM_ELEMENT_S) SEP \
    ELT("S.3"     , ATOM_TYPE_S_3  , ATOM_ELEMENT_S) SEP \
    ELT("S.O"     , ATOM_TYPE_S_O  , ATOM_ELEMENT_S) SEP \
    ELT("S.O2"    , ATOM_TYPE_S_O2 , ATOM_ELEMENT_S) SEP \
\
    ELT("K"       , ATOM.TYPE_K    , ATOM_ELEMENT_K ) SEP \
    ELT("F"       , ATOM.TYPE_F    , ATOM_ELEMENT_F ) SEP \
    ELT("I"       , ATOM.TYPE_I    , ATOM_ELEMENT_I ) SEP \
    ELT("LP"      , ATOM.TYPE_LP   , ATOM_ELEMENT_LP) SEP \
    ELT("MG"      , ATOM.TYPE_MG   , ATOM_ELEMENT_MG) SEP \
    ELT("CR.OH"   , ATOM_TYPE_CR_OH, ATOM_ELEMENT_CR) SEP \
    ELT("CR.TH"   , ATOM_TYPE_CR_TH, ATOM_ELEMENT_CR) SEP \
    ELT("CO.OH"   , ATOM_TYPE_CO_OH, ATOM_ELEMENT_CO) SEP \
    ELT("CL"      , ATOM.TYPE_CL   , ATOM_ELEMENT_CL) SEP \
    ELT("SE"      , ATOM.TYPE_SE   , ATOM_ELEMENT_SE) SEP \
    ELT("NA"      , ATOM.TYPE_NA   , ATOM_ELEMENT_NA) SEP \
    ELT("SI"      , ATOM.TYPE_SI   , ATOM_ELEMENT_SI) SEP \
    ELT("FE"      , ATOM.TYPE_FE   , ATOM_ELEMENT_FE) SEP \
    ELT("ZN"      , ATOM.TYPE_ZN   , ATOM_ELEMENT_ZN) SEP \
    ELT("SN"      , ATOM.TYPE_SN   , ATOM_ELEMENT_SN) SEP \
    ELT("LI"      , ATOM.TYPE_LI   , ATOM_ELEMENT_LI) SEP \
    ELT("AL"      , ATOM.TYPE_AL   , ATOM_ELEMENT_AL) SEP \
    ELT("CA"      , ATOM.TYPE_CA   , ATOM_ELEMENT_CA) SEP \
    ELT("MN"      , ATOM.TYPE_MN   , ATOM_ELEMENT_MN) SEP \
    ELT("CU"      , ATOM.TYPE_CU   , ATOM_ELEMENT_CU) SEP \
    ELT("BR"      , ATOM.TYPE_BR   , ATOM_ELEMENT_BR) SEP \
    ELT("MO"      , ATOM.TYPE_MO   , ATOM_ELEMENT_MO) SEP \
\
    ELT("DU"      , ATOM.TYPE_DU   , ATOM_ELEMENT_X) SEP \
    ELT("ANY"     , ATOM.TYPE_ANY  , ATOM_ELEMENT_X) SEP \
    ELT("DU.C"    , ATOM_TYPE_DU_C , ATOM_ELEMENT_X) SEP \
    ELT("HAL"     , ATOM.TYPE_HAL  , ATOM_ELEMENT_X) SEP \
    ELT("HET"     , ATOM.TYPE_HET  , ATOM_ELEMENT_X) SEP \
    ELT("HEV"     , ATOM.TYPE_HEV  , ATOM_ELEMENT_X)

int get_atom_element(std::string atom_type);

/**
 * MolType
 */
class MolType {
public:
    enum {
#define ELT(name, type) type
#define SEP ,
        MOL2_MOL_TYPE_MAP
#undef ELT
#undef SEP
    };

    static std::string get_name(int id) {
        std::map<int, std::string> m {
#define ELT(name, type) {type, #name}
#define SEP ,
            MOL2_MOL_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[id];
    }

    static int get_id(const std::string &name) {
        static std::map<std::string, int> m {
#define ELT(name, type) {#name, type}
#define SEP ,
            MOL2_MOL_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[name];
    }
};

/**
 * Charge Type
 */
class Mol2ChargeType {
public:
    enum {
#define ELT(name, type) type
#define SEP ,
        MOL2_CHARGE_TYPE_MAP
#undef ELT
#undef SEP
    };

    static std::string get_name(int id) {
        std::map<int, std::string> m {
#define ELT(name, type) {type, #name}
#define SEP ,
            MOL2_CHARGE_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[id];
    }

    static int get_id(const std::string &name) {
        std::map<std::string, int> m {
#define ELT(name, type) {#name, type}
#define SEP ,
            MOL2_CHARGE_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[name];
    }
};

/**
 * Bond Type
 */
class Mol2BondType {
public:
    enum {
#define ELT(name, type, annotation) type
#define SEP ,
        MOL2_BOND_TYPE_MAP
#undef ELT
#undef SEP
    };

    static std::string get_name(int id) {
        std::map<int, std::string> m {
#define ELT(name, type, annotation) {type, name}
#define SEP ,
            MOL2_BOND_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[id];
    }

    static int get_id(const std::string &name) {
        std::map<std::string, int> m {
#define ELT(name, type, annotation) {name, type}
#define SEP ,
            MOL2_BOND_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[name];
    }
};

/**
 * Mol2BondStatus
 */
class Mol2BondStatus {
public:
    enum {
#define ELT(n, status) status = n
#define SEP ,
        MOL2_BOND_STATUS_MAP
#undef ELT
#undef SEP
    };

    static std::string get_name(int id) {
        std::map<int, std::string> m {
#define ELT(n, status) {status, #status}
#define SEP ,
            MOL2_BOND_STATUS_MAP
#undef ELT
#undef SEP
        };
        return m[id];

    }

    static int get_id(const std::string &name) {
        std::map<std::string, int> m {
#define ELT(n, status) {#status, status}
#define SEP ,
            MOL2_BOND_STATUS_MAP
#undef ELT
#undef SEP
        };
        return m[name];

    }
};

/**
 * Mol2AtomType
 */
class Mol2AtomType {
public:
    enum {
#define ELT(name, type, annotation) type
#define SEP ,
        MOL2_ATOM_TYPE_MAP
#undef ELT
#undef SEP
    };

    static std::string get_name(int id) {
        std::map<int, std::string> m {
#define ELT(name, type, annotation) {type, #name}
#define SEP ,
            MOL2_ATOM_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[id];

    }

    static int get_id(const std::string &name) {
        std::map<std::string, int> m {
#define ELT(name, type, annotation) {#name, type}
#define SEP ,
            MOL2_ATOM_TYPE_MAP
#undef ELT
#undef SEP
        };
        return m[name];
    }
};

/**
 * Mol2AtomStatus related definitions
 */
class Mol2AtomStatus {
public:
    enum {
#define ELT(n, status) status = n
#define SEP ,
        MOL2_ATOM_STATUS_MAP
#undef ELT
#undef SEP
    };

    static std::string get_name(int id) {
        std::map<int, std::string> m {
#define ELT(n, status) {status, #status}
#define SEP ,
            MOL2_ATOM_STATUS_MAP
#undef ELT
#undef SEP
        };
        return m[id];

    }

    static int get_id(const std::string &name) {
        std::map<std::string, int> m {
#define ELT(n, status) {#status, status}
#define SEP ,
            MOL2_ATOM_STATUS_MAP
#undef ELT
#undef SEP
        };
        return m[name];

    }
};

struct Mol2Bond {
    int origin_atom_id;
    int target_atom_id;
    std::string type;
    std::string status_bits = "";
};
using Mol2Bonds = std::vector<Mol2Bond>;

struct Mol2Atom {
    int id;
    std::string name;
    double x;
    double y;
    double z;
    std::string type;
    int subst_id = INT_MAX;
    std::string subst_name = "";
    double charge = DBL_MAX;
    std::string status_bit = "";
};
using Mol2Atoms = std::vector<Mol2Atom>;

struct Mol2Substructure {
    std::string name;
    int root_atom;
    std::string type;
    int dict_type = INT_MAX;
    std::string chain;
    std::string sub_type;
    int inter_bonds = INT_MAX;
    std::string status;
    std::string comment;
};
using Mol2Substructures = std::vector<Mol2Substructure>;

/**
 * I tried to implement the program to perceive the atom types and bond orders according to atom
 * coordinates and element types. But it seems to be hard to finish. Please
 * refer to: "Labute, P., 2005. On the perception of molecules from 3D atomic
 * coordinates. Journal of chemical information and modeling, 45(2),
 * pp.215-221."
 *
 * Jian Wang (jianopt@gmail.com)
 */

/*
void set_c_types(int i, vector<int> &types, const vector<int> &elements, const vector<array<double, 3>> &coords, const vector<vector<int>> &neighbors) {
    if (neighbors[i].size() == 4) {
        types[i] = MOL2_ATOM_C_3;
    }
    else if (neighbors[i].size() == 3) {
        if (is_planar(i, neighbors[i], coords)) {
            if (all_of(neighbors[i].begin(), neighbors[i].end(), [](int j){
                return elements[j] == ATOM_ELEMENT_N;
            })) {
                types[i] = MOL2_ATOM_C_CAT;
                for (auto && j : neighbors[i]) {
                    types[j] = MOL2_ATOM_N_PL3;
                }
            }
            else {
                types[i] = MOL2_ATOM_C_2;
            }
        }
        else {
            types[i] = MOL2_ATOM_C_3;
        }
    }
    else if (neighbors[i].size() == 2) {
        if (is_line()) {
            types[i] = MOL2_ATOM_C_1;
        }
        else if (angle) {
            types[i] = MOL2_ATOM_C_2;
        }
        else if (angle) {
            types[i] = MOL2_ATOM_C_3;
        }
    }
    else if (neighbors[i].size() == 1) {
        if (is_line()) {
            types[i] = MOL2_ATOM_C_1;
        }
        else if () {
            types[i] = MOL2_ATOM_C_2;
        }
        else {
            types[i] = MOL2_ATOM_C_3;
        }
    }
}

void set_n_types(int i, vector<int> &types, const vector<int> &elements, const vector<array<double, 3>> &coords, const vector<vector<int>> &neighbors) {
    if (neighbors[i].size() == 3) {
        types[i] = MOL2_ATOM_N_3;
    }
    else if (neighbors[i].size() == 2) {
        if (is_planar(i, neighbors[i], coords)) {
            types[i] = MOL2_ATOM_N_2;
        }
        else {
            types[i] = MOL2_ATOM_N_3;
        }
    }
    else if (neighbors[i].size() == 1) {
        if (is_planar(i, neighbors[i], coords)) {
            types[i] = MOL2_ATOM_N_2;
        }
        else if () {
            types[i] = MOL2_ATOM_N_3;
        }
        else {
            types[i] = MOL2_ATOM_N_1;
        }
    }
}

void set_o_types(int i, vector<int> &types, const vector<int> &elements, const vector<array<double, 3>> &coords, const vector<vector<int>> &neighbors) {
    if (neighbors[i].size() == 2) {
        types[i] = MOL2_ATOM_O_3;
    }
    else if (neighbors[i].size() == 1) {
        if () {
            types[i] = MOL2_ATOM_N_2;
        }
        else {
            types[i] = MOL2_ATOM_N_3;
        }
    }
}

void set_s_types(int i, vector<int> &types, const vector<int> &elements, const vector<array<double, 3>> &coords, const vector<vector<int>> &neighbors) {
}

void set_p_types(int i, vector<int> &types, const vector<int> &elements, const vector<array<double, 3>> &coords, const vector<vector<int>> &neighbors) {
}

template<typename Atom_>
vector<int> get_types(const vector<int> &elements, const vector<array<double, 3>> &coords) {
    int n = elements.size();

    auto rings = get_rings(coords);

    vector<int> types(n, -1);

    auto neighbors = find_neighbors(coords);

    // Set aromatic atoms
    for (auto && ring : rings) {
        if (is_aromatic(ring)) {
            for (auto && i : ring) {
                auto element = elements[i];
                if (element == ATOM_ELEMENT_C) {
                    types[i] = MOL2_ATOM_C_AR;
                }
                else if (element == ATOM_ELEMENT_N) {
                    if (neighbors[i].size() == 2) {
                        types[i] = MOL2_ATOM_N_AR;
                    }
                    else if (neighbors[i].size() == 3) {
                        types[i] = MOL2_ATOM_N_PL3;
                    }
                }
            }
        }
    }

    for (int i = 0; i < n; i++) {
        if (types[i] == -1) {
            else if (element[i] == ATOM_ELEMENT_N) {
            }
            else if (element[i] == ATOM_ELEMENT_O) {
            }
            else if (element[i] == ATOM_ELEMENT_H) {
            }
            else if (element[i] == ATOM_ELEMENT_S) {
            }
            else if (element[i] == ATOM_ELEMENT_P) {
            }
        }
    }

    return move(types);
}
*/

} // namespace bio
} // namespace jnc

