#pragma once

#include <iostream>
#include <string>
#include <array>

namespace jnc {

namespace bio {

class PdbAtom : public std::array<double, 3> {
public:
    std::string name;
    std::string type;
    std::string element;
    int num;
    double charge;
    double bfactor = 0;
    bool is_std = true;

    int get_element_type() const;
};

std::ostream &operator <<(std::ostream &output, const PdbAtom &atom);

}

} // namespace jnc

