#pragma once

#include <string>
#include "rna_score_dih.hpp"
#include "rna_score_dist.hpp"

namespace jnc {

class RNAScorer {
public:
    DistAnal * m_dist_anal = NULL;
    DihAnal  * m_dih_anal = NULL;

    std::string m_par_dist;
    std::string m_par_dih;

    double m_bin_dist = 0.3;
    double m_bin_dih = 4.5;
    double m_cutoff = 20;

    double m_constant = 27.1118;
    double m_weight_dist = 0.433513;
    double m_weight_dih = 1.59348;

    RNAScorer(const std::string &lib) {
        m_par_dist = lib + "/dist.frequencies";
        m_par_dih = lib + "/par_dih";

        m_dist_anal = new DistAnal;
        m_dist_anal->init(m_bin_dist, m_cutoff);
        m_dist_anal->read_freqs(m_par_dist);

        m_dih_anal = new DihAnal(m_bin_dih);
        m_dih_anal->read_parm(m_par_dih);
    }

    ~RNAScorer() {
        if (m_dist_anal != NULL) delete m_dist_anal;
        if (m_dih_anal != NULL) delete m_dih_anal;
    }

    template<typename Chain_>
    double dist_score(const Chain_ &chain) {
        return m_dist_anal->score(chain);
    }

    template<typename Chain_>
    double dih_score(const Chain_ &chain) {
        return m_dih_anal->score(chain);
    }

    template<typename Chain_>
    double score(const Chain_ &chain) {
        double score_dist = m_dist_anal->score(chain);
        double score_dih = m_dih_anal->score(chain);

        return m_constant + score_dist * m_weight_dist + score_dih * m_weight_dih;
    }

    template<typename Chain_>
    void train(const Chain_ &c) {
        m_dist_anal->train(c);
    }

    void print_counts(std::ostream & stream) const {
        m_dist_anal->print_counts(stream);
    }

    void print_freqs(std::ostream & stream) const {
        m_dist_anal->print_freqs(stream);
    }

    template<typename Residue_>
    double en_stacking(const Residue_ &r1, const Residue_ &r2) {
        return m_dist_anal->en_stacking(r1, r2);
    }

    template<typename Residue_>
    double en_pairing(const Residue_ &r1, const Residue_ &r2) {
        return m_dist_anal->en_pairing(r1, r2);
    }
};

}

