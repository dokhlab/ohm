#include "bio_types.hpp"
#include <map>

namespace jnc {

using namespace std;

int get_atom_element(const std::string &atom_name) {
#define ELT(atom_name, atom_type) {atom_name, atom_type}
#define SEP ,
    static map<string, int> atom_map { JN_BIO_ATOM_TABLE };
#undef SEP
#undef ELT
    try {
        return atom_map.at(atom_name);
    }
    catch (...) {
        return ATOM_ELEMENT_X;
    }
}

std::string get_element_name(int element) {
#define ELT(atom_name, atom_type) {atom_type, atom_name}
#define SEP ,
    static map<int, string> table { JN_BIO_ATOM_TABLE };
#undef SEP
#undef ELT
    try {
        return table.at(element);
    }
    catch (...) {
        return "X";
    }
}

} // namespace jnc
