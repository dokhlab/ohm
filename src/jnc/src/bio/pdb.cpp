#include "../core/string.hpp"
#include "pdb.hpp"
#include "pdb_reader.hpp"
#include "pdb_writer.hpp"
#include <cctype>
#include <iomanip>

namespace jnc {

namespace bio {

Pdb::Pdb(const std::string &filename) { read(filename); }

void Pdb::read(const std::string &filename) {
  // Clear the Pdb
  clear();

  PdbReader pdb_reader(*this);
  pdb_reader.read(filename);
}

void Pdb::remove_hydrogens() {
  for (auto &&model : *this) {
    for (auto &&chain : model) {
      for (auto &&res : chain) {
        auto r = res;
        r.clear();
        for (auto &&atom : res) {
          //                    if (atom.name[0] != 'H' && (!std::isdigit(atom.name[0]) || atom.name[1] != 'H')) {
          if (atom.name[0] != 'H' && atom.name[1] != 'H') {
            r.push_back(atom);
          }
        }
        res = std::move(r);
      }
    }
  }
}

void Pdb::sort() {
  for (auto &&model : *this) {
    for (auto &&chain : model) {
      for (auto &&res : chain) {
        std::sort(res.begin(), res.end(), [](const auto &a1, const auto &a2) { return a1.name < a2.name; });
      }
    }
  }
}

std::ostream &operator<<(std::ostream &output, const Pdb &pdb) {
  PdbWriter pdb_writer(output);
  pdb_writer.write_pdb(pdb);
  return output;
}

Pdb read_pdb(const std::string &filename) {
  Pdb pdb;
  PdbReader pdb_reader(pdb);
  pdb_reader.read(filename);
  return std::move(pdb);
}

Pdb read_pdb(std::istream &ifile, const std::string &model_name) {
  Pdb pdb;
  PdbReader pdb_reader(pdb);
  pdb_reader.read(ifile, model_name);
  return std::move(pdb);
}


} // namespace bio
} // namespace jnc
