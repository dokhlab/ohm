#include "rna_score_dist.hpp"

namespace jnc {

#define PRINT_MAT3(a, stream) do { \
	int i, j, k; \
	for (i = 0; i < 85; i++) {\
		for (j = 0; j < 85; j++) {\
			for (k = 0; k < bins; k++) {\
				stream << a[(i * 85 + j) * bins + k] << ' ';\
			}\
			stream << std::endl;\
		}\
	}\
} while (0)


void DistAnal::read_freqs(const std::string &filename) {
    std::ifstream ifile(filename.c_str());
    for (int i = 0; i < 85; i++) {
        for (int j = 0; j < 85; j++) {
            for (int k = 0; k < bins; k++) {
                ifile >> m_freqs[(i * 85 + j) * bins + k];
            }
        }
    }
    ifile.close();
}

void DistAnal::print_freqs(std::ostream & stream) const {
	PRINT_MAT3(m_freqs, stream);
}

void DistAnal::print_counts(std::ostream & stream) const {
	PRINT_MAT3(m_counts, stream);
}


}

