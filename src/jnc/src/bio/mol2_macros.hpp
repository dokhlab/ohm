#pragma once

#define MOL2_MOL_TYPE_MAP \
    ELT(SMALL        ,/*MOL_TYPE_*/SMALL       )SEP \
    ELT(BIOPOLYMER   ,/*MOL_TYPE_*/BIOPOLYMER  )SEP \
    ELT(PROTEIN      ,/*MOL_TYPE_*/PROTEIN     )SEP \
    ELT(NUCLEIC_ACID ,/*MOL_TYPE_*/NUCLEIC_ACID)SEP \
    ELT(SACCHARIDE   ,/*MOL_TYPE_*/SACCHARIDE  )SEP 

#define MOL2_CHARGE_TYPE_MAP \
    ELT(NO_CHARGES      ,/*CHARGE_TYPE_*/NO_CHARGES      )SEP \
    ELT(DEL_RE          ,/*CHARGE_TYPE_*/DEL_RE          )SEP \
    ELT(GASTEIGER       ,/*CHARGE_TYPE_*/GASTEIGER       )SEP \
    ELT(GAST_HUCK       ,/*CHARGE_TYPE_*/GAST_HUCK       )SEP \
    ELT(HUCKEL          ,/*CHARGE_TYPE_*/HUCKEL          )SEP \
    ELT(PULLMAN         ,/*CHARGE_TYPE_*/PULLMAN         )SEP \
    ELT(GAUSS80_CHARGES ,/*CHARGE_TYPE_*/GAUSS80_CHARGES )SEP \
    ELT(AMPAC_CHARGES   ,/*CHARGE_TYPE_*/AMPAC_CHARGES   )SEP \
    ELT(MULLIKEN_CHARGES,/*CHARGE_TYPE_*/MULLIKEN_CHARGES)SEP \
    ELT(DICT_CHARGES    ,/*CHARGE_TYPE_*/DICT_CHARGES   )SEP \
    ELT(MMFF94_CHARGES  ,/*CHARGE_TYPE_*/MMFF94_CHARGES  )SEP \
    ELT(USER_CHARGES    ,/*CHARGE_TYPE_*/USER_CHARGES    )SEP 

#define MOL2_BOND_TYPE_MAP \
    ELT("1"  ,/*BOND_TYPE_*/SINGLE       ,single       )SEP \
    ELT("2"  ,/*BOND_TYPE_*/DOUBLE       ,double       )SEP \
    ELT("3"  ,/*BOND_TYPE_*/TRIPLE       ,triple       )SEP \
    ELT("am" ,/*BOND_TYPE_*/AMIDE        ,amide        )SEP \
    ELT("ar" ,/*BOND_TYPE_*/AROMATIC     ,aromatic     )SEP \
    ELT("du" ,/*BOND_TYPE_*/DUMMY        ,dummy        )SEP \
    ELT("un" ,/*BOND_TYPE_*/UNKNOWN      ,unknown      )SEP \
    ELT("nc" ,/*BOND_TYPE_*/NOTCONNECTED ,not connected)SEP

#define MOL2_BOND_STATUS_MAP \
    ELT(1   ,/*BOND_STATUS_*/TYPECOL      )SEP \
    ELT(2   ,/*BOND_STATUS_*/GROUP        )SEP \
    ELT(4   ,/*BOND_STATUS_*/CAP          )SEP \
    ELT(8   ,/*BOND_STATUS_*/BACKBONE     )SEP \
    ELT(16  ,/*BOND_STATUS_*/DICT         )SEP \
    ELT(32  ,/*BOND_STATUS_*/INTERRES     )SEP

#define MOL2_ATOM_TYPE_MAP \
    ELT(Any     ,/*ATOM_TYPE_*/ANY     ,"any atom"                                                                        )SEP \
    ELT(Hev     ,/*ATOM_TYPE_*/HEV     ,"heavy atom (non hydrogen)"                                                       )SEP \
    ELT(Het     ,/*ATOM_TYPE_*/HET     ,"heteroatom = N, O, S, P"                                                         )SEP \
    ELT(LP      ,/*ATOM_TYPE_*/LP      ,"lone pair"                                                                       )SEP \
    ELT(Du      ,/*ATOM_TYPE_*/DU      ,"dummy atom"                                                                      )SEP \
    ELT(Du.C    ,/*ATOM_TYPE_*/DU_C    ,"dummy carbon"                                                                    )SEP \
\
    ELT(H       ,/*ATOM_TYPE_*/H       ,"hydrogen"                                                                        )SEP \
    ELT(H.spc   ,/*ATOM_TYPE_*/H_SPC   ,"hydrogen in Single Point Charge (SPC) water model"                               )SEP \
    ELT(H.t3p   ,/*ATOM_TYPE_*/H_T3P   ,"hydrogen in Transferable intermolecular Potential (TIP3P) water model"           )SEP \
\
    ELT(C.3     ,/*ATOM_TYPE_*/C_3     ,"carbon sp3"                                                                      )SEP \
    ELT(C.2     ,/*ATOM_TYPE_*/C_2     ,"carbon sp2"                                                                      )SEP \
    ELT(C.1     ,/*ATOM_TYPE_*/C_1     ,"carbon sp"                                                                       )SEP \
    ELT(C.ar    ,/*ATOM_TYPE_*/C_AR    ,"carbon aromatic"                                                                 )SEP \
    ELT(C.cat   ,/*ATOM_TYPE_*/C_CAT   ,"carbocation (C +) used only in a guadinium group"                                )SEP \
\
    ELT(N.4     ,/*ATOM_TYPE_*/N_4     ,"nitrogen sp3 positively charged"                                                 )SEP \
    ELT(N.3     ,/*ATOM_TYPE_*/N_3     ,"nitrogen sp3"                                                                    )SEP \
    ELT(N.2     ,/*ATOM_TYPE_*/N_2     ,"nitrogen sp2 "                                                                   )SEP \
    ELT(N.1     ,/*ATOM_TYPE_*/N_1     ,"nitrogen sp"                                                                     )SEP \
    ELT(N.ar    ,/*ATOM_TYPE_*/N_AR    ,"nitrogen aromatic"                                                               )SEP \
    ELT(N.am    ,/*ATOM_TYPE_*/N_AM    ,"nitrogen amide"                                                                  )SEP \
    ELT(N.pl3   ,/*ATOM_TYPE_*/N_PL3   ,"nitrogen trigonal planar"                                                        )SEP \
\
    ELT(O.3     ,/*ATOM_TYPE_*/O_3     ,"oxygen sp3"                                                                      )SEP \
    ELT(O.2     ,/*ATOM_TYPE_*/O_2     ,"oxygen sp2"                                                                      )SEP \
    ELT(O.co2   ,/*ATOM_TYPE_*/O_CO2   ,"oxygen in carboxylate and phosphate groups"                                      )SEP \
    ELT(O.spc   ,/*ATOM_TYPE_*/O_SPC   ,"oxygen in Single Point Charge (SPC) water model "                                )SEP \
    ELT(O.t3p   ,/*ATOM_TYPE_*/O_T3P   ,"oxygen in Transferable Intermolecular Potential (TIP3P) water model"             )SEP \
\
    ELT(S.3     ,/*ATOM_TYPE_*/S_3     ,"sulfur sp3"                                                                      )SEP \
    ELT(S.2     ,/*ATOM_TYPE_*/S_2     ,"sulfur sp2"                                                                      )SEP \
    ELT(S.O     ,/*ATOM_TYPE_*/S_O     ,"sulfoxide sulfur"                                                                )SEP \
    ELT(S.O2    ,/*ATOM_TYPE_*/S_O2    ,"sulfone sulfur"                                                                  )SEP \
    ELT(P.3     ,/*ATOM_TYPE_*/P_3     ,"phosphorous sp3"                                                                 )SEP \
\
    ELT(Hal     ,/*ATOM_TYPE_*/HAL     ,"halogen: fluorine(F), chlorine(Cl), bromine(Br), iodine(I), astatine(At)"        )SEP \
    ELT(F       ,/*ATOM_TYPE_*/F       ,"fluorine"                                                                        )SEP \
    ELT(Cl      ,/*ATOM_TYPE_*/CL      ,"chlorine"                                                                        )SEP \
    ELT(Br      ,/*ATOM_TYPE_*/BR      ,"bromine"                                                                         )SEP \
    ELT(I       ,/*ATOM_TYPE_*/I       ,"iodine"                                                                          )SEP \
\
    ELT(Li      ,/*ATOM_TYPE_*/LI      ,"lithium"                                                                         )SEP \
    ELT(Na      ,/*ATOM_TYPE_*/NA      ,"sodium"                                                                          )SEP \
    ELT(Mg      ,/*ATOM_TYPE_*/MG      ,"magnesium"                                                                       )SEP \
    ELT(Fe      ,/*ATOM_TYPE_*/FE      ,"iron"                                                                            )SEP \
    ELT(Al      ,/*ATOM_TYPE_*/AL      ,"aluminum"                                                                        )SEP \
    ELT(Si      ,/*ATOM_TYPE_*/SI      ,"silicon"                                                                         )SEP \
    ELT(K       ,/*ATOM_TYPE_*/K       ,"potassium"                                                                       )SEP \
    ELT(Ca      ,/*ATOM_TYPE_*/CA      ,"calcium"                                                                         )SEP \
    ELT(Cr.th   ,/*ATOM_TYPE_*/CR_TH   ,"chromium (tetrahedral)"                                                          )SEP \
    ELT(Cr.oh   ,/*ATOM_TYPE_*/CR_OH   ,"chromium (octahedral)"                                                           )SEP \
    ELT(Co.oh   ,/*ATOM_TYPE_*/CO_OH   ,"cobalt (octahedral)"                                                             )SEP \
    ELT(Mn      ,/*ATOM_TYPE_*/MN      ,"manganese"                                                                       )SEP \
    ELT(Cu      ,/*ATOM_TYPE_*/CU      ,"copper"                                                                          )SEP \
    ELT(Zn      ,/*ATOM_TYPE_*/ZN      ,"zinc"                                                                            )SEP \
    ELT(Se      ,/*ATOM_TYPE_*/SE      ,"selenium"                                                                        )SEP \
    ELT(Mo      ,/*ATOM_TYPE_*/MO      ,"molybdenum"                                                                      )SEP \
    ELT(Sn      ,/*ATOM_TYPE_*/SN      ,"tin"                                                                             )SEP

#define MOL2_ATOM_STATUS_MAP \
    ELT(1   ,/*ATOM_STATUS_*/DSPMOD       )SEP \
    ELT(2   ,/*ATOM_STATUS_*/TYPECOL      )SEP \
    ELT(4   ,/*ATOM_STATUS_*/CAP          )SEP \
    ELT(8   ,/*ATOM_STATUS_*/BACKBONE     )SEP \
    ELT(16  ,/*ATOM_STATUS_*/DICT         )SEP \
    ELT(32  ,/*ATOM_STATUS_*/ESSENTIAL    )SEP \
    ELT(64  ,/*ATOM_STATUS_*/WATER        )SEP \
    ELT(128 ,/*ATOM_STATUS_*/DIRECT       )SEP


