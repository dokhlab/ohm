#include "pdb_writer.hpp"
#include "pdb_chain.hpp"

namespace jnc {

namespace bio {

V<const PdbAtom *> PdbChain::patoms() const {
    V<const PdbAtom *> as;
    for (auto && res : *this) {
        for (auto && atom : res) {
            as.push_back(&atom);
        }
    }
    return std::move(as);
}

V<PdbAtom *> PdbChain::patoms() {
    V<PdbAtom *> as;
    for (auto && res : *this) {
        for (auto && atom : res) {
            as.push_back(&atom);
        }
    }
    return std::move(as);
}

void PdbChain::read(const std::string & fn) {
    Pdb mol(fn);
    for (auto && chain : mol[0]) {
        for (auto && res : chain) {
            push_back(std::move(res));
        }
    }
}

std::ostream &operator <<(std::ostream &output, const PdbChain &chain) {
    PdbWriter l(output);
    l.write_model_begin();
    l.write_chain(chain);
    l.write_model_end();
    l.write_file_end();
    return output;
}

} // namespace bio

} // namespace jnc

