#pragma once

#include <array>
#include <string>
#include "../math/geom.hpp"

namespace jnc {

class DistAnal {
public:
    using Point = std::array<double, 4>;

    double interval;
    double cutoff;
    int bins;
    double penalty = 0;

    int len = 0;
    std::vector<int> num, type, size_nt;
    std::vector<Point *> m_coords;

    std::vector<double> m_freqs;
    std::vector<int> m_counts;

    DistAnal & init(double interval = 0.5, double cutoff = 20) {
        this->interval = interval;
        this->cutoff = cutoff;
        bins = int(ceil(cutoff / interval));
        m_freqs.resize(85 * 85 * bins);
        m_counts.resize(85 * 85 * bins);
        return *this;
    }

    int res_type(const std::string &name) {
        return (name == "A" ? 1 : (name == "U" ? 2 : (name == "G" ? 3 : (name == "C" ? 4 : (name == "T" ? 2 : -1)))));
    }

    template<typename Residue_>
    int atom_type(const Residue_ &r, int k) {
        int t = res_type(r.name);
        return (r[0].name == "P" ? 0 : 3) + (t == 1 ? k : (t == 2 ? 22 + k : (t == 3 ? 42 + k : 65 + k)));
    }

    void free_coords() {
        do {
            for (auto && p : m_coords) {
                delete [] p;
            }
        } while (0);
    }

    ~DistAnal() {
        free_coords();
    }

    template<typename Chain_>
    void read_mol(const Chain_ &chain) {
        len = chain.size();
        num.resize(len);
        type.resize(len);
        size_nt.resize(len);
        free_coords();
        m_coords.resize(len);

        Point p;
        int m = 0, n = 0;
        for (auto && res : chain) {
            m++;
            size_nt[n] = res.size();
            type[n] = res_type(res.name);
            m_coords[n] = new Point[size_nt[n]];
            int k = 0;
            for (auto && atom : res) {
                for (int l = 0; l < 3; l++) m_coords[n][k][l] = atom[l];
                m_coords[n][k][3] = atom_type(res, k);
                //m_coords[n][k][3] = (res[0].name == "P" ? 0 : 3);
                //m_coords[n][k][3] += (type[n] == 1 ? k : (type[n] == 2 ? 22 + k : (type[n] == 3 ? 42 + k : 65 + k)));
                if (atom.name == "O5*" && n != 0) {
                    double dist = distance(m_coords[n][k], p);
                    if (dist > 4) m += 10000;
                }
                else if (atom.name == "O3*") {
                    for (int l = 0; l < 3; l++) p[l] = m_coords[n][k][l];
                }
                k++;
            }
            num[n] = m;
            n++;
        }
    }

    template<typename Chain_>
    DistAnal & train(const Chain_ & c) {
        int i, j, k, l, type1, type2;
        double temp;

        read_mol(c);
        for (i = 0; i < len; i++) {
            for (j = i + 1; j < len; j++) {
                for (k = 0; k < size_nt[i]; k++) {
                    for (l = 0; l < size_nt[j]; l++) {
                        type1 = static_cast<int>(m_coords[i][k][3]);
                        type2 = static_cast<int>(m_coords[j][l][3]);
                        if (num[j] - num[i] == 1 && !in_base(type1) && !in_base(type2)) continue;
                        temp = distance(m_coords[i][k], m_coords[j][l]);
                        if (temp >= cutoff) continue;
                        m_counts[static_cast<int>(m_coords[i][k][3] * 85 + m_coords[j][l][3]) * bins +
                            static_cast<int>(temp / interval)]++;
                        m_counts[static_cast<int>(m_coords[j][l][3] * 85 + m_coords[i][k][3]) * bins +
                            static_cast<int>(temp / interval)]++;
                    }
                }
            }
        }
        return *this;
    }

    bool in_base(int type) {
        return (type > 11 && type < 22) ||
            (type > 33 && type < 42) ||
            (type > 54 && type < 62) ||
            (type > 74 && type < 85);
    }

    template<typename Chain_>
    double score(const Chain_ &chain) {
        int i, j, k, l, type1, type2;
        double a, b, temp;

        read_mol(chain);
        double sc = 0;
        for (i = 0; i < len; i++) {
            for (j = i + 1; j < len; j++) {
                for (k = 0; k < size_nt[i]; k++) {
                    for (l = 0; l < size_nt[j]; l++) {
                        type1 = int(m_coords[i][k][3]);
                        type2 = int(m_coords[j][l][3]);
                        if (num[j] - num[i] == 1 && !in_base(type1) && !in_base(type2)) continue;
                        temp = distance(m_coords[i][k], m_coords[j][l]);
                        if (temp >= cutoff) continue;
                        a = m_freqs[(type1 * 85 + type2) * bins + int(temp / interval)];
                        b = m_freqs[(type2 * 85 + type1) * bins + int(temp / interval)];
                        sc += (a == 0 ? penalty : (-log(a) * ((in_base(type1) && in_base(type2)) ? 2.5 : 1)));
                        sc += (b == 0 ? penalty : (-log(b) * ((in_base(type1) && in_base(type2)) ? 2.5 : 1)));
                    }
                }
            }
        }
        sc = sc / (len * (len - 1));
        return sc;
    }

    void read_freqs(const std::string &);
    void print_freqs(std::ostream &) const;
    void print_counts(std::ostream &) const;

    template<typename Residue_>
    double en_stacking(const Residue_ &r1, const Residue_ &r2) {
        int n1, n2, t1, t2;
        double d, a, b, e;

        e = 0;
        n1 = 0;
        for (auto && a1 : r1) {
            t1 = atom_type(r1, n1);
            n2 = 0;
            for (auto && a2 : r2) {
                t2 = atom_type(r2, n2);
                if (!in_base(t1) && !in_base(t2)) continue;
                d = distance(a1, a2);
                if (d >= cutoff) continue;
                a = m_freqs[(t1 * 85 + t2) * bins + int(d / interval)];
                b = m_freqs[(t2 * 85 + t1) * bins + int(d / interval)];
                e += (a == 0 ? penalty : (-log(a) * ((in_base(t1) && in_base(t2)) ? 2.5 : 1)));
                e += (b == 0 ? penalty : (-log(b) * ((in_base(t1) && in_base(t2)) ? 2.5 : 1)));
                n2++;
            }
            n1++;
        }
        return e;
    }

    template<typename Residue_>
    double en_pairing(const Residue_ &r1, const Residue_ &r2) {
        int n1, n2, t1, t2;
        double d, a, b, e;

        e = 0;
        n1 = 0;
        for (auto && a1 : r1) {
            t1 = atom_type(r1, n1);
            n2 = 0;
            for (auto && a2 : r2) {
                t2 = atom_type(r2, n2);
                d = distance(a1, a2);
                if (d >= cutoff) continue;
                a = m_freqs[(t1 * 85 + t2) * bins + int(d / interval)];
                b = m_freqs[(t2 * 85 + t1) * bins + int(d / interval)];
                e += (a == 0 ? penalty : (-log(a) * ((in_base(t1) && in_base(t2)) ? 2.5 : 1)));
                e += (b == 0 ? penalty : (-log(b) * ((in_base(t1) && in_base(t2)) ? 2.5 : 1)));
                n2++;
            }
            n1++;
        }
        return e;
    }

};

}


