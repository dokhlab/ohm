#include "pdb_writer.hpp"
#include "pdb_model.hpp"
#include "coroutine"

namespace jnc {

namespace bio {

V<const PdbAtom *> PdbModel::patoms() const {
    V<const PdbAtom *> as;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            for (auto && atom : res) {
                as.push_back(&atom);
            }
        }
    }
    return std::move(as);
}

V<PdbAtom *> PdbModel::patoms() {
    V<PdbAtom *> as;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            for (auto && atom : res) {
                as.push_back(&atom);
            }
        }
    }
    return std::move(as);
}

V<const PdbResidue *> PdbModel::presidues() const {
    V<const PdbResidue *> rs;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            rs.push_back(&res);
        }
    }
    return std::move(rs);
}

V<PdbResidue *> PdbModel::presidues() {
    V<PdbResidue *> rs;
    for (auto && chain : *this) {
        for (auto && res : chain) {
            rs.push_back(&res);
        }
    }
    return std::move(rs);
}

std::ostream &operator <<(std::ostream &output, const PdbModel &model) {
    PdbWriter l(output);
    l.write_model(model);
    l.write_file_end();
    return output;
}

} // namespace bio

} // namespace jnc

