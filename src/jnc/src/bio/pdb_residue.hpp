#pragma once

#include <vector>
#include "pdb_atom.hpp"

namespace jnc {

namespace bio {

class PdbResidue : public std::vector<PdbAtom> {
public:
    std::string name;
    int num = -1;
    bool is_std = true;

    bool has_atom(std::string name) const;

    PdbAtom &get_atom(std::string name);

    const PdbAtom &get_atom(std::string name) const;

    PdbAtom &operator [](const std::string &atom_name) {
        return get_atom(atom_name);
    }

    const PdbAtom &operator [](const std::string &atom_name) const {
        return get_atom(atom_name);
    }

    PdbAtom &operator [](int i) {
        return this->at(i);
    }

    const PdbAtom &operator [](int i) const {
        return this->at(i);
    }

};

std::ostream &operator <<(std::ostream &output, const PdbResidue &residue);

}

} // namespace jnc

