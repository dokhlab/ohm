#pragma once

#include <map>
#include <string>
#include <vector>
#include <functional>
#include <algorithm>
#include <numeric>

namespace jnc {

class RnaNames {
public:
    std::vector<std::string> residue_names {"A", "U", "G", "C"};

    std::map<std::string, std::vector<std::string>> residue_atoms;

    std::map<std::string, std::map<std::string, int>> residue_atom_map;

    static RnaNames *instance() {
        static RnaNames rna_names;
        return &rna_names;
    }

private:
    RnaNames() {
        residue_atoms["A"] = std::vector<std::string> {"P", "OP1", "OP2", "O5'", "C5'", "C4'", "O4'", "C3'", "O3'", "C2'", "O2'", "C1'", "N9", "C8", "N7", "C5", "C6", "N6", "N1", "C2", "N3", "C4"};
        residue_atoms["U"] = std::vector<std::string> {"P", "OP1", "OP2", "O5'", "C5'", "C4'", "O4'", "C3'", "O3'", "C2'", "O2'", "C1'", "N1", "C2", "O2", "N3", "C4", "O4", "C5", "C6"};
        residue_atoms["G"] = std::vector<std::string> {"P", "OP1", "OP2", "O5'", "C5'", "C4'", "O4'", "C3'", "O3'", "C2'", "O2'", "C1'", "N9", "C8", "N7", "C5", "C6", "O6", "N1", "C2", "N2", "N3", "C4"};
        residue_atoms["C"] = std::vector<std::string> {"P", "OP1", "OP2", "O5'", "C5'", "C4'", "O4'", "C3'", "O3'", "C2'", "O2'", "C1'", "N1", "C2", "O2", "N3", "C4", "N4", "C5", "C6"};

        for (auto && type : {"A", "U", "G", "C"}) {
            residue_atom_map[type] = std::map<std::string, int> {};;
            for (int i = 0; i < residue_atoms[type].size(); i++) {
                residue_atom_map[type][residue_atoms[type][i]] = i;
            }
        }
    }
};

template<typename Residue_>
void sort_rna_residue(Residue_ &residue) {
    int n = residue.size();
    auto & names = RnaNames::instance()->residue_names;

    if (std::any_of(names.begin(), names.end(), [&residue](const std::string &name){
        return name == residue.name;
    })) {
        auto & map = RnaNames::instance()->residue_atom_map[residue.name];

        std::vector<int> indices(n);
        std::iota(indices.begin(), indices.end(), 0);

        std::vector<int> ranks(n);
        for (int i = 0; i < n; i++) {
            ranks[i] = map[residue[i].name];
        }

        std::sort(indices.begin(), indices.end(), [&ranks](int i, int j){
            return ranks[i] <= ranks[j];
        });

        Residue_ r = residue;
        for (int i = 0; i < n; i++) {
            r[i] = residue[indices[i]];
        }

        residue = std::move(r);
    }
}

}

