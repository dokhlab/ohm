#pragma once

#include "pdb_model.hpp"
#include "pdb_types.hpp"

namespace jnc {
namespace bio {

class Pdb : public V<PdbModel> {
public:
  std::string name;

  Pdb() {}

  Pdb(const std::string &filename);

  void read(const std::string &filename);

  void remove_hydrogens();

  void sort();
};

std::ostream &operator<<(std::ostream &output, const Pdb &pdb);

Pdb read_pdb(const std::string &filename);
Pdb read_pdb(std::istream &ifile, const std::string &filename="");

} // namespace bio
} // namespace jnc
