#pragma once

#include <string>
#include <array>
#include "../math/geom.hpp"

namespace jnc {

class DihAnal {
public:
    using Point = std::array<double, 3>;

    DihAnal(double = 1.0);
    ~DihAnal();

    template<typename Chain_>
    void read_mol(const Chain_ &chain) {
        int length = chain.size();
        initPoints(length);
        len = length;

        int n = 0;
        for (auto && res : chain) {
            std::string resName = res.name;
            int k = 0;
            for (auto && atom : res) {
                std::string name = atom.name;
                if (name == "P") {
                    p[n] = new Point{atom[0], atom[1], atom[2]};
                } else if (name == "O5*") {
                    o5_[n] = new Point{atom[0], atom[1], atom[2]};
                } else if (name == "C5*") {
                    c5_[n] = new Point{atom[0], atom[1], atom[2]};
                } else if (name == "C4*") {
                    c4_[n] = new Point{atom[0], atom[1], atom[2]};
                } else if (name == "C3*") {
                    c3_[n] = new Point{atom[0], atom[1], atom[2]};
                } else if (name == "O3*") {
                    o3_[n] = new Point{atom[0], atom[1], atom[2]};
                } else if (name == "C2*") {
                    c2_[n] = new Point{atom[0], atom[1], atom[2]};
                } else if (name == "C1*") {
                    c1_[n] = new Point{atom[0], atom[1], atom[2]};
                } else if ((resName == "A" || resName == "G") && name == "N9") {
                    b1[n] = new Point{atom[0], atom[1], atom[2]};
                } else if ((resName == "A" || resName == "G") && name == "C8") {
                    b2[n] = new Point{atom[0], atom[1], atom[2]};
                } else if ((resName == "U" || resName == "C") && name == "N1") {
                    b1[n] = new Point{atom[0], atom[1], atom[2]};
                } else if ((resName == "U" || resName == "C") && name == "C6") {
                    b2[n] = new Point{atom[0], atom[1], atom[2]};
                }
                k++;
            }
            n++;
        }
    }

    template<typename Chain_>
    double score(const Chain_ &chain) {
        read_mol(chain);

        double sc = 0;
        int n = 0;
        double temp, a, b;
        for (int i = 0; i < len; i++) {
            /* alpha */
            if (i >= 1 && p[i] != NULL && distance(*(o3_[i - 1]), *(o5_[i])) < 3) {
                temp = dihedral(*(o3_[i - 1]), *(p[i]), *(o5_[i]), *(c5_[i]));
                a = obsProb[int(temp / interval)];
                if (a == 0) {
                    sc -= 0;
                } else {
                    b = refProb[int(temp / interval)];
                    sc -= log(a / b);
                }
                n++;
            }
            /* beta */
            if (p[i] != NULL) {
                temp = dihedral(*(p[i]), *(o5_[i]), *(c5_[i]), *(c4_[i]));
                a = obsProb[bins + int(temp / interval)];
                if (a == 0) {
                    sc -= 0;
                } else {
                    b = refProb[int(temp / interval)];
                    sc -= log(a / b);
                }
                n++;
            }
            /* gamma */
            temp = dihedral(*(o5_[i]), *(c5_[i]), *(c4_[i]), *(c3_[i]));
            a = obsProb[bins * 2 + int(temp / interval)];
            if (a == 0) {
                sc -= 0;
            } else {
                b = refProb[int(temp / interval)];
                sc -= log(a / b);
            }
            n++;
            /* delta */
            temp = dihedral(*(c5_[i]), *(c4_[i]), *(c3_[i]), *(o3_[i]));
            a = obsProb[bins * 3 + int(temp / interval)];
            if (a == 0) {
                sc -= 0;
            } else {
                b = refProb[int(temp / interval)];
                sc -= log(a / b);
            }
            n++;
            /* epsilon */
            if (i + 1 < len && p[i + 1] != NULL && distance(*(o3_[i]), *(o5_[i + 1])) < 3) {
                temp = dihedral(*(c4_[i]), *(c3_[i]), *(o3_[i]), *(p[i + 1]));
                a = obsProb[bins * 4 + int(temp / interval)];
                if (a == 0) {
                    sc -= 0;
                } else {
                    b = refProb[int(temp / interval)];
                    sc -= log(a / b);
                }
                n++;
            }
            /* zeta */
            if (i + 1 < len && p[i + 1] != NULL && distance(*(o3_[i]), *(o5_[i + 1])) < 3) {
                temp = dihedral(*(c3_[i]), *(o3_[i]), *(p[i + 1]), *(o5_[i + 1]));
                a = obsProb[bins * 5 + int(temp / interval)];
                if (a == 0) {
                    sc -= 0;
                } else {
                    b = refProb[int(temp / interval)];
                    sc -= log(a / b);
                }
                n++;
            }
            /* chi */
            temp = dihedral(*(c2_[i]), *(c1_[i]), *(b1[i]), *(b2[i]));
            a = obsProb[bins * 6 + int(temp / interval)];
            if (a == 0) {
                sc -= 0;
            } else {
                b = refProb[int(temp / interval)];
                sc -= log(a / b);
            }
            n++;
        }

        return sc;
    }

    void train();
    void read_parm(std::string);
    void initProb();
    void printParm();
    void printProb();

    void delPoints();
    void initPoints(int);

    double interval;
    int bins;

    int *obsParm;
    double *obsProb;
    int *refParm;
    double *refProb;
    int len;
    Point **p, **o5_, **c5_, **c4_, **c3_, **o3_, **c2_, **c1_, **b1, **b2;
};

}
