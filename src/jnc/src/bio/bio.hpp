#pragma once

#include "./pdb.hpp"
#include "./mol2.hpp"
#include "./pdb_writer.hpp"
#include "./pdb_reader.hpp"
#include "./residue_sort.hpp"
#include "./rna_score.hpp"

namespace jnc {
namespace bio {

bool is_rna(const PdbChain &chain);

bool is_protein(const PdbChain &chain);

bool is_rna_residue(const std::string &res_name);

/**
 * Write a line in a PDB file.
 */
std::string pdb_line(const std::string &record_name, int atom_num, const std::string &atom_name,
                            const std::string &residue_name, const std::string &chain_name, int residue_num,
                            double x, double y, double z, double occupancy=1.0, double temperature_factor=0.0,
                            const std::string &element="", const std::string &charge="");

}
}

