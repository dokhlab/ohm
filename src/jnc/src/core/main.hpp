/***************************************************************************
 *
 * Authors: "Jian Wang (jianopt@gmail.com)"
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#include <iostream>
#include <map>
#include <functional>
#include <string>
#include "traits.hpp"
#include "string.hpp"
#include "opt.hpp"

#define JN_REG_COMPONENT(i, par)\
    int JN_PP_CAT(main_, i)(const ::jnc::Opt &);\
    ::jnc::MainComponent JN_PP_CAT(component_, i)(JN_PP_STR(i), JN_PP_CAT(main_, i));\
    int JN_PP_CAT(main_, i)(const ::jnc::Opt &par)

namespace jnc {

class Main {
public:
    using func_t = STD_ function<int(const Opt &)>;
    using func_map_t = M<Str, func_t>;

    func_map_t methods;

    func_t main_help = [](const Opt &par) -> int {
        STD_ cout
            << "Wrong command: "<< string_join(' ', par.g) << '\n'
            << "Possible commands:\n";
        for (auto && p : Main::instance().methods) {
            auto &&v = string_tokenize(p.first, "_");
            STD_ cout << "    " << string_join(' ', v) << '\n';
        }
        return 0;
    };

    static Main &instance() {
        static Main main;
        return main;
    }

    static void run(int argc, char **argv) {
        Opt par(argc, argv);
        instance().get_func(par.g)(par);
    }

    int get_score(Str s, Qs p) {
        auto &&v = string_tokenize(s, "_");
        if (v.size() != p.size()) return 0;
        auto it1 = v.begin();
        auto it2 = p.begin();
        int score = 0;
        for (; it1 != v.end() && it2 != p.end(); it1++, it2++) {
            if (*it1 == *it2) score++;
            else return 0;
        }
        return score;
    }

    func_t &get_func(Qs path) {
        using pair_t = STD_ pair<Str, int>;
        M<Str, int> scores;
        for (auto && p : methods) {
            scores[p.first] = get_score(p.first, path);
        }
        auto it = STD_ max_element(scores.begin(), scores.end(), [](const pair_t &p1, const pair_t &p2){
            return p1.second <= p2.second;
        });
        if (it->second == 0) return main_help;
        else {
            return methods[it->first];
        }
    }

};

class MainComponent : public Main {
public:
    template<typename F>
    MainComponent(const ::jnc::Str &name, F &&f) {
        Main::instance().methods[name] = f;
    }
};

} // namespace jn

