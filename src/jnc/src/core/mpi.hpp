/***************************************************************************
 *
 * Authors: "Jian Wang"
 * Email: jianopt@163.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#include "macros.hpp"

#ifdef USEMPI

#include "mkl.hpp"
#include <mpi.h>
#define JN_MPI_INITIALIZE(argc, argv) JN_MPI_Init(argc, argv)
#define JN_MPI_FINALIZE JN_MPI_Finalize()
#define JN_MPI_IS_ROOT (mpi_rank() == 0)
#define JN_MPI_LOG if (JN_MPI_IS_ROOT) ::std::cout

namespace jnc {

int mpi_rank();
int mpi_size();

} // namespace jnc

#else // USEMPI

#define JN_MPI_INITIALIZE(argc, argv)
#define JN_MPI_FINALIZE
#define JN_MPI_LOG ::std::cout

#endif // USEMPI


