/***************************************************************************
 *
 * Authors: "Jian Wang"
 * Email: jianopt@163.com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This complete copyright notice must be included in any revised version of the
 * source code. Additional authorship citations may be added, but existing
 * author citations must be preserved.
 ***************************************************************************/

#pragma once

#define JN_PP_CAT(a, b)  a##b
#define JN_PP_CAT2(a, b) JN_PP_CAT(a, b)
#define JN_PP_CAT3(a, b) JN_PP_CAT2(a, b)

#define JN_PP_STR(a)  #a
#define JN_PP_STR2(a) JN_PP_STR(a)
#define JN_PP_STR3(a) JN_PP_STR2(a)

#ifndef JN_PI
#  define JN_PI 3.14159265358979323846
#endif // PI

#ifdef JN_SHOW_INFO

#  define JN_INFO(x) MPI_LOG << #x << ": " << (x) << ::std::endl
#  define JN_INFOS(x) MPI_LOG << x << ::std::endl
#  define JN_INFOV(x) MPI_LOG << #x << ": "; for (auto && n : (x)) MPI_LOG << n << ' '; MPI_LOG << ::std::endl
#  define JN_INFOA(x) (x).identify(#x);

#  define JN_INFOV2(x, name) MPI_LOG << name << ": " << (x) << ::std::endl
#  define JN_INFOA2(x, name) (x).identify(name);

#else

#  define JN_INFO(x)
#  define JN_INFOS(x)
#  define JN_INFOV(x)
#  define JN_INFOA(x)

#  define JN_INFOV2(x, name)
#  define JN_INFOA2(x, name)

#endif // JN_SHOW_INFO

#  define STD_ ::std::
#  define JN_ ::jnc::

#define SWAP32(x) ( (((x)&0x000000FF)<<24) | (((x)&0x0000FF00)<<8) | (((x)&0x00FF0000)>>8) | (((x)&0xFF000000)>>24) )

/**
 * This macro is used to define all the default constructors and the default
 * assignment operators.
 */
#define JN_DEFAULTS(name) \
    name() = default; \
name(const name &) = default; \
name(name &&) = default; \
name &operator =(const name &) = default; \
name &operator =(name &&) = default


