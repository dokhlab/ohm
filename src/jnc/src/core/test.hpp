#pragma once

#include <iostream>
#include <map>
#include <functional>
#include <string>
#include <csetjmp>
#include <csignal>
#include "traits.hpp"
#include "string.hpp"
#include "opt.hpp"

#define JN_TEST_CASE(i)\
    int JN_PP_CAT(test_, i)();\
    ::jnc::TestCase JN_PP_CAT(component_, i)(JN_PP_STR(i), JN_PP_CAT(test_, i));\
    int JN_PP_CAT(test_, i)()

namespace jnc {

template <typename F, typename... Args, typename = decltype(::std::declval<F>()(::std::declval<Args>()...))>
void do_check_failed(F&& f, Args&&... args) {
    f(std::forward<Args>(args)...);
}

template <typename... Msgs>
void do_check_failed(Msgs&&... msgs) {
    (void)::std::initializer_list<int>{(::std::cout << ">>> " << msgs << ::std::endl, 0)...};
}

class Test {
public:
    using func_t = STD_ function<int()>;
    using func_map_t = M<Str, func_t>;

    func_map_t methods;
    Str last_checked_file_;
    size_t last_checked_line_;
    size_t failure_num_;

    func_t test_help = []() -> int {
        STD_ cout
            << "Possible commands:\n";
        for (auto && p : Test::instance().methods) {
            auto &&v = string_tokenize(p.first, "_");
            STD_ cout << "    " << string_join(' ', v) << '\n';
        }
    };

    static jmp_buf& abort_case()
    {
        static jmp_buf buf;
        return buf;
    }

    static Test &instance() {
        static Test test;
        return test;
    }

    static void run(int argc, char **argv) {
        Opt opt(argc, argv);
        if (opt.g.empty()) {
            for (auto && p : Test::instance().methods) {
                p.second();
            }
        }
        else {
            instance().get_func(opt.g)();
        }
    }

    int get_score(Str s, Qs p) {
        auto &&v = string_tokenize(s, "_");
        if (v.size() != p.size()) return 0;
        auto it1 = v.begin();
        auto it2 = p.begin();
        int score = 0;
        for (; it1 != v.end() && it2 != p.end(); it1++, it2++) {
            if (*it1 == "*") continue;
            else if (*it1 == *it2) score++;
            else return 0;
        }
        return score;
    }

    func_t &get_func(Qs path) {
        using pair_t = STD_ pair<Str, int>;
        M<Str, int> scores;
        for (auto && p : methods) {
            scores[p.first] = get_score(p.first, path);
        }
        auto it = STD_ max_element(scores.begin(), scores.end(), [](const pair_t &p1, const pair_t &p2){
            return p1.second <= p2.second;
        });
        if (it->second == 0) return test_help;
        else {
            return methods[it->first];
        }
    }

    void inc_failure() {
        ++failure_num_;
    }

    void check_file(const Str& file) {
        last_checked_file_ = file;
    }

    void check_line(size_t line) {
        last_checked_line_ = line;
    }

};

class TestCase : public Test {
public:
    template<typename F>
    TestCase(const ::jnc::Str &name, F &&f) {
        Test::instance().methods[name] = f;
    }
};

#define JN_TEST_CHECK_G(cond, strict, ...)                                                                  \
    do {                                                                                            \
        Test::instance().check_file(__FILE__);                                                      \
        Test::instance().check_line(__LINE__);                                                      \
        if(!(cond)) {                                                                               \
            Test::instance().inc_failure();                                                         \
            if(strict) {                                                                            \
                std::cout << ">>> check \"" << #cond << "\" failed." << std::endl;                  \
                std::cout << ">>> critical error at " __FILE__ "(" << __LINE__ << ")." << std::endl;\
                do_check_failed(__VA_ARGS__);                                                       \
                longjmp(Test::abort_case(), true);                                                  \
            } else {                                                                                \
                std::cout << ">>> check \"" << #cond << "\" failed." << "at "                       \
                << __FILE__ << "(" << __LINE__ << ")" << std::endl;                                 \
                do_check_failed(__VA_ARGS__);                                                       \
            }                                                                                       \
        }                                                                                           \
    } while(0)

#define JN_TEST_CHECK(cond, ...)                                                                       \
    JN_TEST_CHECK_G(cond, false, __VA_ARGS__)

#define JN_TEST_REQUIRE(cond, ...)                                                                     \
    JN_TEST_CHECK_G(cond, true, __VA_ARGS__)

} // namespace jnc

