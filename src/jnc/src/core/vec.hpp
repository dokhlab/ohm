#pragma once

#include "coro.hpp"
#include <array>
#include <concepts>
#include <iostream>
#include <list>
#include <vector>

namespace jnc {

template <typename T>
concept ArrayLike = requires(T a) { *std::begin(a); };

template <typename T>
concept MatrixLike = requires(T a) { *std::begin(*(std::begin(a))); };

template <typename T>
concept ItemLike = !
ArrayLike<T>;

#define DEF_OPERATOR(name, operator)                                                                                   \
  template <typename A, typename B> A &name(A &a, const B &b) {                                                        \
    auto it1 = std::begin(a);                                                                                          \
    auto it2 = std::begin(b);                                                                                          \
    for (; it1 != std::end(a) && it2 != std::end(b); it1++) {                                                          \
      *it1 operator* it2;                                                                                              \
      it2++;                                                                                                           \
    }                                                                                                                  \
    return a;                                                                                                          \
  }                                                                                                                    \
                                                                                                                       \
  template <typename A, typename B>                                                                                    \
    requires ItemLike<B>                                                                                               \
  A &name(A &a, const B &b) {                                                                                          \
    auto it1 = std::begin(a);                                                                                          \
    for (; it1 != std::end(a); it1++) {                                                                                \
      *it1 operator b;                                                                                                 \
    }                                                                                                                  \
    return a;                                                                                                          \
  }

DEF_OPERATOR(vec_assign, =);
DEF_OPERATOR(vec_plus_by, +=);
DEF_OPERATOR(vec_minus_by, -=);
DEF_OPERATOR(vec_times_by, *=);
DEF_OPERATOR(vec_divide_by, /=);

template <typename Rt> struct vec_center_impl : public VIEW_CLOSURE {
  template <typename Vec> Rt operator()(Vec &&vec) const {
    int n = 0;
    Rt center;
    for (auto &&val : vec) {
      if (n == 0) {
        vec_assign(center, val);
      } else {
        vec_plus_by(center, val);
      }
      n++;
    }
    vec_divide_by(center, n);
    return center;
  }
};
template <typename Rt> constexpr auto vec_center = vec_center_impl<Rt>{};

struct vec_print_impl : public VIEW_CLOSURE {
  template <typename Vec> void operator()(Vec &&vec) const { std::cout << vec << ' '; }

  template <typename Vec>
    requires ArrayLike<Vec>
  void operator()(Vec &&vec) const {
    for (auto &&val : vec) {
      (*this)(val);
    }
    std::cout << std::endl;
  }
};
constexpr auto vec_print = vec_print_impl{};

template <typename Vec> using item_type = typename std::decay<decltype(*std::begin(std::declval<Vec>()))>::type;

template <typename Mat, typename Item = item_type<item_type<Mat>>> generator<Item> mat_flatten(Mat &&mat) {
  for (auto &&item : mat) {
    for (auto &&i : item) {
      co_yield i;
    }
  }
}

} // namespace jnc
