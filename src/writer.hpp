#pragma once

#include "jnc/bio"

namespace rnalig {

/**
 * Write the ligand.
 */
template <typename Ligand_> void write_ligand(std::ostream &ofile, const Ligand_ &ligand, int atom_index = 0) {
  int residue_index = 0;
  std::string residue_name = "X";
  for (auto &&atom : ligand) {
    ofile << jnc::bio::pdb_line("ATOM", atom_index + 1, atom.name, residue_name, "B", residue_index + 1, atom[0],
                                atom[1], atom[2])
          << std::endl;
    atom_index++;
  }
  for (auto &&atom : ligand) {
    for (auto &&h : atom.hydrogens) {
      ofile << jnc::bio::pdb_line("ATOM", atom_index + 1, h.name, residue_name, "B", residue_index + 1, h[0], h[1],
                                  h[2])
            << std::endl;
      atom_index++;
    }
  }
  ofile << "TER" << std::endl;
}

/**
 * Write the receptor.
 */
template <typename Receptor_> void write_receptor(std::ostream &ofile, const Receptor_ &receptor, int atom_index = 0) {
  int residue_index = 0;
  for (auto &&residue : receptor) {
    for (auto &&atom : residue) {
      ofile << jnc::bio::pdb_line("ATOM", atom_index + 1, atom.name, residue.name, "A", residue_index + 1, atom[0],
                                  atom[1], atom[2])
            << std::endl;
      atom_index++;
    }
    for (auto &&atom : residue) {
      for (auto &&hydrogen : atom.hydrogens) {
        ofile << jnc::bio::pdb_line("ATOM", atom_index + 1, hydrogen.name, residue.name, "A", residue_index + 1,
                                    hydrogen[0], hydrogen[1], hydrogen[2])
              << std::endl;
        atom_index++;
      }
    }
    residue_index++;
  }
  ofile << "TER" << std::endl;
}

class TrajectoryWriter {
public:
  std::string filename;
  std::string receptor_chain_name = "A";
  std::string ligand_chain_name = "B";
  int model_index = 0;
  int atom_index = 0;

  TrajectoryWriter() {}

  /**
   * Set the trajectory file name when the object is created.
   */
  TrajectoryWriter(const std::string &filename) { set_file(filename); }

  /**
   * Write the "END" label into the trajectory file when the object is
   * destroyed.
   */
  ~TrajectoryWriter() { write_end(); }

  /**
   * Write the "END" label into the trajectory file.
   */
  void write_end() {
    if (!filename.empty() && model_index != 0) {
      std::ofstream ofile(filename, std::ios::app);
      ofile << "END" << std::endl;
      ofile.close();
    }
  }

  /**
   * Set the trajectory file name.
   */
  void set_file(const std::string &filename) {
    write_end();

    this->filename = filename;
    atom_index = 0;
    model_index = 0;
  }

  /**
   * Write the complex to the trajectory.
   */
  template <typename Receptor_, typename Ligand_>
  void write(const Receptor_ &receptor, const Ligand_ &ligand, double energy) {
    if (filename.empty())
      return;

    // Clear the file.
    if (model_index == 0) {
      std::ofstream ofile(filename);
      ofile.close();
    }

    std::ofstream ofile(filename, std::ios::app);

    ofile << "MODEL " << model_index + 1 << std::endl;
    ofile << "REMARK Energy: " << energy << std::endl;
    write_receptor(ofile, receptor, atom_index);
    write_ligand(ofile, ligand, atom_index);
    ofile << "ENDMDL" << std::endl;

    ofile.close();

    model_index++;
    atom_index = 0;
  }
};

} // namespace rnalig
