#pragma once

#include "jnc/math"

class Grid {
public:
    using Range = std::array<std::array<int, 2>, 3>;
    using Coord = std::array<double, 3>;

    double *data = NULL;
    double *bfactor_data = NULL;
    double vdw_radius = 1.4;
    double shell = 0.5;
    int size;
    std::array<int, 3> shape;
    std::array<int, 3> coeff;
    std::array<double, 3> interval;
    Range range;
    double interior_score = -1000;
    double shell_score = 1;

    Grid() {}

    template<typename Chain_>
    void init(const Chain_ &chain) {
        // Set interval
//        interval = {0.25, 0.25, 0.25};

        // Set range
        for (int i = 0; i < 3; i++) {
            range[i][0] = 9999;
            range[i][1] = -9999;
        }
        for (auto && residue : chain) {
            for (auto && atom : residue) {
                if (range[0][0] > atom[0]) range[0][0] = atom[0];
                if (range[0][1] < atom[0]) range[0][1] = atom[0];
                if (range[1][0] > atom[1]) range[1][0] = atom[1];
                if (range[1][1] < atom[1]) range[1][1] = atom[1];
                if (range[2][0] > atom[2]) range[2][0] = atom[2];
                if (range[2][1] < atom[2]) range[2][1] = atom[2];
            }
        }
        for (int i = 0; i < 3; i++) {
            range[i][0] -= 30;
            range[i][1] += 30;
        }

        // Set shape
        for (int i = 0; i < 3; i++) {
            shape[i] = int(std::ceil((range[i][1] - range[i][0]) / interval[i]));
        }

        size = shape[0] * shape[1] * shape[2];

        coeff = {shape[1] * shape[2], shape[2], 1};
       
        data = new double[size];
        for (int i = 0; i < size; i++) data[i] = 0;

        bfactor_data = new double[size];
        for (int i = 0; i < size; i++) bfactor_data[i] = 0;

        for (auto && residue : chain) {
            add(residue);
        }
    }

    template<typename Residue_>
    void remove(const Residue_ &residue) {
        add(residue, -1);
    }

    template<typename Residue_>
    void add(const Residue_ &residue, double c = 1.0) {
        for (auto && atom : residue) {
            auto && r = get_range(atom, shell + vdw_radius);
            for (int i = r[0][0]; i <= r[0][1]; i++) {
                for (int j = r[1][0]; j <= r[1][1]; j++) {
                    for (int k = r[2][0]; k <= r[2][1]; k++) {
                        auto &&coord = get_coord(i, j, k);
                        double d = jnc::distance(coord, atom);
                        double &b = bfactor(i, j, k);
                        if (d < vdw_radius) {
                            at(i, j, k) += interior_score * c;
                            if (b < atom.bfactor) {
                                b = atom.bfactor;
                            }
                        }
                        else if (d < shell + vdw_radius) {
                            if (at(i, j, k) >= 0) {
                                at(i, j, k) += shell_score * c;
                                if (b < atom.bfactor) {
                                    b = atom.bfactor;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    int index(int i, int j, int z) {
        return i * coeff[0] + j * coeff[1] + z * coeff[2];
    }

    double &at(int i, int j, int z) {
        int n = i * coeff[0] + j * coeff[1] + z * coeff[2];
        if (n >= size) {
            std::cout << jnc::string_format("Coord(%d %d %d) out of box(%d %d %d).", i, j, z, shape[0], shape[1], shape[2]) << std::endl;
            throw "error";
        }
        else {
            return data[n];
        }
    }

    double &bfactor(int i, int j, int z) {
        int n = i * coeff[0] + j * coeff[1] + z * coeff[2];
        if (n >= size) {
            std::cout << jnc::string_format("Coord(%d %d %d) out of box(%d %d %d).", i, j, z, shape[0], shape[1], shape[2]) << std::endl;
            throw "error";
        }
        else {
            return bfactor_data[n];
        }
    }

    template<typename Position_>
    Range get_range(const Position_ &p, double d) {
        Range r;
        r[0][0] = int((p[0] - range[0][0] - d) / interval[0]);
        r[0][1] = int((p[0] - range[0][0] + d) / interval[0]);
        r[1][0] = int((p[1] - range[1][0] - d) / interval[1]);
        r[1][1] = int((p[1] - range[1][0] + d) / interval[1]);
        r[2][0] = int((p[2] - range[2][0] - d) / interval[2]);
        r[2][1] = int((p[2] - range[2][0] + d) / interval[2]);
        return std::move(r);
    }

    Coord get_coord(int i, int j, int k) {
        return Coord {range[0][0] + i * interval[0], range[1][0] + j * interval[1], range[2][0] + k * interval[2]};
    }

    ~Grid() {
        if (data != NULL) delete [] data;
        if (bfactor_data != NULL) delete [] bfactor_data;
    }

};

