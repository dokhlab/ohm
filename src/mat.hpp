#pragma once

#include <iostream>
#include <fstream>
#include <utility>
#include <array>
#include <string>

template<typename T>
class Mat {
public:
    using data_type = T;

    data_type *data;
    std::array<int, 2> shape;

    Mat() {
        data = NULL;
        shape[0] = 0;
        shape[1] = 0;
    }

    Mat(int a, int b) {
        shape[0] = a;
        shape[1] = b;
        data = new data_type[a * b];
    }

    Mat(int a, int b, const data_type &v) {
        shape[0] = a;
        shape[1] = b;
        data = new data_type[a * b];
        for (int i = 0; i < a * b; i++) {
            data[i] = v;
        }
    }

    Mat(const Mat<data_type> &mat) {
        shape[0] = mat.shape[0];
        shape[1] = mat.shape[1];
        int size = shape[0] * shape[1];
        data = new data_type[size];
        for (int i = 0; i < size; i++) {
            data[i] = mat.data[i];
        }
    }

    Mat(Mat<data_type> &&mat) {
        data = mat.data;
        mat.data = NULL;

        swap(shape, mat.shape);
    }

    Mat &operator =(const Mat<data_type> &mat) {
        if (data != NULL) {
            delete [] data;
            data = NULL;
        }

        shape[0] = mat.shape[0];
        shape[1] = mat.shape[1];
        int size = shape[0] * shape[1];
        data = new data_type[size];
        for (int i = 0; i < size; i++) {
            data[i] = mat.data[i];
        }
    }

    Mat &operator =(Mat<data_type> &&mat) {
        data = mat.data;
        mat.data = NULL;

        swap(shape, mat.shape);

        return *this;
    }

    ~Mat() {
        if (data != NULL) {
            delete [] data;
            data = NULL;
        }
    }

    static Mat<data_type> read(const std::string &filename) {
        std::ifstream ifile(filename.c_str());
        int a, b;
        ifile >> a >> b;
        Mat<data_type> mat(a, b);
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                double d;
                ifile >> d;
                mat(i, j) = d;
            }
        }
        ifile.close();

        return std::move(mat);
    }

    int rows() const {
        return shape[0];
    }

    int cols() const {
        return shape[1];
    }

    data_type &operator ()(int i, int j) {
        return data[i * shape[1] + j];
    }

    const data_type &operator ()(int i, int j) const {
        return data[i * shape[1] + j];
    }

    data_type average() const {
        data_type sum = 0;
        int n = shape[0] * shape[1];
        int m = 0;
        for (int i = 0; i < n; i++) {
            if (data[i] != 0) {
                sum += data[i];
                m++;
            }
        }
        return data_type(sum / double(m));
    }

    data_type max() const {
        auto max = data[0];
        for (int i = 1; i < shape[0] * shape[1]; i++) {
            if (max < data[i]) {
                max = data[i];
            }
        }
        return max;
    }

    data_type min() const {
        auto min = data[0];
        for (int i = 1; i < shape[0] * shape[1]; i++) {
            if (min > data[i]) {
                min = data[i];
            }
        }
        return min;
    }
};

using Matd = Mat<double>;
using Matb = Mat<bool>;


