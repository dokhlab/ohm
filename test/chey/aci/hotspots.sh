module load ohm
module load anaconda

source ./config.sh

exclude ${name}-bfactor1.pdb "$active+$allosteric" ${name}-bfactor1-main.pdb

include ${name}-bfactor1.pdb "$active" ${name}-bfactor1-active.pdb

include ${name}-bfactor1.pdb "$allosteric" ${name}-bfactor1-allosteric.pdb

hotspots -i ${name}-bfactor1-main.pdb -o ${name}-hotspots.pdb -int 1.5 -shell 1 -r 3 -t 0.05 -c 0.7 -all >hotspots.txt

bash gen-hotspots-pse.sh ${name}-bfactor1-main.pdb ${name}-bfactor1-active.pdb ${name}-bfactor1-allosteric.pdb ${name}-hotspots.pdb

