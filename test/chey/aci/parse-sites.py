#! /bin/env python

import sys
import re

sites_str = sys.argv[1]
ind_file = sys.argv[2]
sites_file = sys.argv[3]

inds = []
for s in re.split('\+', sites_str):
    v = re.split('-', s)
    if len(v) == 1:
        inds.append(int(v[0]))
    elif len(v) == 2:
        for i in range(int(v[0]), int(v[1])+1):
            inds.append(i)

residues = []
for line in open(ind_file):
  l = re.split('\s+', line.strip())
  if len(l) == 3:
    ind = int(l[0])
    chain = l[1]
    residue = l[2]

    w = re.split('/', chain)
    chain_name = w[1]

    w = re.split('/', residue)
    res_num = int(w[0])

    s = '{0}/{1}'.format(chain_name, res_num)

    if ind in inds:
      residues.append(s)

f = open(sites_file, 'w+')
f.write(','.join([res for res in residues]))
f.close()
