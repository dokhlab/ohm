module load ohm
module load anaconda

source ./config.sh

exclude ${name}-bfactor1.pdb "${active}+${allosteric}" ${name}-bfactor1-main.pdb
include ${name}-bfactor1.pdb "${active}" ${name}-bfactor1-active.pdb
include ${name}-bfactor1.pdb "${allosteric}" ${name}-bfactor1-allosteric.pdb
pocket -i ${name}-bfactor1-main.pdb -o ${name}-pockets.pdb -sc $volume_ratio -int $cell_size -delta $volume_radius -n $pockets -r $vdw -s $shell >pockets.txt
bash gen-pocket-pse.sh ${name}-bfactor1-main.pdb ${name}-bfactor1-active.pdb ${name}-bfactor1-allosteric.pdb ${name}-pockets.pdb

