#! /bin/env python

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib import colors
import sys

from colour import Color

n_colors = 50
color_gradients = list(Color("blue").range_to(Color("red"), n_colors+1))
newCmp = colors.ListedColormap([i.rgb for i in color_gradients], name='blue-red')

data_file = sys.argv[1]
label_file = sys.argv[2]
out_file = sys.argv[3]

f = open(label_file)
labels = [line.strip() for line in f.readlines()]
f.close()

data = np.genfromtxt(data_file)

n = len(data)

plot = plt.imshow(data, cmap='Purples', origin='lower')

locs, labels_ = plt.xticks()

locs = [int(loc) for loc in locs]
labels = [labels[loc] if (loc < len(labels) and loc >= 0) else '' for loc in locs]

plt.xticks(locs, labels)
plt.yticks(locs, labels)
plt.xlim(-1, n)
plt.ylim(-1, n)

plt.colorbar(plot)
#plt.show()
plt.savefig(out_file)
