#! /bin/env python

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib import colors
import sys

from colour import Color

n_colors = 50
color_gradients = list(Color("blue").range_to(Color("red"), n_colors+1))
newCmp = colors.ListedColormap([i.rgb for i in color_gradients], name='blue-red')

print(len(color_gradients))

data_file = sys.argv[1]
out_file = sys.argv[2]

data = np.genfromtxt(data_file)
n = len(data)

y = data
m = float(max(y))
colors = [color_gradients[int(i * n_colors)].rgb for i in y]

#colors = cm.hsv(y / float(max(y)))
plot = plt.scatter(y, y, c = y, cmap=newCmp)
plt.clf()
plt.colorbar(plot)
plt.xlabel('Sequence')
plt.ylabel('ACI')

plt.bar(range(1, n+1), data, color=colors)
#plt.show()
plt.savefig(out_file)
