module load anaconda

ipdb=$1
opdb=$2

tmp=$RANDOM
cat <<! >${tmp}.pym
cmd.load("$ipdb")
cmd.remove("(solvent and (all))")
cmd.h_add("all")
cmd.sort("all extend 1")
cmd.save("$opdb")
cmd.quit()
!
pymol -c ${tmp}.pym
rm -f ${tmp}.pym
