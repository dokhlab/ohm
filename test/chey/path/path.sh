module load anaconda
module load ohm

source ./config.sh

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c $distance_cutoff -a $alpha
python ind2label.py ${name}.ind ${name}.label
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

# Set active site
python format-sites.py ${active} ${name}.ind ${name}.active
active_name=${active}
active=$(cat ${name}.active)

# Set allosteric site
python format-sites.py ${allosteric} ${name}.ind ${name}.allosteric
allos_name=${allosteric}
allosteric=$(cat ${name}.allosteric)

diffuse ${name}.mat $active ${name}.nodes ${name}.bonds -n $rounds -c $probability_cutoff

bfactor ${name}.pdb ${name}.nodes ${name}-bfactor.pdb

python plot-nodes.py ${name}.nodes ${name}-nodes.png

diffuse path ${name}.mat $allosteric $active ${name}.paths -n $pathways -c 0.05
python get-network.py ${name}.paths $pathways ${name}.net

plot-paths -i ${name}-bfactor.pdb -p ${name}.paths -o ${name}-path.pym -active ${active_name} -allos ${allos_name} -ind ${name}.ind

pymol -c ${name}-path.pym

