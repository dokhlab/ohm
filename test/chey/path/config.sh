name="chey"
active="A/57"
allosteric="D/1-16"
alpha="4.5"
distance_cutoff="3.4"
probability_cutoff="0.05"
rounds="10000"
pathways="100"
cell_size="1"
volume_radius="5"
volume_ratio="0.3"
pockets=50
vdw="3.3"
shell="0.1"
