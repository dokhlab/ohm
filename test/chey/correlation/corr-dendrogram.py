import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage
import sys

def getClusterIds(i, links):
    link = links[i]
    a = int(link[0])
    b = int(link[1])
    ids = []
    if a == b:
        ids.append(a)
    else:
        ids.extend(getClusterIds(a, links))
        ids.extend(getClusterIds(b, links))
    return ids

def getClusterLinks(i, links):
    link = links[i]
    a = int(link[0])
    b = int(link[1])
    ls = []
    if a == b:
        pass
    else:
        ls.append(i)
        ls.extend(getClusterLinks(a, links))
        ls.extend(getClusterLinks(b, links))
    return ls

def smoothsegment(seg, Nsmooth=100):
    return np.concatenate([[seg[0]], np.linspace(seg[1], seg[2], Nsmooth), [seg[3]]])

def plotDendrogram(icoord, dcoord, colors, ivl, figsize, polar=False):
    if polar:
        dcoord = -np.log(dcoord+1)
        # avoid a wedge over the radial labels
        gap = 0.01
        imax = icoord.max()
        imin = icoord.min()
        icoord = ((icoord - imin) / (imax - imin) * (1-gap) + gap*0.75) * 2 * np.pi
#    with plt.style.context("seaborn-white"):
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, polar=polar)
        for xs, ys, color in zip(icoord, dcoord, colors):
            if polar:
                xs = smoothsegment(xs)
                ys = smoothsegment(ys)
            ax.plot(xs,ys, color=color)
#        ax.set_title(f"Polar= {polar}", fontsize=15)
        if polar:
            n = len(ivl)
            xticks = [(i * (n+1) / float(n*n) * (1 - gap) + gap * 0.75) * 2 * np.pi for i in range(n)]
            ax.spines['polar'].set_visible(False)
            ax.set_rlabel_position(0)

#            Nxticks = 10
#            xticks = np.linspace(gap/2, 1-gap/2, Nxticks)
#            ax.set_xticks(xticks*np.pi*2)
            ax.set_xticks(xticks)
            ax.set_xticklabels(ivl, fontsize=30)
            ax.set_yticklabels([])

            plt.gcf().canvas.draw()
            angles = np.linspace(0,2*np.pi,len(ax.get_xticklabels())+1)
            angles[np.cos(angles) < 0] = angles[np.cos(angles) < 0] + np.pi
            angles = np.rad2deg(angles)
            labels = []
            for label, angle in zip(ax.get_xticklabels(), angles):
                x,y = label.get_position()
                lab = ax.text(x,y, label.get_text(), transform=label.get_transform(), ha=label.get_ha(), va=label.get_va())
                lab.set_rotation(angle)
                labels.append(lab)
            ax.set_xticklabels([])
            ax.grid(False)

matFile = sys.argv[1]
labelFile = sys.argv[2]
cutoff = float(sys.argv[3])
clusterFile = sys.argv[4]
normalDendrogramFigFile = sys.argv[5]
circleDendrogramFigFile = sys.argv[6]

mat = np.genfromtxt(matFile)

f = open(labelFile)
labels = [l.strip() for l in f.readlines()]
f.close()
#mat = [[1, 2, 4], [2, 3, 5], [5, 7, 11], [6, 10, 20], [9, 19, 30]]

n = len(mat)
Z = linkage(mat, method='complete')
#print(Z)

links = []
for i in range(n):
    links.append([i, i, 0, 1])
for link in Z:
    links.append(link)

cutoffDistance = cutoff * links[-1][2]
clusters = []
#    for cluster in links:
#        print(' '.join(str(i) for i in cluster))
for i in range(len(links)):
    link = links[i]
    a = int(link[0])
    b = int(link[1])
    la = links[a]
    lb = links[b]
    if link[2] >= cutoffDistance:
        if la[2] < cutoffDistance:
            clusters.append(a)
        if lb[2] < cutoffDistance:
            clusters.append(b)
clusterIds = [getClusterIds(i, links) for i in clusters]
clusterLinks = [getClusterLinks(i, links) for i in clusters]

#cmap = mpl.cm.get_cmap('twilight_shifted')
#cmap = mpl.cm.get_cmap('rainbow')
#cmap = mpl.cm.get_cmap('gist_ncar')
cmap = mpl.cm.get_cmap('nipy_spectral')
linkColors = {}
for i in range(len(links)):
    link = links[i]
    if link[2] >= cutoffDistance:
        linkColors[i] = '#474747'

clusterColors = []
for i in range(len(clusterLinks)):
    links_ = clusterLinks[i]
    color = cmap(float(i) / len(clusterLinks))
    color = mpl.colors.rgb2hex(color[:3])
    clusterColors.append(color)
#    print('Color')
#    print(color)
    for link in links_:
        linkColors[link] = color

def foo(link):
#    print('Link:')
#    print(link)
#    print(linkColors[link])
    return linkColors[link]

# Write to cluster file
nResult = 0
f = open(clusterFile, 'w+')
for ids in clusterIds:
    f.write('Cluster {0} {1}: {2}\n'.format(nResult+1, clusterColors[nResult], ' '.join([labels[i] for i in ids])))
    nResult += 1
f.close()

#dn = dendrogram(Z, labels=labels)
plt.figure(figsize=(7,n/6))
dn = dendrogram(Z, orientation='right', labels=labels, link_color_func=foo)
plt.tight_layout()
plt.savefig(normalDendrogramFigFile)

icoord = np.asarray(dn["icoord"], dtype=float)
dcoord = np.asarray(dn["dcoord"], dtype=float)
colors = dn['color_list']
ivl = dn['ivl']
#print(len(colors))
#print(colors)

plotDendrogram(icoord, dcoord, colors, ivl, figsize=(5,5), polar=True)

#plt.show()
plt.savefig(circleDendrogramFigFile)


