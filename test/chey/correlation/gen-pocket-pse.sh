module load anaconda

source config.sh

pdb=$1
active=$2
allosteric=$3
pocket=$4

fullname=${pocket%.*}

cat <<! >${fullname}.pym
cmd.bg_color('white')
cmd.load("$pdb", 'main')
cmd.load("$active", 'active')
cmd.load("$allosteric", 'allosteric')
cmd.load("$pocket", 'pockets')
cmd.zoom("all")

cmd.show_as('surface', 'main')
cmd.show_as('sticks', 'active')
cmd.show_as('sticks', 'allosteric')
cmd.show_as('spheres', 'pockets')

cmd.spectrum("b",selection="main or active or allosteric")
util.color_chains('pockets')

cmd.save("${fullname}.pse")
cmd.quit()
!

pymol -c ${fullname}.pym
