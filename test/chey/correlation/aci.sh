module load anaconda
module load ohm

source ./config.sh

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c $distance_cutoff -a $alpha
python ind2label.py ${name}.ind ${name}.label
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

# Get sites
python format-sites.py "${active}" ${name}.ind ${name}.active
active=$(cat ${name}.active)

diffuse ${name}.mat $active ${name}.nodes ${name}.bonds -n $rounds -c $probability_cutoff

bfactor ${name}.pdb ${name}.nodes ${name}-bfactor.pdb

python plot-nodes.py ${name}.nodes ${name}-nodes.png

