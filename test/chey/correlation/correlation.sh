module load anaconda
module load ohm

source ./config.sh

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c $distance_cutoff -a $alpha
python ind2label.py ${name}.ind ${name}.label
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

diffuse all ${name}.mat ${name}.corr
python plot-mat.py ${name}.corr ${name}.label ${name}-corr.png

python corr-dendrogram.py ${name}.m ${name}.label 0.7 ${name}.clusters ${name}-dendrogram-normal ${name}-dendrogram-circle.png
plot-clusters ${name}.pdb ${name}.clusters ${name}-cluster.pym

pymol -c ${name}-cluster.pym

