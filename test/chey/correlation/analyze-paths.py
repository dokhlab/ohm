#! /bin/env python

import re
import sys

path_file = sys.argv[1]

nodes = {}

nodes[0] = {'num': 0, 'sons': set(), 'weight': 0.0}
nodes[-1] = {'num': -1, 'sons': set(), 'weight': 0.0}

for line in open(path_file):
    l = re.split('\s+', line.strip())
    if len(l) > 0:
        weight = float(l[0])
        path = [int(i) for i in re.split('-', l[1])]
        n = len(path)
        ls = [nodes[0]]
        for i in range(1, n-1):
            num = path[i]

            if num not in nodes:
                nodes[num] = {'num': num, 'sons': set(), 'weight': 0.0}

            ls.append(nodes[num])
        ls.append(nodes[-1])

        for node in ls:
            node['weight'] += weight

        for i in range(0, n-1):
            ls[i]['sons'].add(ls[i+1]['num'])

nodes = nodes.values()
nodes.sort(key=lambda node: -node['weight'])

for node in nodes:
    print 'Index:{0} Weight:{1} Sons:{2}'.format(node['num'], node['weight'], ','.join([str(n) for n in node['sons']]))
