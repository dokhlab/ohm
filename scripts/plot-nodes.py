#! /bin/env python

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib import colors
import sys
import re
import cmocean

from colour import Color

#n_colors = 50
#color_gradients = list(Color("blue").range_to(Color("red"), n_colors+1))
#newCmp = colors.ListedColormap([i.rgb for i in color_gradients], name='blue-red')

newCmp = cmocean.cm.thermal

def get_color(x):
#    return color_gradients[int(x * n_colors)].rgb
    return newCmp(x)

def get_site(site_str):
    inds = []
    for s in re.split('\+', site_str):
        v = re.split('-', s)
        if len(v) == 1:
            inds.append(int(v[0])-1)
        elif len(v) == 2:
            for i in range(int(v[0])-1, int(v[1])):
                inds.append(i)
    return inds

#print(len(color_gradients))

data_file = sys.argv[1]
out_file = sys.argv[2]
site_str1 = sys.argv[3]
site_str2 = ''
if len(sys.argv) > 4:
    site_str2 = sys.argv[4]

site1 = np.array(get_site(site_str1))
if site_str2:
    site2 = np.array(get_site(site_str2))

data = np.genfromtxt(data_file)
n = len(data)

y = data
y = (y - np.min(y)) / (np.max(y) - np.min(y))
m = float(max(y))
#colors = [color_gradients[int(i * n_colors)].rgb for i in y]
colors = [get_color(i) for i in y]

#colors = cm.hsv(y / float(max(y)))
plot = plt.scatter(y, y, c = y, cmap=newCmp)
plt.clf()
plt.colorbar(plot)
plt.xlabel('Sequence')
plt.ylabel('ACI')

plt.bar(range(1, n+1), y, color=colors)
plt.plot(site1+1, [1 for i in site1], 'k.')
if site_str2:
#    print('site 2')
    plt.plot(site2+1, [1 for i in site2], 'o', mec='red', mfc='none')
#plt.show()
plt.savefig(out_file, dpi=300)
