#! /bin/env python

import re
import sys

filename = sys.argv[1]
nline = 0
for line in open(filename):
    l = [float(i) for i in re.split('\s+', line.strip())]
    n = len(l)
    if n > 0:
        if nline == 0:
            s = [0 for i in l]
        for i in range(n):
            s[i] += l[i]
        nline += 1

for i in s:
    print(i)
