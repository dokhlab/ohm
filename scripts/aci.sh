module load anaconda
module load ohm

source ./config.sh

echo 'Calculate contacts'
contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c $distance_cutoff

echo 'Set label'
python ind2label.py ${name}.ind ${name}.label

echo 'Plot contact matrix'
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

echo 'Get sites'
python format-sites.py "${active}" ${name}.ind ${name}.active
active=$(cat ${name}.active)
if [ ! -z ${allosteric} ]; then
    python format-sites.py "${allosteric}" ${name}.ind ${name}.allosteric
    allosteric=$(cat ${name}.allosteric)
fi

echo 'Diffuse'
diffuse aci ${name}.mat $active ${name}.nodes ${name}.bonds -n $rounds -c $probability_cutoff -norm -a $alpha -mdist ${name}.mdist

echo 'Write ACI as bfactor to PDB file'
bfactor ${name}.pdb ${name}.nodes ${name}-bfactor.pdb

echo 'Plot ACI'
python plot-nodes.py ${name}.nodes ${name}-nodes.png ${active} ${allosteric}

echo 'Find hotspots'
peaks ${name}.pdb ${name}.nodes ${name}-peaks.txt
python name-peaks.py ${name}.ind ${name}-peaks.txt ${name}-named-peaks.txt

