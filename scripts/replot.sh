module load anaconda
module load ohm

source ./config.sh

python ind2label.py ${name}.ind ${name}.label
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

bfactor ${name}.pdb ${name}.nodes1 ${name}-bfactor1.pdb
bfactor ${name}.pdb ${name}.nodes2 ${name}-bfactor2.pdb

python plot-nodes.py ${name}.nodes1 ${name}-nodes1.png
python plot-nodes.py ${name}.nodes2 ${name}-nodes2.png

python get-network.py ${name}.paths $pathways ${name}.net

plot-paths -i ${name}-bfactor1.pdb -p ${name}.paths -o ${name}1.pym
plot-paths -i ${name}-bfactor2.pdb -p ${name}.paths -o ${name}2.pym

pymol -c ${name}1.pym
pymol -c ${name}2.pym

