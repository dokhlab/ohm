import re
import sys

ind_file = sys.argv[1]
peak_file = sys.argv[2]
out_file = sys.argv[3]

inds = []
for line in open(ind_file):
    l = re.split('\s+', line.strip())
    if len(l) > 0:
        inds.append('{0}/{1}'.format(re.split('/', l[1])[1], re.split('/', l[2])[0]))

f = open(out_file, 'w+')
for line in open(peak_file):
    l = re.split('\s+', line.strip())
    l = [int(i)-1 for i in l]
    f.write(' '.join(inds[i] for i in l))
    f.write('\n')
f.close()
