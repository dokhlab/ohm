#! /bin/env python

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from matplotlib import colors
import sys

import cmocean

from colour import Color

newCmp = cmocean.cm.thermal

def get_color(x):
    return newCmp(x)

def read_labels(label_file):
    lines = []
    for line in open(label_file):
        l = line.strip()
        if l != '':
            lines.append(l)
    return lines

def polarBar(ax, x, y1, y2, colors1, colors2, labels):
    n = len(labels)

    ymax1 = np.max(y1)
    ymax2 = np.max(y2)

    y1 = -np.log(y1 + 1)
    y2 = np.log(y2 / 5 + 1)
    # avoid a wedge over the radial labels
    gap = 0.02
    offset = gap * 0.5
    imax = np.max(x)
    imin = np.min(x)
    x = ((x - imin) / (imax - imin) * (1-gap) + offset) * 2 * np.pi
#    with plt.style.context("seaborn-white"):
    for xs, ys, color in zip(x, y1, colors1):
        ax.bar(xs, ys, color=color, width=np.pi*2/n*(1-gap))

#    ax.fill_between(x, 0, y2, color=colors2)
    for xs, ys, color in zip(x, y2, colors2):
        ax.bar(xs, ys, color=color, width=np.pi*2/n*(1-gap))

#        ax.set_title(f"Polar= {polar}", fontsize=15)

    xticks = [(i * (n+1) / float(n*n) * (1 - gap) + offset) * 2 * np.pi for i in range(n)]
    ax.spines['polar'].set_visible(False)
    ax.set_rlabel_position(0)

#            Nxticks = 10
#            xticks = np.linspace(gap/2, 1-gap/2, Nxticks)
#            ax.set_xticks(xticks*np.pi*2)
    ax.set_xticks(xticks)
    ax.set_xticklabels(labels, fontsize=30)
    ax.set_yticklabels([])

    plt.gcf().canvas.draw()
    angles = np.linspace(offset*2*np.pi, 2*np.pi*(1-gap+offset),len(ax.get_xticklabels())+1)
    angles[np.cos(angles) < 0] = angles[np.cos(angles) < 0] + np.pi
    angles = np.rad2deg(angles)
    labels = []
    for label, angle in zip(ax.get_xticklabels(), angles):
        x,y = label.get_position()
        lab = ax.text(x,y, label.get_text(), transform=label.get_transform(), ha=label.get_ha(), va=label.get_va())
        lab.set_rotation(angle)
        labels.append(lab)
    ax.set_xticklabels([])
    ax.grid(False)

#n_colors = 50
#color_gradients = list(Color("blue").range_to(Color("red"), n_colors+1))
#newCmp = colors.ListedColormap([i.rgb for i in color_gradients], name='blue-red')

from matplotlib import cm

cm2 = cm.get_cmap('Greys')

data_file = sys.argv[1]
area_file = sys.argv[2]
label_file = sys.argv[3]
out_file = sys.argv[4]

y1 = np.genfromtxt(data_file)
y1 /= np.max(y1)

y2 = np.genfromtxt(area_file)
y2 /= np.max(y2)

# print(y2)

#colors1 = [color_gradients[int(i * n_colors)].rgb for i in y1]
colors1 = [get_color(i) for i in y1]
#colors2 = [color_gradients[int(i * n_colors)].rgb for i in y2]
colors2 = [cm2(i*0.4+0.3) for i in y2]
labels = read_labels(label_file)

n = len(y1)

#colors = cm.hsv(y / float(max(y)))

fig = plt.figure(figsize=(20, 20))
ax = fig.add_subplot(111, polar=True)

#plot = ax.scatter(y1, y1, c = y1, cmap=newCmp)
#plt.clf()
#plt.colorbar(plot)

polarBar(ax, range(1, n + 1), y1, y2, colors1, colors2, labels)
plt.savefig(out_file)


