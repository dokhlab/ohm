module load anaconda

pdb=$1
active=$2
allosteric=$3
hotspots=$4

fullname=${hotspots%.*}

cat <<! >${fullname}.pym
cmd.bg_color('white')
cmd.load("$pdb", 'main')
cmd.load("$active", 'active')
cmd.load("$allosteric", 'allosteric')
cmd.load("$hotspots", 'hotspots')
cmd.zoom("all")

cmd.show_as('surface', 'main')

cmd.show_as('sticks', 'active')

cmd.show_as('sticks', 'allosteric')

#cmd.alter('hotspots', 'vdw=0.3')
cmd.alter('hotspots', 'vdw=1.5')
cmd.show_as('spheres', 'hotspots')

cmd.spectrum("b",selection="main or active or allosteric or hotspots")
#util.color_chains('hotspots')

cmd.save("${fullname}.pse")
cmd.quit()
!

pymol -c ${fullname}.pym
