module load anaconda
module load ohm

source ./config.sh

contacts ${name}.pdb -oi ${name}.ind -oc ${name}.mat -c $distance_cutoff -a $alpha
python ind2label.py ${name}.ind ${name}.label
perl -lane '$n++;print if $n>1' ${name}.mat >${name}.m
python plot-mat.py ${name}.m ${name}.label ${name}-mat.png

diffuse ${name}.mat $active ${name}.nodes1 ${name}.bonds1 -n $rounds -c $probability_cutoff
diffuse ${name}.mat $allosteric ${name}.nodes2 ${name}.bonds2 -n $rounds -c $probability_cutoff

bfactor ${name}.pdb ${name}.nodes1 ${name}-bfactor1.pdb
bfactor ${name}.pdb ${name}.nodes2 ${name}-bfactor2.pdb

python plot-nodes.py ${name}.nodes1 ${name}-nodes1.png
python plot-nodes.py ${name}.nodes2 ${name}-nodes2.png

diffuse path ${name}.mat $active $allosteric ${name}.paths -n $pathways -c 0.05
python get-network.py ${name}.paths $pathways ${name}.net

plot-paths -i ${name}-bfactor1.pdb -p ${name}.paths -o ${name}1.pym
plot-paths -i ${name}-bfactor2.pdb -p ${name}.paths -o ${name}2.pym

pymol -c ${name}1.pym
pymol -c ${name}2.pym

