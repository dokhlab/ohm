#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include "allosteric_hotspot_finder.hpp"

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    AllostericHotspotFinder finder(opt);
    finder.run();

    return 0;
}

