#include <iostream>
#include <fstream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <sstream>
#include <vector>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    std::string traj_file = opt.argv[1];
    int bin = JN_INT(opt.argv[2]);
    std::string out_file = opt.argv[3];

    std::ifstream ifile(traj_file.c_str());
    std::ofstream ofile(out_file.c_str());
    jnc::bio::Pdb pdb;
    jnc::bio::PdbReader pdb_reader(pdb);
    jnc::bio::PdbWriter writer(ofile);

    int imodel = 0;
    std::cout << std::endl;
    while (ifile) {
        pdb_reader.read_model(ifile);
        if (pdb.size() > 0) {
            auto &model = pdb.back();
            if (imodel % bin == 0) {
                writer.write_model(model);
            }
            pdb.clear();

            std::cout << "\r" << imodel << "          " << std::flush;
            imodel++;
        }
    }
    std::cout << std::endl;
    ifile.close();

    ofile.close();

    return 0;
}

