#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    jnc::bio::Pdb pdb(opt.g[0]);

    std::ofstream ofile(opt.g[1].c_str());
    int n_res = 0;
    int n_chain = 0;
    for (auto && chain : pdb[0]) {
        int n_res_in_chain = 0;
        for (auto && res : chain) {
            ofile << n_res + 1 << ' ' << n_chain + 1 << '/' << chain.name << ' ' << res.num << '/' << res.name << std::endl;
            n_res++;
            n_res_in_chain++;
        }
        n_chain++;
    }
    ofile.close();

    return 0;
}


