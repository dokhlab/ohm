#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <sstream>

template<typename T>
std::array<double, 3> residue_center(const T &residue) {
    std::array<double, 3> c {0, 0, 0};
    int n = 0;
    for (auto && atom : residue) {
        for (int i = 0; i < 3; i++) {
            c[i] += atom[i];
        }
        n++;
    }
    for (int i = 0; i < 3; i++) {
        c[i] /= n;
    }
    return std::move(c);
}

// template<typename T, typename Path_, typename C_>
// std::string plot_path(const T &rs, const Path_ &path, const C_ &c1, const C_ &c2) {
template<typename T, typename Path_>
std::string plot_path(const T &rs, const Path_ &path) {
    int n = path.first.size();

    std::vector<std::array<double, 3>> c;
//    c.push_back(c1);
    for (auto && i : path.first) {
//        std::cerr << i << ' ' << rs.size() << ' ' << rs[i]->size() << std::endl;
        c.push_back(residue_center(*(rs[i])));
    }
//    c.push_back(c2);

    auto &c1 = c.front();
    auto &c2 = c.back();

    double width = 2 * path.second;
    double half_width = width / 2.0;

    std::string start_color = "0.57, 0.05, 0.25";
    std::string path_color = "0.094, 0.621, 0.020";
    std::string end_color = "0.02, 0.22, 0.59";

    std::stringstream ss;
    ss << "cyl = [\n"
       << "COLOR, " << path_color << ",\n"
       << jnc::string_format("SPHERE, %.2f, %.2f, %.2f, %f, \n", c[0][0], c[0][1], c[0][2], half_width)
       << jnc::string_format("25.0, 0.8, CYLINDER, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %f, %s, %s,\n", c[0][0], c[0][1], c[0][2], c[1][0], c[1][1], c[1][2], half_width, path_color, path_color);
//       << "25.0, 0.8, CYLINDER, " << c[0][0] << ", " << c[0][1] << ", " << c[0][2] << ", " << c[1][0] << ", " << c[1][1] << ", " << c[1][2] << ", " << half_width << ", " << path_color << ", " << path_color << ",\n";

    for (int i = 1; i < n - 1; i++) {
        ss << "COLOR, " << path_color << ",\n"
           << jnc::string_format("SPHERE, %.2f, %.2f, %.2f, %f, \n", c[i][0], c[i][1], c[i][2], half_width)
//           << "SPHERE, " << c[i][0] << ", " << c[i][1] << ", " << c[i][2] << ", " << half_width << ",\n"
           << jnc::string_format("25.0, 0.8, CYLINDER, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %f, %s, %s,\n", c[i][0], c[i][1], c[i][2], c[i+1][0], c[i+1][1], c[i+1][2], half_width, path_color, path_color);
//           << "25.0, 0.8, CYLINDER, " << c[i][0] << ", " << c[i][1] << ", " << c[i][2] << ", " << c[i+1][0] << ", " << c[i+1][1] << ", " << c[i+1][2] << ", " << half_width << ", " << path_color << ", " << path_color << ",\n";
    }

    ss << "COLOR, " << path_color << ",\n"
//       << "SPHERE, " << c2[0] << ", " << c2[1] << ", " << c2[2] << ", " << half_width << "\n"
       << jnc::string_format("SPHERE, %.2f, %.2f, %.2f, %f\n", c2[0], c2[1], c2[2], half_width)
       << "]\n";

    return ss.str();

}

static std::vector<int> get_sites(const std::string &str) {
    std::vector<int> sites;
    auto && v = jnc::string_tokenize(str, "+");
    for (auto && s : v) {
        auto && w = jnc::string_tokenize(s, "-");
        if (w.size() == 1) {
            sites.push_back(JN_INT(s) - 1);
        } else if (w.size() == 2) {
            int l = JN_INT(w[0]) - 1;
            int u = JN_INT(w[1]) - 1;
            for (int i = l; i <= u; i++) {
                sites.push_back(i);
            }
        }
    }
    return std::move(sites);
}

static std::string get_selection(const std::string &pdb_name, const std::string &raw_string) {
    auto && v = jnc::string_tokenize(raw_string, ",");
    std::stringstream ss;
    ss << pdb_name << " and (";
    for (int i = 0; i < v.size(); i++) {
        auto && w = jnc::string_tokenize(v[i], "/");
        if (i != 0) {
            ss << " or ";
        }
        ss << "(chain " << w[0] << " and resi " << w[1] << ")";
    }
    ss << ")";
    return ss.str();
}

template<typename T_, typename V_>
static std::string path_selection(const T_ &ind, const V_ &path) {
    std::stringstream ss;
    for (int i = 0; i < path.size(); i++) {
        if (i != 0) {
            ss << " or ";
        }
        ss << "(chain " << ind[path[i]].at("chain") << " and resi " << ind[path[i]].at("resi") << ")";
    }
    return ss.str();
}

static std::vector<std::map<std::string, std::string>> read_ind(const std::string &filename) {
    std::vector<std::map<std::string, std::string>> v;

    std::ifstream ifile(filename.c_str());
    std::string line;
    while (ifile) {
        std::getline(ifile, line);
        auto && w = jnc::string_tokenize(line, " ");
        if (w.size() == 3) {
            std::map<std::string, std::string> m;

            auto && w1 = jnc::string_tokenize(w[1], "/");
            m["chain"] = w1[1];

            auto && w2 = jnc::string_tokenize(w[2], "/");
            m["resi"] = w2[0];

            v.push_back(m);
        }
    }
    ifile.close();

    return v;
}

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    auto pdb_file = opt.get("i");
    auto path_file = opt.get("p");
    auto out_file = opt.get("o");

    auto active = opt.get("active");
    auto allos = opt.get("allos");

    auto && ind = read_ind(opt.get("ind"));

//    auto sites = opt.getv("s");
//    auto sites1 = get_sites(sites[0]);
//    auto sites2 = get_sites(sites[1]);

    int max = 100;
    opt.set(max, "n");

    auto pdb_name = jnc::path_splitext(jnc::path_basename(pdb_file))[0];
    auto out_name = jnc::path_splitext(out_file)[0];

    jnc::bio::Pdb pdb(pdb_file);
    auto rs = pdb[0].presidues();

    auto active_selection = get_selection(pdb_name, active);
    auto allos_selection = get_selection(pdb_name, allos);

//    std::array<double, 3> c1 {0, 0, 0};
//    std::array<double, 3> c2 {0, 0, 0};
//
//    // Set sites1 center
//    int n_atom = 0;
//    for (auto && i : sites1) {
//        for (auto && atom : *rs[i]) {
//            for (int j = 0; j < 3; j++) {
//                c1[j] += atom[j];
//            }
//            n_atom++;
//        }
//    }
//    for (int j = 0; j < 3; j++) c1[j] /= n_atom;
//
//    // Set sites2 center
//    n_atom = 0;
//    for (auto && i : sites2) {
//        for (auto && atom : *rs[i]) {
//            for (int j = 0; j < 3; j++) {
//                c2[j] += atom[j];
//            }
//            n_atom++;
//        }
//    }
//    for (int j = 0; j < 3; j++) c2[j] /= n_atom;

    std::ofstream ofile(out_file.c_str());
    ofile << "from pymol.cgo import *\n"
          << "from pymol import cmd\n"
          << "cmd.load('"<< pdb_file <<"')\n"
          << "cmd.show_as('cartoon', '" << pdb_name << "')\n"
          << "cmd.bg_color('white')\n"
          << "cmd.set('opaque_background',1,u'',0)\n"
          << "cmd.set('cartoon_transparency',0.5,u'',0)\n"
          << "cmd.spectrum('b', selection=('" << pdb_name << "'))\n"
          << "cmd.extract('allos', selection=('" << allos_selection << "'))\n"
          << "cmd.extract('active', selection=('" << active_selection << "'))\n"
//          << "cmd.show_as('sticks', 'allos')\n"
//          << "cmd.show_as('sticks', 'active')\n"
          << "cmd.color('red', 'allos')\n"
          << "cmd.color('blue', 'active')\n"
          << std::endl;
    
    std::ifstream ifile(path_file);
    std::string line;
    std::map<std::vector<int>, double> paths;
    while (ifile) {
        std::getline(ifile, line);
        auto v = jnc::string_tokenize(line, " ");
        if (v.size() == 2) {
            auto weight = JN_DBL(v[0]);

            std::vector<int> path;
            auto w = jnc::string_tokenize(v[1], "-");
            for (auto && i : w) {
                double j = JN_INT(i) - 1;
//                if (std::find(sites1.begin(), sites1.end(), j) == sites1.end() && std::find(sites2.begin(), sites2.end(), j) == sites2.end()) {
                    path.push_back(j);
//                }
            }

            if (!paths.count(path)) {
                paths[path] = 0.0;
            }

            paths[path] += weight;
        }
    }
    ifile.close();

    std::vector<std::pair<std::vector<int>, double>> vpath;
    double weightMax = 0;
    for (auto && p : paths) {
        vpath.push_back(p);
        if (weightMax < p.second) {
            weightMax = p.second;
        }
    }

    for (auto && p : vpath) {
        p.second /= weightMax;
    }

    std::sort(vpath.begin(), vpath.end(), [](auto && p1, auto && p2) {
        return p1.second > p2.second;
    });

    for (int i = 0; i < vpath.size() && i < max; i++) {
//        ofile << plot_path(rs, vpath[i], c1, c2);
        ofile << "cmd.select('path" << i + 1 << "-residues', '" << path_selection(ind, vpath[i].first) << "')\n";
        ofile << plot_path(rs, vpath[i]);
        ofile << "cmd.load_cgo(cyl, 'path" << i+1 << "')\n" << std::endl;
    }

    ofile << "cmd.disable('all')\n"
          << "cmd.enable('" << pdb_name << "')\n"
          << "cmd.enable('active')\n"
          << "cmd.enable('allos')\n"
          << "cmd.enable('path1')\n"
          << "cmd.label('name CA+C1*+C1 and path1-residue', '\"%s-%s\"%(resn,resi)')\n"
          << "cmd.set('label_position', '(0,0,20)')\n"
          << "cmd.set('label_bg_color', -7, u'', 0)\n"
          << "cmd.set('label_color', -6, u'', 0)\n"
          << "cmd.zoom('all',animate=-1)\n"
          << "cmd.save('" << out_name << ".pse')\n"
          << std::endl;

    ofile.close();

    return 0;
}

