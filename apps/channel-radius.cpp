#include <iostream>
#include <fstream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <sstream>
#include <vector>

template<typename T, typename U>
T string_parse(U && u) {
    std::stringstream stream;
    T t;

    stream << u;
    stream >> t;
    return t;
}

std::vector<int> get_indices(const std::string &str) {
    std::vector<int> sites;
    auto && v = jnc::string_tokenize(str, "+");
    for (auto && s : v) {
        auto && w = jnc::string_tokenize(s, "-");
        if (w.size() == 1) {
            sites.push_back(string_parse<int>(s) - 1);
        } else if (w.size() == 2) {
            int l = string_parse<int>(w[0]) - 1;
            int u = string_parse<int>(w[1]) - 1;
            for (int i = l; i <= u; i++) {
                sites.push_back(i);
            }
        }
    }
    return std::move(sites);
}

template<typename Residues, typename Indices>
std::vector<jnc::bio::PdbResidue *> get_res(Residues &rs, const Indices &indices) {
    std::vector<jnc::bio::PdbResidue *> v;
    for (auto && i : indices) {
        v.push_back(rs[i]);
    }
    return std::move(v);
}

template<typename Residue>
double get_rmsd(Residue *r1, Residue *r2) {
    double d, dx, dy, dz;
    int n_atoms;

    n_atoms = r1->size();

    for (int i = 0; i < n_atoms; i++) {
        auto &a1 = (*r1)[i];
        auto &a2 = (*r2)[i];
        dx = a1[0] - a2[0];
        dy = a1[1] - a2[1];
        dz = a1[2] - a2[2];
        d = std::sqrt(dx*dx+dy*dy+dz*dz);
    }

    return d;
}

template<typename Residues_, typename Indices_, typename Mat_>
void set_coords(Residues_ &rs, const Indices_ &inds, Mat_ &m) {
    int n_atoms = 0;
    for (auto && i : inds) {
        auto &r = *rs[i];
        n_atoms += r.size();
    }

    m.resize(n_atoms, 3);

    int j = 0;
    for (auto && i : inds) {
        auto &r = *rs[i];
        for (auto && a : r) {
            m(j, 0) = a[0];
            m(j, 1) = a[1];
            m(j, 2) = a[2];
            j++;
        }
    }
}

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    jnc::bio::PdbModel ref;
    std::vector<jnc::bio::PdbResidue *> rs_ref;
    int n_res;
    std::vector<double> rmsf;
    jnc::Matd x, y;
    double d;

    std::string traj_file = opt.argv[1];
    auto &&inds = get_indices(opt.argv[2]);
    std::string rmsf_file = opt.argv[3];

    std::ifstream ifile(traj_file.c_str());
    jnc::bio::Pdb pdb;
    jnc::bio::PdbReader pdb_reader(pdb);
    int imodel = 0;
    while (ifile) {
        pdb_reader.read_model(ifile);
        if (pdb.size() > 0) {
            auto &model = pdb.back();
            auto &&rs = model.presidues();
            n_res = rs.size();
            if (n_res != 0) {
                std::cout << "\rModel " << imodel+1 << std::flush;
                if (imodel == 0) {
                    ref = model;
                    rs_ref = ref.presidues();
                    rmsf.resize(n_res, 0);
                    set_coords(rs, inds, y);
                } else {
                    set_coords(rs, inds, x);
                    auto && sp = jnc::suppos(x, y);
                    for (int i = 0; i < n_res; i++) {
                        for (auto && a : *rs[i]) {
                            sp.apply(a);
                        }
                        d = get_rmsd(rs_ref[i], rs[i]);
                        rmsf[i] += d;
                    }
                }
                imodel++;
            }
            pdb.clear();
        }
    }
    ifile.close();
    std::cout << std::endl;

    std::ofstream ofile(rmsf_file.c_str());
    for (int i = 0; i < n_res; i++) {
        ofile << rmsf[i] / (imodel-1) << std::endl;
    }
    ofile.close();

    return 0;
}

