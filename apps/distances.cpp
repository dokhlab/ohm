#include <iostream>
#include <fstream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <sstream>
#include <vector>

template<typename T, typename U>
T string_parse(U && u) {
    std::stringstream stream;
    T t;

    stream << u;
    stream >> t;
    return t;
}

std::vector<int> get_indices(const std::string &str) {
    std::vector<int> sites;
    auto && v = jnc::string_tokenize(str, "+");
    for (auto && s : v) {
        auto && w = jnc::string_tokenize(s, "-");
        if (w.size() == 1) {
            sites.push_back(string_parse<int>(s) - 1);
        } else if (w.size() == 2) {
            int l = string_parse<int>(w[0]) - 1;
            int u = string_parse<int>(w[1]) - 1;
            for (int i = l; i <= u; i++) {
                sites.push_back(i);
            }
        }
    }
    return std::move(sites);
}

template<typename Residues, typename Indices>
std::vector<jnc::bio::PdbResidue *> get_res(Residues &rs, const Indices &indices) {
    std::vector<jnc::bio::PdbResidue *> v;
    for (auto && i : indices) {
        v.push_back(rs[i]);
    }
    return std::move(v);
}

template<typename Residue>
double residue_distance(Residue *r1, Residue *r2) {
    double d_min, d, dx, dy, dz;
    
    d_min = 999999;
    for (auto && a1 : *r1) {
        for (auto && a2 : *r2) {
            dx = a1[0] - a2[0];
            dy = a1[1] - a2[1];
            dz = a1[2] - a2[2];
            d = std::sqrt(dx*dx+dy*dy+dz*dz);
            if (d_min > d) {
                d_min = d;
            }
        }
    }
    return d_min;
}

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    auto &&inds1 = get_indices(opt.argv[2]);
    auto &&inds2 = get_indices(opt.argv[3]);
    std::ofstream ofile(opt.argv[4]);

    std::ifstream ifile(opt.argv[1]);
    jnc::bio::Pdb pdb;
    jnc::bio::PdbReader pdb_reader(pdb);
    int imodel = 0;
    while (ifile) {
        pdb_reader.read_model(ifile);
        auto &model = pdb.back();
        auto &&rs = model.presidues();
        if (rs.size() != 0) {
            std::cout << "Model " << imodel+1 << std::endl;
            auto &&rs1 = get_res(rs, inds1);
            auto &&rs2 = get_res(rs, inds2);
            for (auto && r1 : rs1) {
                for (auto && r2 : rs2) {
                    double d = residue_distance(r1, r2);
                    ofile << d << ' ';
                }
            }
            ofile << std::endl;
            imodel++;
        }
        pdb.clear();
    }
    ifile.close();
    ofile.close();

    return 0;
}

