#include "argparse.hpp"
#include "grid.hpp"
#include "jnc/bio"
#include "jnc/core"
#include "jnc/math"
#include <algorithm>
#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <ranges>
#include <string>
#include <unordered_map>
#include <vector>

int atom_type_index(const std::string &name) {
  if (name[0] == 'C') {
    if (name[1] == 'l') {
      return 5;
    } else {
      return 0;
    }
  } else if (name[0] == 'O') {
    return 1;
  } else if (name[0] == 'N') {
    return 2;
  } else if (name[0] == 'S') {
    return 3;
  } else if (name[0] == 'P') {
    return 4;
  } else {
    return 5;
  }
}

int main(int argc, char **argv) {
  argparse::ArgumentParser parser("pocket-grid");

  parser.add_argument("in").help("specify the input file.");
  parser.add_argument("out").help("specify the output file.");
  parser.add_argument("-b", "--bin").default_value(0.5).help("specify the bin size.").scan<'g', double>();
  parser.add_argument("-d", "--dim").default_value(64).help("specify the dimension of the grid.").scan<'d', int>();
  parser.add_argument("-l", "--lig").default_value(std::string("UNK")).help("specify the name of the ligand.");

  try {
    parser.parse_args(argc, argv);
  } catch (const std::runtime_error &err) {
    std::cerr << err.what() << std::endl;
    std::cerr << parser;
    std::exit(1);
  }

  auto infile = parser.get("in");
  auto outfile = parser.get("out");
  auto grid_bin = parser.get<double>("--bin");
  auto grid_dim = parser.get<int>("--dim");
  auto lig_name = parser.get("--lig");

  double side_length = grid_bin * grid_dim;

  // get the center of the ligand
  jnc::bio::Pdb pdb(infile);
  auto &&center = pdb[0] | std::views::join | std::views::filter([&lig_name](auto &r) { return r.name == lig_name; }) |
                  std::views::join | jnc::vec_center<std::array<double, 3>>;

  // get the min and max of x y and z
  std::array<std::array<double, 2>, 3> boundary;
  for (int i = 0; i < 3; i++) {
    boundary[i][0] = center[i] - side_length / 2;
    boundary[i][1] = center[i] + side_length / 2;
  }

  // traverse all the atom and retain those that satisfy the min and max
  std::ofstream ofile(outfile.c_str());
  ofile << 2 << ' ' << grid_dim << ' ' << grid_dim << ' ' << grid_dim << ' ' << 6 << std::endl;
  for (auto &&residue : pdb[0] | std::views::join) {
    int is_ligand = (residue.name == lig_name ? 1 : 0);
    for (auto &&atom : residue) {
      if (atom.name[0] != 'H') {
        if (std::ranges::all_of(std::views::iota(0, 3), [&atom, &boundary](int i) {
              return atom[i] >= boundary[i][0] && atom[i] < boundary[i][1];
            })) {
          ofile << is_ligand << ' ';
          for (int i = 0; i < 3; i++) {
            ofile << int((atom[i] - boundary[i][0]) / grid_bin) << ' ';
          }
          ofile << atom_type_index(atom.name) << std::endl;
        }
      }
    }
  }
  ofile.close();
  return 0;
}
