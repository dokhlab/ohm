#include "argparse.hpp"
#include "grid.hpp"
#include "jnc/bio"
#include "jnc/core"
#include "jnc/math"
#include <algorithm>
#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <ranges>
#include <string>
#include <unordered_map>
#include <vector>

#include <iostream>
#include <string>
#include <map>

std::string upper(std::string s) {
    std::string u = s;
    std::transform(u.begin(), u.end(), u.begin(), ::toupper);
    return u;
}

struct AminoAcidIndexer {
    std::vector<std::string> aminoAcids;
    std::map<std::string, int> aminoAcidIndex;

    AminoAcidIndexer() {
        // Initialize amino acids in the constructor
        aminoAcids = {"ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE",
                      "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL",
                      "SEC", "PYL"};

        createAminoAcidIndex();
    }

    int nIndices() {
        return aminoAcids.size() + 1; // 23
    }

    int index(const std::string& aminoAcid) const {
        auto it = aminoAcidIndex.find(upper(aminoAcid));
        return (it != aminoAcidIndex.end()) ? it->second : 0;
    }

    void createAminoAcidIndex() {
        for (size_t i = 0; i < aminoAcids.size(); ++i) {
            aminoAcidIndex[aminoAcids[i]] = i + 1;  // Index starts from 1
        }
    }
};

// Calculate the distance between receptor atom and ligand atom
template<typename RA_, typename LA_> double rl_distance(const RA_ &ra, const LA_ &la) {
    double dx = ra[0] - la.x;
    double dy = ra[1] - la.y;
    double dz = ra[2] - la.z;
    return std::sqrt(dx * dx + dy * dy + dz * dz);
}

// Calculate the distance between two nodes
template<typename Node1_, typename Node2_> double nn_distance(const Node1_ &n1, const Node2_ &n2) {
    double dx = n1.x - n2.x;
    double dy = n1.y - n2.y;
    double dz = n1.z - n2.z;
    return std::sqrt(dx * dx + dy * dy + dz * dz);
}

// whether a residue contacts the ligand. 1: yes. 0: no
template<typename Residue_, typename Ligand_> int within_pocket(const Residue_ &r, const Ligand_ &l) {
    for (auto && ra : r) {
        for (auto && la : l.atoms) {
            double d = rl_distance(ra, la);
            if (d < 5) {
                return 1;
            }
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    argparse::ArgumentParser parser("receptor-graph");

    parser.add_argument("receptor").help("specify the receptor file (*.pdb).");
    parser.add_argument("ligand").help("specify the ligand file (*.mol2).");
    parser.add_argument("out").help("specify the output file.");
    // parser.add_argument("-b", "--box").default_value(20).help("specify the dimension of the grid.").scan<'d', int>();

    try {
        parser.parse_args(argc, argv);
    } catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
        std::cerr << parser;
        std::exit(1);
    }

    auto receptor_file = parser.get("receptor");
    auto ligand_file = parser.get("ligand");
    auto outfile = parser.get("out");
    // auto side_length = parser.get<int>("--box");
    double max_distance = 10.0;

    jnc::bio::Pdb receptor(receptor_file);
    jnc::bio::Mol2 ligand(ligand_file);
    ligand.remove_hydrogens();

    AminoAcidIndexer aaIndexer;

    struct Node {
        int id;
        int type; // 0: unknown residue type, 1-n: known residue types, n+1: unknown atom type, n+2-m: known atom types
        int in_pocket;
        double x;
        double y;
        double z;
    };

    struct Edge {
        int i;
        int j;
        int has_bond;
        double distance;
    };

    std::vector<Node> nodes;
    std::map<std::pair<int, int>, Edge> edges;

    // find all residues that are inside the pocket
    int inode = 0;
    std::string last_chain_name = "";
    int last_residue_num = -1;
    for (auto && chain : receptor[0]) {
        for (auto && residue : chain) {
            if (residue.has_atom("CA")) {
                auto &atom = residue["CA"];
                // check if this residue contacts the ligand
                int in_pocket = within_pocket(residue, ligand);
                nodes.push_back(Node{inode, aaIndexer.index(residue.name), in_pocket, atom[0], atom[1], atom[2]});
                int nNodes = nodes.size();
                if (last_chain_name == chain.name && last_residue_num + 1 == residue.num) {
                    double d = nn_distance(nodes[nNodes-2], nodes[nNodes-1]);
                    if (d < 4.5) {
                        edges[{inode-1, inode}] = Edge{inode-1, inode, 1, d};
                    }
                }
                inode++;
                last_chain_name = chain.name;
                last_residue_num = residue.num;
            }
        }
    }

    // scan for non-bonded contacts
    for (auto && n1: nodes) {
        for (auto && n2 : nodes) {
            if (n2.id > n1.id) {
                double d = nn_distance(n1, n2);
                if (d < max_distance) {
                    if (edges.find(std::pair<int,int>{n1.id,n2.id}) == edges.end()) {
                        edges[{n1.id,n2.id}] = Edge{n1.id, n2.id, 0, nn_distance(n1,n2)};
                    }
                }
            }
        }
    }

    // traverse all the atom and retain those that satisfy the min and max
    std::ofstream ofile(outfile.c_str());
    ofile << "NODES" << std::endl;
    for (auto && node : nodes) {
        ofile << node.id << ' ' << node.type << ' ' << node.in_pocket << ' ' << node.x << ' ' << node.y << ' ' << node.z << std::endl;
    }
    ofile << "EDGES" << std::endl;
    for (auto && p : edges) {
        auto &edge = p.second;
        ofile << edge.i << ' ' << edge.j << ' ' << edge.has_bond << ' ' << edge.distance << std::endl;
    }
    ofile.close();
    return 0;
}
