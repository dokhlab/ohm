#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

using AxisRange = std::array<double, 2>;
using Range = std::array<AxisRange, 3>;
using Id = std::array<int, 3>;
using Grid = std::map<Id, std::set<int>>;

template<typename T>
Id get_id(const T &c, const Range &range, double box_size) {
    Id id;
    for (int i = 0; i < 3; i++) {
        id[i] = int((c[i] - range[i][0]) / box_size);
    }
    return id;
}

auto get_sites(const std::string &str) {
    auto v = jnc::string_tokenize(str, "+");
    std::vector<int> sites;
    for (auto && s : v) {
        auto w = jnc::string_tokenize(s, "-");
        if (w.size() == 1) {
            sites.push_back(JN_INT(w[0])-1);
        } else if (w.size() == 2) {
            int l = JN_INT(w[0])-1;
            int u = JN_INT(w[1])-1;
            for (int i = l; i <= u; i++) {
                sites.push_back(i);
            }
        }
    }
    return sites;
}

template<typename Neighbors_, typename CheckedIds_>
void add_neighbors(const Id &id, Neighbors_ &neighbors, Grid &grid, CheckedIds_ &checked_ids) {
    if (checked_ids.find(id) == checked_ids.end()) {
        checked_ids.insert(id);

        auto & ls = grid[id];

        for (auto && i : ls) {
            neighbors.insert(i);
        }

        if (!ls.empty()) {
            for (int i = -1; i <= 1; i++) {
                for (int j = -1; j <= 1; j++) {
                    for (int k = -1; k <= 1; k++) {
                        Id newId {id[0] + i, id[1] + j, id[2] + k};
                        add_neighbors(newId, neighbors, grid, checked_ids);
                    }
                }
            }
        }
    }
}

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    std::string pdb_name = opt.g[0];
    std::string sites_str = opt.g[1];
    std::string out_name = opt.g[2];

    double box_size = 4;
    opt.set(box_size, "b");

    jnc::bio::Pdb pdb(pdb_name);
    auto sites = get_sites(sites_str);

    auto rs = pdb[0].presidues();
    int n = rs.size();

    Grid grid;
    Range range {AxisRange {99999, 0}, AxisRange {99999, 0}, AxisRange {99999, 0}};

    // Set range
    for (int i = 0; i < n; i++) {
        auto & r = *(rs[i]);
        for (auto && atom : r) {
            for (int j = 0; j < 3; j++) {
                if (range[j][0] > atom[j]) {
                    range[j][0] = atom[j];
                }
                if (range[j][1] < atom[j]) {
                    range[j][1] = atom[j];
                }
            }
        }
    }

    // Set Grid
    for (int i = 0; i < n; i++) {
        auto & r = *(rs[i]);
        for (auto && atom : r) {
            auto id = get_id(atom, range, box_size);
            grid[id].insert(i);
        }
    }

    // Set neighbors
    std::set<int> neighbors;
    std::set<Id> checked_ids;
    for (auto && site : sites) {
        auto & residue = *(rs[site]);
        for (auto && atom : residue) {
            auto id = get_id(atom, range, box_size);
            add_neighbors(id, neighbors, grid, checked_ids);
        }
    }

    // Print neighbors
    std::ofstream ofile(out_name.c_str());
    for (auto neighbor : neighbors) {
        ofile << neighbor+1 << std::endl;
    }
    ofile.close();

    return 0;
}

