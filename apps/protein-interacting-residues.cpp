#include "argparse.hpp"
#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

int main(int argc, char **argv) {
  using namespace jnc;

  argparse::ArgumentParser parser("extract-rna");

  parser.add_argument("-i", "--input").help("specify the input file.");
  parser.add_argument("-p", "--prefix").help("specify the prefix of output files.");

  try {
    parser.parse_args(argc, argv);
  } catch (const std::runtime_error &err) {
    std::cerr << err.what() << std::endl;
    std::cerr << parser;
    std::exit(1);
  }

  auto infile = parser.present("-i");
  auto prefix = parser.get("-p");

  auto pdb = infile ? bio::read_pdb(*infile) : bio::read_pdb(std::cin);

  auto rna_file = prefix + "-rna.pdb";
  auto ind_file = prefix + ".ind";

  // initialize a grid to record the positions of protein residues
  using Id = std::array<int, 3>;
  using Grid = std::map<Id, std::set<bio::PdbResidue *>>;

  for (auto && chain : pdb[0]) {
    // if bio::is_protein(chain);
  }

  std::ofstream rna_out(rna_file.c_str());
  std::ofstream ind_out(ind_file.c_str());
  int n_res = 0;
  int n_chain = 0;
  for (auto &&chain : pdb[0]) {
    int n_res_in_chain = 0;
    int is_rna = bio::is_rna(chain);
    for (auto &&res : chain) {
      for (auto &&atom : res) {
        if (is_rna) {
          rna_out << bio::pdb_line("ATOM", atom.num, atom.name, res.name, chain.name, res.num, atom[0], atom[1], atom[2])
                  << std::endl;
        }
      }
      ind_out << n_res + 1 << ' ' << n_chain + 1 << '/' << chain.name << ' ' << res.num << '/' << res.name << std::endl;
      n_res++;
      n_res_in_chain++;
    }
    if (is_rna) rna_out << "TER" << std::endl;
    n_chain++;
  }
  rna_out.close();
  ind_out.close();

  return 0;
}
