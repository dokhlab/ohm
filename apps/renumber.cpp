#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    jnc::bio::Pdb pdb(opt.g[0]);

    int ires = 1;
    for (auto && chain : pdb[0]) {
        for (auto && res : chain) {
            res.num = ires;
            ires++;
        }
    }
    std::ofstream ofile(opt.g[1].c_str());
    ofile << pdb;
    ofile.close();

    return 0;
}

