#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <jnc/core>
#include <vector>
#include <map>

#include "mat.hpp"

using namespace std;

using Neighbors = vector<vector<int>>;
static Neighbors get_neighbors(const Matd &mat, double cutoff, int direct = 1) {
    int n = mat.rows();
    Neighbors neighbors(n);
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if ((mat(i, j) - cutoff) * direct > 0) {
                neighbors[i].push_back(j);
                neighbors[j].push_back(i);
            }
        }
    }
    return move(neighbors);
}

template<typename R1_, typename R2_>
double residue_distance(const R1_ r1, const R2_ r2) {
    double min = 9999;
    for (auto && a1 : r1) {
        for (auto && a2 : r2) {
            double d = jnc::distance(a1, a2);
            if (min > d) {
                min = d;
            }
        }
    }
    return min;
}

template<typename T>
vector<T> read_vector(const string &filename) {
    vector<T> v;
    ifstream ifile(filename.c_str());
    string line;
    while (ifile) {
        getline(ifile, line);
        jnc::string_trim(line);
        auto w = jnc::string_tokenize(line);
        if (w.size() > 0) {
            v.push_back(jnc::lexical_cast<T>(w[0]));
        }
    }
    ifile.close();
    return v;
}

template<typename Direct_>
int get_cluster(int i, const Direct_ direct) {
    if (direct[i] == -1) {
        return i;
    } else {
        return get_cluster(direct[i], direct);
    }
}

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    auto pdbfile = opt.argv[1];
    auto vfile = opt.argv[2];
    auto outfile = opt.argv[3];

    jnc::bio::Pdb pdb(pdbfile);

    auto pr = pdb[0].presidues();

    int n = pr.size();
    Matd m(n, n, 0);

    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            m(i, j) = m(j, i) = residue_distance(*pr[i], *pr[j]);
        }
    }

    auto neighbors = get_neighbors(m, 10, -1);

    auto v = read_vector<double>(vfile);

    if (v.size() != n) {
        cerr << "Number of reidues not equal in the two input files!" << endl;
        cerr << pdbfile << ": " << n << " residues" << endl;
        cerr << vfile << ": " << v.size() << " residues" << endl;
    }

    vector<int> direct(n);
    for (int i = 0; i < n; i++) {
        double min = 9999;
        int ind = -1;
        for (auto && j : neighbors[i]) {
            if (v[j] > v[i]) {
                double d = m(i, j);
                if (min > d) {
                    min = d;
                    ind = j;
                }
            }
        }
        direct[i] = ind;
    }

    for (int i = 0; i < n; i++) {
        if (direct[i] == -1 && v[i] != 0) {
            for (int j = i + 1; j < n; j++) {
                if (direct[j] == -1 && v[j] != 0) {
                    if (m(i, j) < 5) {
                        if (v[i] > v[j]) {
                            direct[j] = i;
                        } else {
                            direct[i] = j;
                        }
                    }
                }
            }
        }
    }

    map<int, vector<int>> clusters;
    for (int i = 0; i < n; i++) {
        if (v[i] != 0 && direct[i] == -1) {
            clusters[i] = vector<int>{i};
        }
    }

    vector<int> cluster(n);
    for (int i = 0; i < n; i++) {
        if (v[i] != 0) {
            cluster[i] = get_cluster(i, direct);
        }
    }

    for (int i = 0; i < n; i++) {
        if (v[i] != 0 && direct[i] != -1) {
            if (m(i, cluster[i]) < 4.5 || v[i] / v[cluster[i]] > 0.7) {
                clusters[cluster[i]].push_back(i);
            }
        }
    }

    vector<vector<int>> sorted_clusters;
    for (auto && p : clusters) {
        sorted_clusters.push_back(p.second);
    }
    sort(sorted_clusters.begin(), sorted_clusters.end(), [&v](auto &&v1, auto &&v2){
        if (v[v1[0]] > v[v2[0]]) {
            return true;
        } else if (v[v1[0]] == v[v2[0]] && v1.size() > v2.size()) {
            return true;
        } else {
            return false;
        }
    });
//    for (auto && c : sorted_clusters) {
//        sort(next(c.begin(), 1), c.end(), [&c](int i){
//        });
//    }

    ofstream ofile(outfile);
    for (auto && c : sorted_clusters) {
        for (auto && i : c) {
            ofile << i + 1 << ' ';
        }
        ofile << endl;
    }
    ofile.close();

    return 0;
}

