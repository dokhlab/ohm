/**
 * This program extracts the pocket from a protein structure and converts it to a graph.
*/

#include "argparse.hpp"
#include "grid.hpp"
#include "jnc/bio"
#include "jnc/core"
#include "jnc/math"
#include <algorithm>
#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <ranges>
#include <string>
#include <unordered_map>
#include <vector>

#include <iostream>
#include <string>
#include <map>

std::string upper(std::string s) {
    std::string u = s;
    std::transform(u.begin(), u.end(), u.begin(), ::toupper);
    return u;
}

struct AminoAcidIndexer {
    std::vector<std::string> aminoAcids;
    std::map<std::string, int> aminoAcidIndex;

    AminoAcidIndexer() {
        // Initialize amino acids in the constructor
        aminoAcids = {"ALA", "ARG", "ASN", "ASP", "CYS", "GLN", "GLU", "GLY", "HIS", "ILE",
                      "LEU", "LYS", "MET", "PHE", "PRO", "SER", "THR", "TRP", "TYR", "VAL",
                      "SEC", "PYL"};

        createAminoAcidIndex();
    }

    int nIndices() {
        return aminoAcids.size() + 1; // 23
    }

    int index(const std::string& aminoAcid) const {
        auto it = aminoAcidIndex.find(upper(aminoAcid));
        return (it != aminoAcidIndex.end()) ? it->second : 0;
    }

    void createAminoAcidIndex() {
        for (size_t i = 0; i < aminoAcids.size(); ++i) {
            aminoAcidIndex[aminoAcids[i]] = i + 1;  // Index starts from 1
        }
    }
};

struct AtomTypeIndexer {
    std::vector<std::string> atomTypes;
    std::map<std::string, int> atomTypeIndex;

    AtomTypeIndexer() {
        // Initialize amino acids in the constructor
        atomTypes = {"C", "H", "N", "O", "S", "P", "F", "CL", "BR", "I", "LI", "MG", "FE"};
        createAtomTypeIndex();
    }

    int nIndices() {
        return atomTypes.size() + 1; // 14
    }

    int index(const std::string& atomType) const {
        auto it = atomTypeIndex.find(upper(atomType));
        return (it != atomTypeIndex.end()) ? it->second : -1;
    }

    void createAtomTypeIndex() {
        for (size_t i = 0; i < atomTypes.size(); ++i) {
            atomTypeIndex[atomTypes[i]] = i + 1;  // Index starts from 1
        }
    }
};

std::string extractElementType(const std::string& atomType) {
    // Find the position of the first dot ('.') in the atomType
    size_t dotPosition = atomType.find('.');

    // Extract the substring before the dot (if found)
    if (dotPosition != std::string::npos) {
        return atomType.substr(0, dotPosition);
    } else {
        // If no dot is found, return the entire atomType
        return atomType;
    }
}

struct BondTypeIndexer {
    std::vector<std::string> bondTypes;
    std::map<std::string, int> bondTypeIndex;

    BondTypeIndexer() {
        // • 1 = single
        // • 2 = double
        // • 3 = triple
        // • am = amide
        // • ar = aromatic
        // • du = dummy
        // • un = unknown (cannot be determined from the parameter tables)
        // • nc = not connected
        bondTypes = {"1", "2", "3", "am", "ar"};
        createBondTypeIndex();
    }

    int nIndices() {
        return bondTypes.size() + 1;
    }

    int index(const std::string& bondType) const {
        if (bondType == "nc") {
            return -1;
        } else {
            auto it = bondTypeIndex.find(upper(bondType));
            return (it != bondTypeIndex.end()) ? it->second : 0;
        }
    }

    void createBondTypeIndex() {
        for (size_t i = 0; i < bondTypes.size(); ++i) {
            bondTypeIndex[bondTypes[i]] = i + 1;  // Index starts from 1
        }
    }
};

int main(int argc, char **argv) {
    argparse::ArgumentParser parser("pocket-grid");

    parser.add_argument("receptor").help("specify the receptor file (*.pdb).");
    parser.add_argument("ligand").help("specify the ligand file (*.mol2).");
    parser.add_argument("out").help("specify the output file.");
    parser.add_argument("-b", "--box").default_value(20).help("specify the dimension of the grid.").scan<'d', int>();

    try {
        parser.parse_args(argc, argv);
    } catch (const std::runtime_error &err) {
        std::cerr << err.what() << std::endl;
        std::cerr << parser;
        std::exit(1);
    }

    auto receptor_file = parser.get("receptor");
    auto ligand_file = parser.get("ligand");
    auto outfile = parser.get("out");
    auto side_length = parser.get<int>("--box");

    jnc::bio::Pdb receptor(receptor_file);
    jnc::bio::Mol2 ligand(ligand_file);
    ligand.remove_hydrogens();

    AminoAcidIndexer aaIndexer;
    AtomTypeIndexer atomIndexer;
    BondTypeIndexer bondIndexer;

    // get the center of the ligand
    std::array<double, 3> center = {0, 0, 0};
    for (auto && atom : ligand.atoms) {
        center[0] += atom.x;
        center[1] += atom.y;
        center[2] += atom.z;
    }
    for (int i = 0; i < 3; i++) {
        center[i] /= ligand.atoms.size();
    }

    // get the min and max of x y and z
    std::array<std::array<double, 2>, 3> boundary;
    for (int i = 0; i < 3; i++) {
        boundary[i][0] = center[i] - side_length / 2;
        boundary[i][1] = center[i] + side_length / 2;
    }

    struct Node {
        int id;
        int isReceptor;
        int type; // 0: unknown residue type, 1-n: known residue types, n+1: unknown atom type, n+2-m: known atom types
        double x;
        double y;
        double z;
    };

    struct Edge {
        int i;
        int j;
        int type; // 0: no bond, 1: residue connectivity in receptor, 2: single bond in ligand, ...
    };

    std::vector<Node> nodes;
    std::vector<Edge> edges;

    // find all residues that are inside the pocket
    std::vector<jnc::bio::PdbResidue *> residues;
    int inode = 0;
    std::string last_chain_name = "";
    int last_residue_num = -1;
    for (auto && chain : receptor[0]) {
        for (auto && residue : chain) {
            if (residue.has_atom("CA")) {
                auto &atom = residue["CA"];
                if (std::ranges::all_of(std::views::iota(0, 3), [&atom, &boundary](int i) {
                        return atom[i] >= boundary[i][0] && atom[i] < boundary[i][1];
                    })) {
                    nodes.push_back(Node{inode, 1, aaIndexer.index(residue.name), atom[0], atom[1], atom[2]});
                    if (last_chain_name == chain.name && last_residue_num + 1 == residue.num) {
                        edges.push_back(Edge{inode-1, inode, 1});
                    }
                    inode++;
                    last_chain_name = chain.name;
                    last_residue_num = residue.num;
                }
            }
        }
    }

    // traverse the ligand
    std::map<int, int> m;
    for (auto && atom : ligand.atoms) {
        m[atom.id] = inode;
        nodes.push_back(Node{inode, 0, atomIndexer.index(extractElementType(atom.type))+aaIndexer.nIndices(), atom.x, atom.y, atom.z});
        inode++;
    }
    for (auto && bond : ligand.bonds) {
        int bond_type = bondIndexer.index(bond.type);
        if (bond_type != -1) {
            edges.push_back(Edge{m[bond.origin_atom_id], m[bond.target_atom_id], bond_type+2});
        }
    }

    std::ofstream ofile(outfile.c_str());
    ofile << "NODES" << std::endl;
    for (auto && node : nodes) {
        ofile << node.id << ' ' << node.isReceptor << ' ' << node.type << ' ' << node.x << ' ' << node.y << ' ' << node.z << std::endl;
    }
    ofile << "EDGES" << std::endl;
    for (auto && edge : edges) {
        ofile << edge.i << ' ' << edge.j << ' ' << edge.type << std::endl;
    }
    ofile.close();
    return 0;
}
