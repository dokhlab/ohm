#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    std::string in_pdb = opt.argv[1];
    std::string cluster_file = opt.argv[2];
    std::string pym_file = opt.argv[3];

    // Read PDB
//    jnc::bio::Pdb pdb(in_pdb);

//    auto rs = pdb[0].presidues();
//    int n = rs.size();

    // Read cluster cluster_file
    std::ifstream ifile(cluster_file.c_str());
    std::ofstream ofile(pym_file.c_str());
    ofile << "cmd.bg_color('white')" << std::endl;
    ofile << jnc::string_format("cmd.load('%s')", in_pdb) << std::endl;
    ofile << "cmd.color('white', 'all')" << std::endl;
    std::string line;
    int n_cluster = 0;
    while (ifile) {
        std::getline(ifile, line);
        auto &&v = jnc::string_tokenize(line, ":");
        if (v.size() == 2) {
            auto &&w = jnc::string_tokenize(v[0], " ");
            if (w.size() == 3) {
                auto &&u = jnc::string_tokenize(v[1], " ");
                std::vector<std::string> selection;
                for (auto && i : u) {
                    auto &&j = jnc::string_tokenize(i, "-");
                    if (j.size() == 2) {
                        selection.push_back(jnc::string_format("(chain %s and resi %s)", j[0], j[1]));
                    }
                }
                auto &&cluster_selection = jnc::string_join(" or ", selection);

                auto &&cluster_color = w[2];
                auto &&cluster_name = jnc::string_format("cluster-%d", n_cluster + 1);
                ofile << jnc::string_format("cmd.select('%s', '%s')", cluster_name, cluster_selection) << std::endl;
                ofile << jnc::string_format("h = '%s'", cluster_color) << std::endl;
                ofile << "h = h.lstrip('#')" << std::endl;
                auto &&color_name = jnc::string_format("cluster_%d_color", n_cluster + 1);
                ofile << jnc::string_format("%s = [int(h[i:i+2], 16) for i in (0, 2, 4)]", color_name) << std::endl;
                ofile << jnc::string_format("cmd.set_color('%s', %s)", color_name, color_name) << std::endl;
                ofile << jnc::string_format("cmd.color('%s', '%s')", color_name, cluster_name) << std::endl;
                n_cluster++;
            }
        }
    }
    ofile << "cmd.zoom('all', animate=-1)" << std::endl;
    ofile << jnc::string_format("cmd.save('%s.pse')", jnc::path_splitext(pym_file)[0]) << std::endl;
    ofile << "cmd.quit()" << std::endl;

    ifile.close();
    ofile.close();

    return 0;
}

