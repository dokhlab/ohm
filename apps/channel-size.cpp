#include <iostream>
#include <fstream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <sstream>
#include <vector>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    std::string traj_file = opt.argv[1];
    int i1 = JN_INT(opt.argv[2]);
    int i2 = JN_INT(opt.argv[3]);
    int i3 = JN_INT(opt.argv[4]);

    std::ifstream ifile(traj_file.c_str());
    jnc::bio::Pdb pdb;
    jnc::bio::PdbReader pdb_reader(pdb);

    int imodel = 0;
    while (ifile) {
        pdb_reader.read_model(ifile);
        if (pdb.size() > 0) {
            auto &model = pdb.back();

            auto rs = model.presidues();
            int n = rs.size();

            std::array<double, 3> c1, c2, c3;
            for (int i = 0; i < 3; i++) {
                c1[i] = 0;
                c2[i] = 0;
                c3[i] = 0;
            }

            for (auto && atom : *rs[i1]) {
                for (int i = 0; i < 3; i++) {
                    c1[i] += atom[i];
                }
            }
            for (int i = 0; i < 3; i++) {
                c1[i] /= rs[i1]->size();
            }

            for (auto && atom : *rs[i2]) {
                for (int i = 0; i < 3; i++) {
                    c2[i] += atom[i];
                }
            }
            for (int i = 0; i < 3; i++) {
                c2[i] /= rs[i2]->size();
            }

            for (auto && atom : *rs[i3]) {
                for (int i = 0; i < 3; i++) {
                    c3[i] += atom[i];
                }
            }
            for (int i = 0; i < 3; i++) {
                c3[i] /= rs[i3]->size();
            }

            double a = jnc::distance(c1, c2);
            double b = jnc::distance(c1, c3);
            double c = jnc::distance(c3, c2);

            double p = (a + b + c) / 2;
            double s = std::sqrt(p * (p - a) * (p - b) * (p - c));

            pdb.clear();

            std::cout << std::sqrt(s) << std::endl;
        }
    }
    ifile.close();

    return 0;
}

