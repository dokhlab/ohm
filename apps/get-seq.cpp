#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <fstream>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    jnc::bio::Pdb pdb(opt.g[0]);
    std::string out_file = opt.g[1];

    std::map<std::string, std::string> aa_map = {
        {"ALA", "A"},
        {"ARG", "R"},
        {"ASN", "N"},
        {"ASP", "D"},
        {"CYS", "C"},
        {"GLU", "E"},
        {"GLN", "Q"},
        {"GLY", "G"},
        {"HIS", "H"},
        {"ILE", "I"},
        {"LEU", "L"},
        {"LYS", "K"},
        {"MET", "M"},
        {"PHE", "F"},
        {"PRO", "P"},
        {"SER", "S"},
        {"THR", "T"},
        {"TRP", "W"},
        {"TYR", "Y"},
        {"VAL", "V"}
    };

    int ires = 1;
    std::ofstream ofile(out_file.c_str());
    for (auto && chain : pdb[0]) {
        for (auto && res : chain) {
            if (aa_map.count(res.name)) {
//                ofile << aa_map[res.name] << " " << ires << std::endl;
                ofile << aa_map[res.name];
            } else {
//                ofile << "X" << " " << ires << std::endl;
                ofile << "X";
            }
            ires++;
        }
    }
    ofile << std::endl;
    ofile.close();

    return 0;
}

