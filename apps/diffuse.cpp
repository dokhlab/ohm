#include <iostream>
#include <array>
#include <algorithm>
#include <random>
#include <string>
#include <cstring>
#include <vector>
#include <fstream>
#include <sstream>
#include <utility>
#include <functional>
#include <map>

#include <jnc/core>

#include "mat.hpp"

using namespace std;

mt19937 rand_engine { 11 };
uniform_real_distribution<double> unif_distr { 0, 1 };

/**
 * Generate a random float number between 0 and 1.
 */
void my_seed(int seed) {
    rand_engine.seed(seed);
}

double my_rand() {
    return unif_distr(rand_engine);
}

/**
 * Parse a string to type T.
 */
template<typename T, typename U>
T string_parse(U && u) {
    stringstream stream;
    T t;

    stream << u;
    stream >> t;
    return t;
}

using Neighbors = vector<vector<int>>;
using NodePropensities = vector<double>;
using BondPropensities = Matd;
using Path = deque<int>;
using PathPropensities = map<Path, double>;

void set_alpha(Matd &mat, double alpha) {
    int n = mat.rows();
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            mat(i, j) = mat(j, i) = 1 - std::exp(-alpha * mat(i, j));
        }
    }
}

static Neighbors get_neighbors(const Matd &mat, double cutoff, int direct = 1) {
    int n = mat.rows();
    Neighbors neighbors(n);
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if ((mat(i, j) - cutoff) * direct > 0) {
                neighbors[i].push_back(j);
                neighbors[j].push_back(i);
            }
        }
    }
    return move(neighbors);
}

template<typename Mat_, typename Neighbors_, typename Distance_, typename Changed_>
void set_distance(const Mat_ &m, const Neighbors_ &neighbors, Distance_ &distance, Changed_ &changed, int beg) {
    changed[beg] = false;

    double d = distance[beg];

    for (auto && i : neighbors[beg]) {
        double di = distance[i];
        double dd = d + m(beg, i);
        if (di > dd) {
            changed[i] = true;
            distance[i] = dd;
        }
    }

    for (auto && i : neighbors[beg]) {
        if (changed[i]) {
            set_distance(m, neighbors, distance, changed, i);
        }
    }
}

vector<double> min_distance(const Matd &m, const vector<int> &begins, double cutoff) {
    int n = m.rows();
    vector<double> dist(n, 9999999);
    auto neighbors = get_neighbors(m, cutoff, -1);
//    cout << "Cutoff: " << cutoff << endl;

    for (auto && beg : begins) {
        vector<bool> changed(n, false);
        vector<double> distance(n, 9999999);

        distance[beg] = 0;

//        cout << "distance: ";
        set_distance(m, neighbors, distance, changed, beg);
//        for (int i = 0; i < n; i++) {
//            cout << distance[i] << ' ';
//        }
//        cout << endl;

        for (int i = 0; i < n; i++) {
            if (distance[i] < dist[i]) {
                dist[i] = distance[i];
            }
        }
    }

    return dist;
}

template<typename Neighbors_, typename Distance_, typename Path_>
void backtrack(const Neighbors_ &neighbors, const Distance_ &distance, int end, Path_ &path) {
    double min = distance[end];
    int j = -1;
    for (auto && i : neighbors[end]) {
        if (distance[i] < min) {
            j = i;
            min = distance[i];
        }
    }
    if (j == -1) {
        path.pop_front();
        return;
    } else {
        path.push_front(j);
        backtrack(neighbors, distance, j, path);
    }
}

vector<deque<int>> min_path(const Matd &m, const vector<int> &begins, const vector<int> &ends, double cutoff) {
    int n = m.rows();
//    cout << "N: " << n << endl;
    auto neighbors = get_neighbors(m, cutoff, -1);
//    for (int i = 0; i < n; i++) {
//        cout << m(0, i) << ' ';
//    }
//    cout << "Cutoff: " << cutoff << endl;

    vector<deque<int>> paths;
    for (auto && beg : begins) {
//        cout << "find path for " << beg << endl;
        vector<bool> changed(n, false);
        vector<double> distance(n, 9999999);

        distance[beg] = 0;

//        cout << "distance: ";
        set_distance(m, neighbors, distance, changed, beg);
//        for (int i = 0; i < n; i++) {
//            cout << distance[i] << ' ';
//        }
//        cout << endl;

        for (auto && end : ends) {
//            cout << "end: " << end << endl;
            deque<int> path;
            backtrack(neighbors, distance, end, path);
            paths.push_back(path);
        }

    }
    return paths;
}

class Diffuse {
public:
    const Matd &mat;
    const Neighbors &neighbors;
    Matb check_bond;
    Matb through_bond;
    vector<bool> through_node;

    int n;

    Diffuse(const Matd &mat_, const Neighbors &neighbors_) : mat(mat_), neighbors(neighbors_) {
        n = mat_.rows();
        check_bond = Matb(n, n, false);
        through_bond = Matb(n, n, false);
        through_node.resize(n, false);
    }

    void diffuse(int beg) {
        if (!through_node[beg]) {
            through_node[beg] = true;
            for (auto && i : neighbors[beg]) {
                if (!check_bond(beg, i)) {
                    check_bond(beg, i) = check_bond(i, beg) = true;
                    if (my_rand() < mat(beg, i)) {
                        through_bond(beg, i) = through_bond(i, beg) = true;
                        diffuse(i);
                    }
                }
            }
        }
    }

};

class PathFinder {
public:
    using Pair = pair<Path, vector<double>>;
    const Neighbors &neighbors;

    Matd en;
    vector<bool> visited;
    vector<bool> excluded;
    vector<bool> changed;
    vector<double> distance;
    vector<int> previous;

    using Container = deque<Pair>;
    Container container_a;
    Container container_b;

    int n;

    Path path;
    vector<Path> paths;
    vector<vector<double>> distances;
    vector<double> scores;

    PathFinder(const Matd &mat_, const Neighbors &neighbors_) : en(mat_), neighbors(neighbors_) {
        n = mat_.rows();

        for (int i = 0; i < n * n; i++) {
            if (en.data[i] < 0.0001) {
                en.data[i] = -std::log(0.0001);
            } else {
                en.data[i] = -std::log(en.data[i]);
            }
        }

        distance.resize(n, 999999);
        previous.resize(n, -1);
        visited.resize(n, false);
        excluded.resize(n, false);
        changed.resize(n, false);
    }

    bool container_has(const Container &container, const Path &path) {
        for (auto && p : container) {
            auto & path_ = p.first;
            if (path_.size() == path.size()) {
                auto it1 = path.begin();
                auto it2 = path_.begin();
                bool same = true;
                for (; it1 != path.end() && it2 != path_.end(); it1++, it2++) {
                    if (*it1 != *it2) {
                        same = false;
                        break;
                    }
                }
                if (same) {
                    return true;
                }
            }
        }
        return false;
    }

    void run(int start, int end, int n_paths) {
        Path path;

//        cout << "Run: " << start << ' ' << end << endl;
        distance[start] = 0;
        find_path(start, end);

        try {
            path = get_path(start, end);
        } catch (...) {
            return;
        }

//        std::cout << "path.size(): " << path.size() << std::endl;
//        for (auto && i : path) {
//            std::cout << i << ' ';
//        }
//        std::cout << std::endl;

        vector<double> path_distance;
        for (int i = 0; i < path.size(); i++) path_distance.push_back(distance[path[i]]);

        container_a.push_back(make_pair(path, path_distance));
//        cout << jnc::string_join("-", path) << endl;

        for (int i = 1; i < n_paths; i++) {
//            cout << "Sub " << i << endl;
            auto path_k = container_a[i - 1].first;
            auto distance_k = container_a[i - 1].second;

            for (int j = 0; j < int(path_k.size()) - 1; j++) { // !!! size() returns uint
//                cout << "Iter " << j << endl;
                int p = path_k[j];
                int q = path_k[j + 1];

                // reset previous
                for (auto && k : previous) k = -1;

                // reset visited
                for (auto && k : visited) k = false;
                for (int k = 0; k < j; k++) visited[path_k[k]] = true;

                // reset excluded
                for (auto && k : excluded) k = false;
                for (int k = 0; k < j; k++) excluded[path_k[k]] = true;

                // reset changed
                for (auto && k : changed) k = false;

                // reset distance
                for (auto && k : distance) k = 999999;

                distance[path_k[j]] = 0;
                double old_edge = en(p, q);
                en(p, q) = 999999;
                find_path(p, end);
                if (previous[end] == -1) {
//                    cout << "Not found!" << endl;
                    continue; // in case no path is found
                }
                auto path2 = get_path(p, end);
                en(p, q) = old_edge;

                Path path_new;
                vector<double> distance_new;

                for (int k = 0; k <= j; k++) {
                    path_new.push_back(path_k[k]);
                    distance_new.push_back(distance_k[k]);
                }
                for (int k = 1; k < path2.size(); k++) {
                    path_new.push_back(path2[k]);
                    distance_new.push_back(distance[path2[k]]);
                }

//                cout << jnc::string_join(" ", path_new) << endl;
                if (!container_has(container_b, path_new) && !container_has(container_a, path_new)) {
                    container_b.push_back(make_pair(path_new, distance_new));
                } else {
//                    cout << "Duplicate!" << endl;
                }
            }

            sort(container_b.begin(), container_b.end(), [](const Pair &a, const Pair &b){
                return a.second.back() < b.second.back();
            });

            if (container_b.empty()) {
//                cout << "Stop!" << endl;
                break;
            } else {
                container_a.push_back(container_b[0]);
                container_b.pop_front();
            }
        }
    }

    Path get_path(int start, int end) {
//        cout << "Get Path: from " << start << " to " << end << endl;
        Path path;
        path.push_back(end);
        int i = end;
        do {
//            std::cout << i << ' ' << previous[i] << std::endl;
            i = previous[i];
            if (i == -1) {
                throw "Get Path Error!";
            }
            path.push_front(i);
//            cout << "path size: " << path.size() << endl;
        } while (i != start);
//        cout << endl;
//        cout << "path size: " << path.size() << endl;
        return move(path);
    }

    void find_path(int beg, int end) {
        changed[beg] = false;

        double d = distance[beg];
        for (auto && i : neighbors[beg]) {
            if (!excluded[i]) {
                double di = distance[i];
                double dd = d + en(beg, i);
                if (di > dd) {
                    changed[i] = true;
                    distance[i] = dd;
                    previous[i] = beg;
                }
            }
        }

        for (auto && i : neighbors[beg]) {
            if (!excluded[i]) {
                if (changed[i]) {
                    find_path(i, end);
                }
            }
        }
    }
};

void set_propensities(NodePropensities &node_props, BondPropensities &bond_props, const Matd &mat, const vector<int> &sites, double cutoff, int steps) {
    auto neighbors = get_neighbors(mat, cutoff);

    int n = mat.rows();
    for (int step = 0; step < steps; step++) {
        Diffuse diffuse(mat, neighbors);
        for (auto && site: sites) {
            diffuse.diffuse(site);
        }

        for (int i = 0; i < n; i++) {
            if (diffuse.through_node[i]) {
                node_props[i]++;
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (diffuse.through_bond(i, j)) {
                    bond_props(i, j)++;
                    bond_props(j, i)++;
                }
            }
        }
    }

    double max = 0;
    for (int i = 0; i < n; i++) {
        if (max < node_props[i]) {
            max = node_props[i];
        }
    }
    for (int i = 0; i < n; i++) {
        node_props[i] /= max;
    }

    max = 0;
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            double d = bond_props(i, j);
            if (max < d) {
                max = d;
            }
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            bond_props(i, j) /= max;
            bond_props(j, i) /= max;
        }
    }
}

struct prot_t {
    Matd mat;
    Neighbors neighbors;
    double cutoff;
    int steps;
    double alpha;
};

void set_neighbors(prot_t &prot) {
    prot.neighbors = get_neighbors(prot.mat, prot.cutoff);
}

double get_aci(prot_t &prot, int site, int site2) {
    auto &mat = prot.mat;
    auto &neighbors = prot.neighbors;
    double &cutoff = prot.cutoff;
    int &steps = prot.steps;

    int n = mat.rows();

    NodePropensities node_props(n, 0);

    for (int step = 0; step < steps; step++) {
        Diffuse diffuse(mat, neighbors);
        diffuse.diffuse(site);

        for (int j = 0; j < n; j++) {
            if (diffuse.through_node[j]) {
                node_props[j]++;
            }
        }
    }

    double max = 0;
    for (int i = 0; i < n; i++) {
        if (max < node_props[i]) {
            max = node_props[i];
        }
    }

    return node_props[site2] / max;
}

template<typename T>
double square_max_element(T &&t, int n) {
    double mx = 0;
    for (int i = 0;i < n; i++) {
        for (int j = 0; j < n; j++) {
            mx = std::max(mx, t(i, j));
        }
    }
    return mx;
}

template<typename T>
double square_min_element(T &&t, int n) {
    double mn = 99999999;
    for (int i = 0;i < n; i++) {
        for (int j = 0; j < n; j++) {
            mn = std::min(mn, t(i, j));
        }
    }
    return mn;
}

void design(prot_t &prot, int start_node, int end_node, bool increase) {
    int n = prot.mat.rows();
    std::vector<std::tuple<int, double>> vd(n);
    double a, b, d;

    set_neighbors(prot);
    double old_aci = get_aci(prot, start_node, end_node);
    double mx = square_max_element(prot.mat, n);
    double mn = square_min_element(prot.mat, n);

    for (int inode = 0; inode < n; inode++) {
        auto &neighbors = prot.neighbors[inode];
        d = increase ? 0 : 9999999;
        for (auto jnode : neighbors) {
            // auto & l = neighbors[inode][ineighbor];
            double x = increase ? mx : mn;
            a = prot.mat(inode, jnode);
            b = prot.mat(jnode, inode);
            prot.mat(inode, jnode) = x;
            prot.mat(jnode, inode) = x;

            double new_aci = get_aci(prot, start_node, end_node);
            double d_aci = new_aci - old_aci;

            d = (increase ? std::max(d, d_aci) : std::min(d, d_aci));

            prot.mat(inode, jnode) = a;
            prot.mat(jnode, inode) = b;
        }
        vd[inode] = std::make_tuple(inode, d);
    }

    if (increase) {
        std::cout << "Suggested Mutation Sites to Increase Allosteric Correlation between Residue " << start_node+1 << " and Residue " << end_node+1 << "are:" << std::endl;
        std::sort(vd.begin(), vd.end(), [](auto &&p1, auto &&p2){
            return std::get<1>(p1) >= std::get<1>(p2);
        });
    } else {
        std::cout << "Suggested Mutation Sites to Decrease Allosteric Correlation between Residue " << start_node+1 << " and Residue " << end_node+1 << "are:" << std::endl;
        std::sort(vd.begin(), vd.end(), [](auto &&p1, auto &&p2){
            return std::get<1>(p1) <= std::get<1>(p2);
        });
    }

    for (int i = 0; i < 10; i++) {
        std::cout << "Residue " << std::get<0>(vd[i])+1 << ": " << std::get<1>(vd[i]) << std::endl;
    }
}

Matd get_correlations(const Matd &mat, double cutoff, int steps) {
    auto neighbors = get_neighbors(mat, cutoff);

    int n = mat.rows();

    Matd corr(n, n, 0);

    for (int site = 0; site < n; site++) {
        NodePropensities node_props(n, 0);

        for (int step = 0; step < steps; step++) {
            Diffuse diffuse(mat, neighbors);
            diffuse.diffuse(site);

            for (int j = 0; j < n; j++) {
                if (diffuse.through_node[j]) {
                    node_props[j]++;
                }
            }
        }

        double max = 0;
        for (int i = 0; i < n; i++) {
            if (max < node_props[i]) {
                max = node_props[i];
            }
        }

        for (int i = 0; i < n; i++) {
            node_props[i] /= max;
            corr(site, i) = node_props[i];
        }
    }

    return move(corr);
}

PathPropensities get_path_propensities(const Matd &mat, const vector<int> &start_sites, const vector<int> &end_sites, double cutoff, int n_paths) {
    auto neighbors = get_neighbors(mat, cutoff);

    int n = mat.rows();

    PathPropensities path_info;
    for (auto && start : start_sites) {
        for (auto && end : end_sites) {
            PathFinder finder(mat, neighbors);
            finder.run(start, end, n_paths);
            for (auto && p : finder.container_a) {
                path_info[p.first] = exp(-p.second.back());
            }
        }
    }

//    double max = 0;
//    for (auto && p : diffuse.path_info) {
//        if (max < p.second) {
//            max = p.second;
//        }
//    }

//    for (auto && p : path_info) {
//        p.second /= steps;
//    }

    return move(path_info);
}

void print_correlations(const Matd &corr, const string &out_file) {
    ofstream ofile(out_file.c_str());
    int n = corr.rows();
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            ofile << corr(i, j) << ' ';
        }
        ofile << std::endl;
    }
    ofile.close();
}

template<typename T>
void array_norm(T &t) {
    double max = -99999;
    double min = 99999;
    for (auto && i : t) {
        if (i > max) {
            max = i;
        }
        if (i < min) {
            min = i;
        }
    }

    double d = max - min;
    for (auto && i : t) {
        i = (i - min) / d;
    }
}

void print_array(const NodePropensities &props, const string &out_file) {
    ofstream ofile(out_file.c_str());
    for (auto && prop : props) {
        ofile << prop << endl;
    }
    ofile.close();
}

template<typename Mat_>
void print_mat(const Mat_ &mat, const string &out_file) {
    int m = mat.rows();
    int n = mat.cols();
    ofstream ofile(out_file.c_str());
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            ofile << mat(i, j) << ' ';
        }
        ofile << endl;
    }
    ofile.close();
}

void print_path_propensities(const PathPropensities &props, const string &out_file) {
    // Transform to a vector
    vector<pair<Path, double>> v;
    for (auto && p : props) {
        v.push_back(p);
    }

    // Sort
    sort(v.begin(), v.end(), [](auto && p1, auto && p2){
        return p1.second > p2.second;
    });

    // Print
    ofstream ofile(out_file.c_str());
    for (auto && p : v) {
        for (auto && i : p.first) {
            i++;
        }
        ofile << p.second << ' ' << jnc::string_join('-', p.first) << endl;
    }
    ofile.close();
}

vector<int> get_sites(const string &str) {
    vector<int> sites;
    auto && v = jnc::string_tokenize(str, "+");
    for (auto && s : v) {
        auto && w = jnc::string_tokenize(s, "-");
        if (w.size() == 1) {
            sites.push_back(string_parse<int>(s) - 1);
        } else if (w.size() == 2) {
            int l = string_parse<int>(w[0]) - 1;
            int u = string_parse<int>(w[1]) - 1;
            for (int i = l; i <= u; i++) {
                sites.push_back(i);
            }
        }
    }
    return move(sites);
}

/**
 * Example: ./diffuse mat.txt nodes.txt bonds.txt 1 10000 0.3
 */
int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);
    prot_t prot;

    prot.alpha = 1.0;
    opt.set(prot.alpha, "a", "alpha");
    double &alpha = prot.alpha;
    alpha = alpha / 10.0 + 0.05;

    int seed = 11;
    if (opt.has("seed", "S")) {
        opt.set(seed, "seed", "S");
    }
    my_seed(seed);
    
    bool mdist = opt.has("mdist");
    string mdist_file = "";
    opt.set(mdist_file, "mdist");

    bool mpath = opt.has("mpath");
    string mpath_file = "";
    opt.set(mpath_file, "mpath");

    string mat_file = opt.g[1];
    prot.mat = Matd::read(mat_file);
    auto &mat = prot.mat;

    if (opt.g[0] == "path") {
        set_alpha(mat, alpha);

        auto && start_sites = get_sites(opt.g[2]);
        auto && end_sites = get_sites(opt.g[3]);

        string path_out_file = opt.g[4];

        int n = 100;
        opt.set(n, "n");

        double cutoff = 0.05;
        opt.set(cutoff, "c", "cutoff");

        if (mpath) {
            int nrows = mat.rows();

            auto mat2 = mat;
            double lim = 0.00001;
            for (int i = 0; i < nrows; i++) {
                for (int j = i + 1; j < nrows; j++) {
                    if (mat2(i, j) < lim) {
                        mat2(i, j) = mat2(j, i) = -log(lim);
                    } else {
                        mat2(i, j) = mat2(j, i) = -log(mat2(i, j));
                    }
                }
            }
            auto paths = min_path(mat2, start_sites, end_sites, -log(lim));
            ofstream ofile(mpath_file.c_str());
            for (auto && path : paths) {
                for (auto && i : path) {
                    ofile << i + 1 << ' ';
                }
                ofile << endl;
            }
            ofile.close();
        }

        auto && path_propensities = get_path_propensities(mat, start_sites, end_sites, cutoff, n);

        print_path_propensities(path_propensities, path_out_file);

    } else if (opt.g[0] == "mutate") {
        set_alpha(mat, alpha);

        prot.steps = 100;
        opt.set(prot.steps, "n", "steps");

        prot.cutoff = 0.05;
        opt.set(prot.cutoff, "c", "cutoff");

//        auto && start_sites = get_sites(opt.g[2]);
//        auto && end_sites = get_sites(opt.g[3]);
//
        int site1 = JN_INT(opt.g[2]);
        int site2 = JN_INT(opt.g[3]);

        design(prot, site1, site2, !opt.has("decrease"));
    } else if (opt.g[0] == "design") {
        set_alpha(mat, alpha);

        prot.steps = 100;
        opt.set(prot.steps, "n", "steps");

        prot.cutoff = 0.05;
        opt.set(prot.cutoff, "c", "cutoff");

//        auto && start_sites = get_sites(opt.g[2]);
//        auto && end_sites = get_sites(opt.g[3]);
//
        int site1 = JN_INT(opt.g[2]);
        int site2 = JN_INT(opt.g[3]);

        design(prot, site1, site2, !opt.has("decrease"));
    } else if (opt.g[0] == "all") {
        set_alpha(mat, alpha);

        string out_file = opt.g[2];

        int steps = 1000;
        opt.set(steps, "n", "steps");

        double cutoff = 0.05;
        opt.set(cutoff, "c", "cutoff");

        auto && correlations = get_correlations(mat, cutoff, steps);

        print_correlations(correlations, out_file);

        if (mdist) {
            int n = mat.rows();
            Matd dist_mat(n, n, 1);

            auto mat2 = mat;
            double lim = 0.00001;
            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < n; j++) {
                    if (mat2(i, j) < lim) {
                        mat2(i, j) = mat2(j, i) = -log(lim);
                    } else {
                        mat2(i, j) = mat2(j, i) = -log(mat2(i, j));
                    }
                }
            }
            for (int i = 0; i < n; i++) {
                vector<int> sites;
                sites.push_back(i);
                auto distance = min_distance(mat2, sites, -log(lim));
                for (auto && j : distance) {
                    j = exp(-j);
                    if (j <= lim) {
                        j = lim;
                    }
                }
                for (int j = 0; j < n; j++) {
                    dist_mat(i, j) = distance[j];
                }
            }
            print_mat(dist_mat, mdist_file);
        }
    } else {
        auto && sites = get_sites(opt.g[2]);

        string node_out_file = opt.g[3];
        string bond_out_file = opt.g[4];

        int steps = 10000;
        opt.set(steps, "n", "steps");

        bool diff = false;
        diff = opt.has("d", "diff");

        bool iter = opt.has("it", "iter");
        string iter_file = "";

        bool nlen = opt.has("nlen");
        string nlen_file = "";

        double cutoff = 0.05;
        opt.set(cutoff, "c", "cutoff");

        auto mat_raw = mat;
        set_alpha(mat, alpha);

        int n = mat.rows();

        NodePropensities node_props(n, 0);
        BondPropensities bond_props(n, n, 0);

        set_propensities(node_props, bond_props, mat, sites, cutoff, steps);

        if (iter) {
            opt.set(iter_file, "it", "iter");
            double d = 0.05;
            Matd m(int(1 / d), n);
            int i = 0;
            for (double a = d; a <= 1 + d; a += d) {
                auto mat2 = mat_raw;
                set_alpha(mat2, a);
                NodePropensities node_props2(n, 0);
                BondPropensities bond_props2(n, n, 0);
                set_propensities(node_props2, bond_props2, mat2, sites, cutoff, steps);
                for (int j = 0; j < n; j++) {
                    m(i, j) = node_props2[j];
                }
                i++;
            }
            print_mat(m, iter_file);
        }

        // Normalization
        auto mat2 = mat;
        double lim = 0.00001;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (mat2(i, j) < lim) {
                    mat2(i, j) = mat2(j, i) = -log(lim);
                } else {
                    mat2(i, j) = mat2(j, i) = -log(mat2(i, j));
                }
            }
        }
        auto distance = min_distance(mat2, sites, -log(lim));
        for (auto && i : distance) {
            i = exp(-i);
            if (i <= lim) {
                i = lim;
            }
        }
        if (mdist) {
            print_array(distance, mdist_file);
        }
        for (int i = 0; i < n; i++) {
            if (node_props[i] < lim) {
                node_props[i] = lim;
            }
            if (node_props[i] == 1) {
                node_props[i] = 0;
            } else {
                node_props[i] = log(distance[i]) / log(node_props[i]);
            }
        }

        if (nlen) {
            opt.set(nlen_file, "nlen", "network_length");
            auto mat2 = mat;
            double lim = 0.00001;
            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < n; j++) {
                    if (mat2(i, j) < lim) {
                        mat2(i, j) = mat2(j, i) = 999;
                    } else {
                        mat2(i, j) = mat2(j, i) = 1;
                    }
                }
            }
            auto distance = min_distance(mat2, sites, 2);
            for (auto && dist : distance) {
                if (dist > 99999) {
                    dist = 99999;
                }
            }
            print_array(distance, nlen_file);
        }

//        array_norm(node_props);
        print_array(node_props, node_out_file);
        print_mat(bond_props, bond_out_file);
    }
    return 0;
}


