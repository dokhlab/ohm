#include <iostream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <algorithm>
#include <array>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <unordered_map>
#include <functional>
#include "grid.hpp"
#include "jnc/math"
#include "jnc/bio"

namespace position_utils {

using Position = std::array<int, 3>;

struct PositionHash {
    std::size_t operator()(const Position& p) const {
        std::size_t h1 = std::hash<int>{}(p[0]);
        std::size_t h2 = std::hash<int>{}(p[1]);
        std::size_t h3 = std::hash<int>{}(p[2]);
        return h1 ^ ((h2 ^ (h3 << 1)) << 1);
    }
};

struct PositionEqual {
    bool operator()(const Position& p1, const Position& p2) const {
        return p1[0] == p2[0] && p1[1] == p2[1] && p1[2] == p2[2];
    }
};

using PositionMap = std::unordered_map<Position, int, PositionHash, PositionEqual>;

} // namespace position_utils

class HoleFinder {
public:
    using Position = position_utils::Position;
    using Coord = std::array<double, 3>;

    struct BindingSite {
        Position position;
        double score;
        double bfactor;
    };
    using BindingSites = std::vector<BindingSite>;

    struct Cluster {
        std::vector<BindingSite> sites;
        double score;
        double bfactor;
    };
    using Clusters = std::vector<Cluster>;

    using PositionMap = position_utils::PositionMap;

    std::string infile;
    std::string outfile;

    int max_num_clusters = 8;
    int min_num_binding_sites = 100;
    int max_num_binding_sites = 5000;
    double score_cutoff = 0.25;
    double volume_ratio = 0.5;
    double vdw_radius = 1;
    // double vdw_radius = 3.3;
    // double shell = 0.1;
    double shell = 5;
    double interval = 0.4;
    int delta = 7;

    HoleFinder(const jnc::Opt &opt) {
        infile = opt.get("i");
        outfile = opt.get("o");
        opt.set(volume_ratio, "vr", "volume_ratio");
        opt.set(score_cutoff, "sc", "score_cutoff");
        opt.set(vdw_radius, "r", "vdw_radius");
        opt.set(shell, "s", "shell");
        opt.set(interval, "int", "interval");
        opt.set(delta, "d", "delta");
        opt.set(max_num_clusters, "n");
        opt.set(max_num_binding_sites, "m");
    }

    void run() {
        std::vector<BindingSite> results;

        jnc::bio::PdbChain in_pdb(infile);
        Grid sgrid;
        sgrid.vdw_radius = vdw_radius;
        sgrid.shell = shell;
        sgrid.interval = {interval, interval, interval};
        sgrid.init(in_pdb);

        double *p1 = sgrid.data;
        int d = delta;
        for (int x1 = 0; x1 < sgrid.shape[0]; x1++) {
//            std::cout << x1 << std::endl;
            for (int y1 = 0; y1 < sgrid.shape[1]; y1++) {
                for (int z1 = 0; z1 < sgrid.shape[2]; z1++) {
                    double n = 0;
                    double m = 0;
                    if (*p1 > 0 &&
                        x1 >= d && x1 < sgrid.shape[0] - d &&
                        y1 >= d && y1 < sgrid.shape[1] - d &&
                        z1 >= d && z1 < sgrid.shape[2] - d)
                    {
                        for (int x2 = -d; x2 < d; x2++) {
                            for (int y2 = -d; y2 < d; y2++) {
                                for (int z2 = -d; z2 < d; z2++) {
                                    // if (std::sqrt(x2 * x2 + y2 * y2 + z2 * z2) < d) {
                                        // if (sgrid.at(x1 + x2, y1 + y2, z1 + z2) >= 0) {
                                        //     n++;
                                        // }
                                        // m++;
                                    // }
                                    if (sgrid.at(x1 + x2, y1 + y2, z1 + z2) < 0) {
                                        n++;
                                    }
                                    m++;
                                }
                            }
                        }
                        double s = n / m;
                        // if (s < volume_ratio) {
                            results.push_back(BindingSite {Position {x1, y1, z1}, s, sgrid.bfactor(x1, y1, z1)});
                        // }
                    }
                    p1++;
                }
            }
        }

        std::sort(results.begin(), results.end(), [](const BindingSite &bs1, const BindingSite &bs2){
            return bs1.score > bs2.score;
        });

        std::vector<int> indices(results.size());
        std::iota(indices.begin(), indices.end(), 0);

//        auto && indices = cluster(results, 2);

        auto && clusters = cluster(results);

        // Write PDB
        std::ofstream ofile(outfile.c_str());
        jnc::bio::PdbWriter writer(ofile);
        int chain_name = 1;
        for (int i = 0; i < std::min(max_num_clusters, int(clusters.size())); i++) {
            auto & cluster = clusters[i];
            std::cout << "Cluster " << i + 1 << ":";
            jnc::bio::PdbChain chain;
            chain.name = jnc::string_format("%d", chain_name);
            chain_name++;
            for (int j = 0; j < cluster.sites.size(); j++) {
                auto && bs = cluster.sites[j];
                auto && c = sgrid.get_coord(bs.position[0], bs.position[1], bs.position[2]);
                std::cout << " " << bs.score << "," << bs.bfactor;

                jnc::bio::PdbResidue residue;
                residue.name = "X";

                jnc::bio::PdbAtom atom;
                atom.name = "P";
                for (int i = 0; i < 3; i++) atom[i] = c[i];

                residue.push_back(atom);
                chain.push_back(residue);
            }
            writer.write_chain(chain);
            std::cout << std::endl;
        }
        ofile.close();
    }

    Clusters cluster(const BindingSites &bs) {
        int n = bs.size();
        // std::cout << "Number of points on the surface: " << n << std::endl;
        // n = std::count_if(bs.begin(), bs.end(), [this](const BindingSite &b){
        //     return b.score > score_cutoff;
        // });
        // std::cout << "Number of Selected Binding Sites: " << n << std::endl;
        // if (n == 0) {
        //     n = min_num_binding_sites;
        // }
        // else if (n > max_num_binding_sites) {
        //     n = max_num_binding_sites;
        //     std::cout << "Exceeds the max number of sites, adjusting to: " << n << std::endl;
        // }
        std::vector<int> labels(n, -1);

        std::cout << "Cutoff score: " << bs[n-1].score << std::endl;

        // Set Position Map
        std::cout << "Set Position Map..." << std::endl;
        PositionMap m;
        for (int i = 0; i < n; i++) {
            m[bs[i].position] = i;
        }

        // Set labels
        std::cout << "Set Labels..." << std::endl;
        int label = 0;
        for (int i = 0; i < n; i++) {
            if (labels[i] == -1) {
//                std::cout << "Cluster " << label + 1 << std::endl;
                set_labels(i, label, 5, m, bs, labels);
                label++;
            }
        }

        // Set clusters
        std::cout << "Generate Clusters..." << std::endl;
        Clusters clusters(label);
        for (int i = 0; i < n; i++) {
            clusters[labels[i]].sites.push_back(bs[i]);
        }

        // Calculate cluster score and bfactor
        for (auto && cluster : clusters) {
            double score = 0;
            double bfactor = 0;
            for (auto && site : cluster.sites) {
                score += site.score;
                bfactor += site.bfactor;
            }
            cluster.score = score / double(cluster.sites.size());
            cluster.bfactor = bfactor / double(cluster.sites.size());
        }

        // Sort clusters
        std::cout << "Sort Clusters..." << std::endl;
        std::sort(clusters.begin(), clusters.end(), [](const Cluster &c1, const Cluster &c2){
            return c1.bfactor > c2.bfactor;
        });

        return std::move(clusters);
    }

    void set_labels(int i, int label, int d, const PositionMap &m, const BindingSites &bs, std::vector<int> &labels) {
        auto &c1 = bs[i].position;
        labels[i] = label;
        for (int x = -d; x < d; x++) {
            for (int y = -d; y < d; y++) {
                for (int z = -d; z < d; z++) {
                    Position c2 {c1[0] + x, c1[1] + y, c1[2] + z};
                    auto it = m.find(c2);
                    if (it != m.end()) {
                        int j = it->second;
                        if (labels[j] == -1) {
                            set_labels(j, label, d, m, bs, labels);
                        }
                    }
                }
            }
        }
    }
};

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    HoleFinder finder(opt);
    finder.run();

    return 0;
}

