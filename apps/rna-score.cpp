#include <iostream>
#include <fstream>
#include <jnc/bio>
#include <jnc/math>
#include <numeric>
#include <sstream>
#include <vector>

int main(int argc, char **argv) {
    jnc::Opt opt(argc, argv);

    jnc::RNAScorer scorer(opt.get("p"));
    std::string traj_file = opt.argv[1];

    jnc::bio::Pdb pdb;
    jnc::bio::PdbReader pdb_reader(pdb);

    std::ifstream ifile(traj_file.c_str());
    while (ifile) {
        pdb_reader.read_model(ifile);
        if (pdb.size() > 0) {
            auto &model = pdb.back();

            jnc::bio::PdbChain c;
            for (auto && chain : model) {
                for (auto && res : chain) {
                    c.push_back(std::move(res));
                }
            }

            std::cout << scorer.score(c) << std::endl;

            pdb.clear();
        }
    }
    ifile.close();

    return 0;
}

